package com.theportal.hand2hanger.mainscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.theportal.hand2hanger.data.model.FilterItemsData;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class WashRemoveItemAdapter extends RecyclerView.Adapter<WashRemoveItemAdapter.MyViewHolder> {
    //    private static RecyclerViewClickListener mItemListener;
    private final List<FilterItemsData> itemResponses;
    private Context context;
    private RemoveItemClickListener mListener;

    public WashRemoveItemAdapter(List<FilterItemsData> itemResponses, Context context) {
        this.itemResponses = itemResponses;
        this.context = context;
    }

    public void setRemoveItemClickListener(RemoveItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.delete_item_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        FilterItemsData itemList = itemResponses.get(position);
        if (itemList.getFacility_name().equals("")) {
            holder.incenseTypeTv.setText("---");
        } else {
            holder.incenseTypeTv.setText(itemList.getFacility_name());
        }
        String brand = "---";
        if (itemList.getBrand() != null)
            brand = itemList.getBrand();
        int number = position + 1;

        holder.orderItemTv.setText(number + " - " + itemList.getType() + " , " + itemList.getSize()
                + " , " + itemList.getColor() + " , " + brand);


        holder.deleteItemBtn.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onRemoveItemClick(position,itemList.getOrder_id(),itemList.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemResponses.size();
    }

    public interface RemoveItemClickListener {
        void onRemoveItemClick(int position,int order_id,int item_id);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.incense_type_tv)
        TextView incenseTypeTv;
        @BindView(R.id.order_item_tv)
        TextView orderItemTv;
        @BindView(R.id.delete_item_btn)
        ImageButton deleteItemBtn;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
