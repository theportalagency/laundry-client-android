package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemsListOrderDetails {

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("total_amount")
    @Expose
    private double total_amount;

    @SerializedName("facility_id")
    @Expose
    private String facility_id;

    @SerializedName("facility_name")
    @Expose
    private String facility_name;

    @SerializedName("type")
    @Expose
    private String type;


    @SerializedName("count")
    @Expose
    private int count;

    public int getCount() {
        return count;
    }

    public double getAmount() {
        return amount;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public String getType() {
        return type;
    }


}
