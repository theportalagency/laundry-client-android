package com.theportal.hand2hanger.notifications;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.Notifcation;
import com.theportal.hand2hanger.data.model.NotificationItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private final List<Notifcation> notificationItems;
    private Context context;

    NotificationAdapter(List<Notifcation> accountItems, Context context) {
        this.notificationItems = accountItems;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item_rv, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Notifcation notificationItem = notificationItems.get(position);

        String split[] = notificationItem.getCreated_at().split(" ");
        String time = split[1];
        String day = split[0];

        holder.notificationBrief.setText(notificationItem.getData().getBody());
        holder.notificationDate.setText(day);
        holder.notificationTime.setText(time);
        holder.notificationStatue.setText(notificationItem.getData().getTitle());
        switch (notificationItem.getData().getStatus_id()) {
            case 1: //pickedup
                holder.notificationStatueIcon.setImageResource(R.drawable.ic_confirmed_circle_notification);
                break;
            case 2: //drop off
                holder.notificationStatueIcon.setImageResource(R.drawable.ic_pending_circle_notification);
                break;
            case 3: //accepted
                holder.notificationStatueIcon.setImageResource(R.drawable.ic_deliverd_circle_notification);
                break;
            case 4: //out of delivery
                holder.notificationStatueIcon.setImageResource(R.drawable.ic_rejected_circle_notification);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return notificationItems.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification_order_brief)
        TextView notificationBrief;
        @BindView(R.id.notification_order_date)
        TextView notificationDate;
        @BindView(R.id.notification_order_time)
        TextView notificationTime;
        @BindView(R.id.notification_order_statue)
        TextView notificationStatue;
        @BindView(R.id.notification_statue_icon)
        ImageView notificationStatueIcon;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
