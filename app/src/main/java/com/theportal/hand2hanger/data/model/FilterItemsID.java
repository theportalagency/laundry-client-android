package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterItemsID {

    @SerializedName("data")
    @Expose
    private FilterItemsData data;

    public FilterItemsData getData() {
        return data;
    }
}
