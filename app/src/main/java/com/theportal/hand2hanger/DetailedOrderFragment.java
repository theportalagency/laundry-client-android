package com.theportal.hand2hanger;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.OnBackPressed;
import com.theportal.hand2hanger.data.model.AddressOrderDetails;
import com.theportal.hand2hanger.data.model.InvoiceOrderDetails;
import com.theportal.hand2hanger.data.model.ItemsListOrderDetails;
import com.theportal.hand2hanger.data.model.PromoCodeModel;
import com.theportal.hand2hanger.data.model.ReviewModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.DetailsOrder;
import com.theportal.hand2hanger.data.network.DetailsOrderData;
import com.theportal.hand2hanger.data.network.PromoCodeAmount;
import com.theportal.hand2hanger.mainscreen.HomeActivity;
import com.theportal.hand2hanger.myorders.InvoiceBasic;
import com.theportal.hand2hanger.myorders.ItemListAdapter;
import com.theportal.hand2hanger.myorders.PaymentDetailsOrder;
import com.theportal.hand2hanger.tracking.TrackingFragment;
import com.theportalagency.hand2hanger.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.ORDER_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID_SELECT;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.roundAvoid;

public class DetailedOrderFragment extends Fragment implements OnBackPressed {
    @BindView(R.id.cancell_order_btn)
    Button cancelOrderBtn;
    @BindView(R.id.rate_me_btn)
    Button rateOrderBtn;
    @BindView(R.id.track_order_btn)
    Button trackOrderBtn;
    @BindView(R.id.order_statue)
    TextView order_statue;
    @BindView(R.id.invoice_id)
    TextView orderInvoiceId;
    @BindView(R.id.download_invoice_tv)
    TextView downloadInvoiceTv;
    @BindView(R.id.pickup_date)
    TextView pickupDateTv;
    @BindView(R.id.pickup_time)
    TextView pickupTimeTv;
    @BindView(R.id.deliver_address)
    TextView deliveryAddress;
    @BindView(R.id.invoice_clothes_list_rv)
    RecyclerView invoice_clothes_list_rv;
    @BindView(R.id.tv_paymemntdetails)
    TextView tv_paymemntdetails;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.rv_payment)
    RecyclerView rv_payment;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    private int orderId, status_id;
    private String order_no;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private float rating;
    private String token, orderAddress, orderCollectionDate, orderCollectionTime, order_ref, status;
    private PaymentDetailsOrder payment_order;
    private ItemListAdapter itemListAdapter;
    private List<InvoiceBasic> invoiceBasics = new ArrayList<>();
    private String invoice_id;
    private int PERMISSION_REQUEST_CODE = 101;
    double delivery_cost = 0.0;
    double urgent_cost = 0.0;

    private String pickupTime, pickupDate, delivery;
    private double total_amount = 0.0;
    private double order_total_amount = 0.0;
    private double tax = 0.0,discount_fees = 0.0;
    private double facility_fee = 0.0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, view);
        cancelOrderBtn.setOnClickListener(view1 -> cancelOrder());
        rateOrderBtn.setOnClickListener(view1 -> showRateDialog());
        trackOrderBtn.setOnClickListener(view1 -> {
            TrackingFragment trackingFragment = new TrackingFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(ORDER_ID, status_id);
            bundle.putString("order_no", order_no);
            bundle.putString("pickup_d", pickupDate);
            bundle.putString("pickup_t", pickupTime);
            bundle.putString("delivery_d", delivery);
            bundle.putString("delivery_t", pickupTime);
            trackingFragment.setArguments(bundle);
            TAB_ID_SELECT = 0;
            loadFragment(trackingFragment, "TrackingFragment");
        });
        invoice_clothes_list_rv.setHasFixedSize(true);
        rv_payment.setHasFixedSize(true);
        invoice_clothes_list_rv.setNestedScrollingEnabled(false);
        rv_payment.setNestedScrollingEnabled(false);
        invoice_clothes_list_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_payment.setLayoutManager(new LinearLayoutManager(getActivity()));
        downloadInvoiceTv.setOnClickListener(view1 -> {
            checkPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_REQUEST_CODE);
            getOrderInvoice(invoice_id);
        });
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.invoice);
        counterPanel_.setVisibility(View.GONE);
        toolbar.setNavigationOnClickListener(view1 -> {
            Intent i = new Intent(Constants.context, HomeActivity.class);
            i.putExtra("back", 2);
            startActivity(i);
        });
        getBundleData();
        return view;
    }

    private void getBundleData() {
        orderId = getArguments() != null ? getArguments().getInt(ORDER_ID) : 0;
        getDetailsOrder(orderId);
        getReviewOrder(orderId);
    }

    private void getReviewOrder(int id) {
        Call<ReviewModel> call = apiInterface.getReview(token, String.valueOf(id));
        call.enqueue(new Callback<ReviewModel>() {
            @Override
            public void onResponse(Call<ReviewModel> call, Response<ReviewModel> response) {
                ReviewModel review = response.body();
                if (review == null) return;
                String rate = review.getRate();
                if (rate != null) {
                    rateOrderBtn.setEnabled(false);
                    rateOrderBtn.setText("Rate : " + rate);
                }
            }

            @Override
            public void onFailure(Call<ReviewModel> call, Throwable t) {

            }
        });
    }


    private void getDetailsOrder(int id) {
        Call<DetailsOrder> call = apiInterface.getDetailsOrder(token, id);
        call.enqueue(new Callback<DetailsOrder>() {
            @Override
            public void onResponse(Call<DetailsOrder> call, Response<DetailsOrder> response) {
                DetailsOrder order = response.body();
                if (order == null) return;
                DetailsOrderData detailsOrderData = order.getData();
                if (detailsOrderData == null) return;
                String[] split = detailsOrderData.getOrderNo().split("-");
                order_no = split[0];
                order_statue.setText("order is " + detailsOrderData.getStatus());
                status_id = detailsOrderData.getStatus_id();
                if (status_id >= 3) {
                    cancelOrderBtn.setVisibility(View.GONE);
                    cancelOrderBtn.setEnabled(false);
                }
                int hanged = detailsOrderData.getIs_hanged();
                int urgent = detailsOrderData.getIs_urgent();
                if (urgent == 1) {
                    urgent_cost = (detailsOrderData.getSubtotal());
                }
                if (urgent == 1 && hanged == 1)
                    orderInvoiceId.setText("INV " + split[0] + "\n" + "Urgent & Hanged Order");
                else if (urgent == 1 && hanged == 0)
                    orderInvoiceId.setText("INV " + split[0] + "\n" + "Urgent & Folding Order");
                else if (urgent == 0 && hanged == 1)
                    orderInvoiceId.setText("INV " + split[0] + "\n" + "Hanged Order");
                else if (urgent == 0 && hanged == 0)
                    orderInvoiceId.setText("INV " + split[0] + "\n" + "Folding Order");

                if (detailsOrderData.getCollection_date() != null) {
                    pickupDateTv.setText(detailsOrderData.getCollection_date());
                    pickupDate = detailsOrderData.getCollection_date();
                }

                if (detailsOrderData.getPickup_time() != null) {
                    pickupTime = detailsOrderData.getPickup_time().getFrom() + " - " + detailsOrderData.getPickup_time().getTo();
                    pickupTimeTv.setText(detailsOrderData.getPickup_time().getFrom() + " - " + detailsOrderData.getPickup_time().getTo());
                } else {
                    pickupTimeTv.setText("Not Entered Yet");
                    pickupTime = ("Not Entered Yet");
                    pickupDateTv.setVisibility(View.GONE);
                }

                AddressOrderDetails addressOrderDetails = detailsOrderData.getAddress();
                if (addressOrderDetails == null) {
                    deliveryAddress.setText("Not Entered Yet");
                    delivery_cost = 0;
                }
                if (addressOrderDetails != null) {
                    delivery = addressOrderDetails.getBuilding_no() + " " +
                            addressOrderDetails.getStreet() + " " +
                            addressOrderDetails.getRegion() + " - " +
                            addressOrderDetails.getCity() + "\n " +
                            "Floor No " + addressOrderDetails.getFlat_no() +
                            ", Flat No " + addressOrderDetails.getFlat_no();
                    deliveryAddress.setText(addressOrderDetails.getBuilding_no() + " " +
                            addressOrderDetails.getStreet() + " " +
                            addressOrderDetails.getRegion() + " - " +
                            addressOrderDetails.getCity() + "\n " +
                            "Floor No " + addressOrderDetails.getFlat_no() +
                            ", Flat No " + addressOrderDetails.getFlat_no());
                    delivery_cost = addressOrderDetails.getDelivery_cost();
                }
                int status_id = detailsOrderData.getStatus_id();
                if (status_id >= 4) {
                    cancelOrderBtn.setVisibility(View.GONE);
                }
                InvoiceOrderDetails invoice = detailsOrderData.getInvoice();
                invoiceBasics.clear();
                if (invoice != null) {
                    tax = invoice.getTax();
                    delivery_cost = invoice.getDelivery_cost();
                    total_amount = invoice.getSubtotal();
                    order_total_amount = invoice.getTotal();
                    invoice_id = invoice.getInvoiceno();
                } else {
                    downloadInvoiceTv.setVisibility(View.GONE);
                    if (addressOrderDetails != null)
                        delivery_cost = addressOrderDetails.getDelivery_cost();
                    total_amount = detailsOrderData.getSubtotal();
                    order_total_amount = detailsOrderData.getTotal_amount();
                }


                invoiceBasics.add(new InvoiceBasic(
                        "Tax", "" + tax
                ));

                if (delivery_cost != 0.0)
                    invoiceBasics.add(new InvoiceBasic(
                            "Delivery cost", "" + delivery_cost
                    ));


                PromoCodeModel model = detailsOrderData.getPromocode();
                if (model != null) {
                    String defination = null;
                    if (model.getIsPercentage() == 1)
                        defination = model.getAmount() + " %";
                    else
                        defination = model.getAmount() + "";

                    PromoCodeAmount codeAmount = detailsOrderData.getPromo_amount();
                    discount_fees = detailsOrderData.getPromo_amount().getPromo_amount();
                    if (codeAmount != null) {
                        float new_total_amount = codeAmount.getPromo_amount();
                        invoiceBasics.add(new InvoiceBasic(
                                "PromoCode Discount" + " " + defination, " -" + (new_total_amount)
                        ));
                    }
                }

                invoiceBasics.add(new InvoiceBasic(
                        "Sub total", "" + total_amount
                ));
                if (urgent_cost != 0.0) {
                    invoiceBasics.add(new InvoiceBasic(
                            "Urgent Fees", "" + urgent_cost
                    ));
                }

                if (detailsOrderData.getFacility() != null) {
                    int is_percentage = detailsOrderData.getIsPercentage();
                    int facility_amount = detailsOrderData.getFacility_amount();
                    if (is_percentage == 1) {
                        facility_fee = ((total_amount) * facility_amount) / 100;
                    } else {
                        facility_fee = facility_amount;
                    }
                    invoiceBasics.add(new InvoiceBasic(
                            "Incense fees", "" + facility_fee
                    ));
                }
                invoiceBasics.add(new InvoiceBasic(
                        "Total", "" + roundAvoid((order_total_amount)))
                );
                payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                rv_payment.setAdapter(payment_order);
                List<ItemsListOrderDetails> orderDetailsList = detailsOrderData.getItems();
                if (orderDetailsList == null) return;
                itemListAdapter = new ItemListAdapter(Constants.context, orderDetailsList);
                invoice_clothes_list_rv.setAdapter(itemListAdapter);
            }

            @Override
            public void onFailure(Call<DetailsOrder> call, Throwable t) {
                call.cancel();
            }
        });
    }


    private void loadFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment, fragmentTag).
                addToBackStack(null).
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void showRateDialog() {
        AlertDialog deleteItemAlertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.fragment_rate_order)
                .show();
        Objects.requireNonNull(deleteItemAlertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RatingBar ratingBar = deleteItemAlertDialog.findViewById(R.id.ratingBar);
        EditText ratingDialogEt = deleteItemAlertDialog.findViewById(R.id.rating_dialog_et);
        ratingDialogEt.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_FLAG_MULTI_LINE |
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        Button submitRateBtn = deleteItemAlertDialog.findViewById(R.id.other_btn);
        submitRateBtn.setOnClickListener((View v) -> {
            rating = ratingBar.getRating();
            submitReviewRequest(rating, ratingDialogEt.getText().toString());
            deleteItemAlertDialog.dismiss();
        });
    }

    private void submitReviewRequest(float rating, String reviewComments) {
        Map<String, Object> reviewItemMap = new HashMap<>();
        reviewItemMap.put("order_id", orderId);
        reviewItemMap.put("rate", rating);
        reviewItemMap.put("note", reviewComments);
        Call<SignupResponseModel> call = apiInterface.submitReview(token, reviewItemMap);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                SignupResponseModel model = response.body();
                if (model == null) {
                    Toast.makeText(Constants.context, "sorry you already rate this before", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (model.getSuccess() != null) {
                    Toast.makeText(Constants.context, "" + model.getSuccess(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void cancelOrder() {
        showCancelDialog();
    }

    private void showCancelDialog() {
        AlertDialog deleteItemAlertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.delete_basket_item)
                .show();
        Objects.requireNonNull(deleteItemAlertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button cancelOrderBtn = deleteItemAlertDialog.findViewById(R.id.delete_done_btn);
        TextView textView53 = deleteItemAlertDialog.findViewById(R.id.textView53);
        cancelOrderBtn.setText("Cancel");
        textView53.setText("Do You Want To Cancel ?");
        Button cancelDialogBtn = deleteItemAlertDialog.findViewById(R.id.delete_cancel_btn);
        cancelOrderBtn.setOnClickListener((View v) -> {
            cancelOrderRequest();
            deleteItemAlertDialog.dismiss();
        });
        cancelDialogBtn.setOnClickListener(v -> deleteItemAlertDialog.dismiss());
    }


    private void cancelOrderRequest() {
        Map<String, Object> orderItemMap = new HashMap<>();
        orderItemMap.put("order_id", orderId);
        Call<SignupResponseModel> call = apiInterface.cancelOrder(token, orderItemMap);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                SignupResponseModel model = response.body();
                if (model == null)
                    return;
                if (model.getSuccess() != null) {
                    Intent i = new Intent(Constants.context, HomeActivity.class);
                    i.putExtra("back", 2);
                    startActivity(i);
                    Toast.makeText(Constants.context, "" + model.getSuccess(), Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(Constants.context, "" + model.getError(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {

            }
        });
    }

    private void getOrderInvoice(String invoice) {
        Map<String, Object> orderIdMap = new HashMap<>();
        orderIdMap.put("invoiceno", invoice);
        Call<ResponseBody> call = apiInterface.getOrderInvoice(token, orderIdMap);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d("haaaaaaaaaaaaaaaaaa", "server contacted and has file");
                    if (response.body() == null) return;
                    writeResponseBodyToDisk(response.body());
                } else {
                    Toast.makeText(Constants.context, "Try Again", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void writeResponseBodyToDisk(ResponseBody body) {
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File futureStudioIconFile = new File(path, "Hand To Hanger");
            if (!futureStudioIconFile.exists()) {
                futureStudioIconFile.mkdir();
            }
            File gpxfile = new File(futureStudioIconFile, order_no + ".pdf");
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(gpxfile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    Toast.makeText(Constants.context, "File Downloaded Succesfully", Toast.LENGTH_SHORT).show();
                }
                outputStream.flush();
            } catch (IOException e) {
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
        }
    }

    // Function to check and request permission.
    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(Constants.context, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{permission},
                    requestCode);
        } else {
            Toast.makeText(Constants.context,
                    "Permission already granted",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    // This function is called when the user accepts or decline the permission.
    // Request Code is used to check which permission called this function.
    // This request code is provided when the user is prompt for permission.

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(Constants.context,
                        "Camera Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(Constants.context,
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Constants.context, HomeActivity.class);
        i.putExtra("back", 2);
        startActivity(i);
    }
}