package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoCodeAmount {

    @SerializedName("promo_amount")
    @Expose
    private float promo_amount;

    public float getPromo_amount() {
        return promo_amount;
    }
}
