package com.theportal.hand2hanger.mainscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.FacilityTypesResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FacilitiesChooseAdapter extends RecyclerView.Adapter<FacilitiesChooseAdapter.ViewHolder> {

    private Context context;
    private List<FacilityTypesResponse> items;
    private onChoose onChoose;
    private int mSelectedPosition = -1;
    private RadioButton mSelectedRB;
    private int facility_id = 0;
    private boolean data = false,iS_TAILORING = false;

    public FacilitiesChooseAdapter(Context context, List<FacilityTypesResponse> items,
                                   FacilitiesChooseAdapter.onChoose onChoose,int facility_id,boolean data) {
        this.context = context;
        this.items = items;
        this.onChoose = onChoose;
        this.facility_id = facility_id;
        this.data = data;
    }

    public FacilitiesChooseAdapter(Context context, List<FacilityTypesResponse> items,
                                   FacilitiesChooseAdapter.onChoose onChoose,boolean iS_TAILORING) {
        this.context = context;
        this.items = items;
        this.onChoose = onChoose;
        this.iS_TAILORING = iS_TAILORING;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.spinner_custom_design_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FacilityTypesResponse model = items.get(position);
        if (model == null)
            return;

        int is_percentage = model.getIsPercentage();
        switch (is_percentage) {
            case 0:
                holder.tv_spinner.setText(model.getAmount() + " QR");
                break;
            case 1:
                holder.tv_spinner.setText(model.getAmount() + " %");
                break;
            case -1:
                holder.tv_spinner.setVisibility(View.VISIBLE);
                holder.tv_spinner.setText(model.getAmount() + " QR");
                break;
        }

        if (!data) {
            holder.cb_spinner.setOnCheckedChangeListener((compoundButton, b) -> {
                if (position != mSelectedPosition && mSelectedRB != null) {
                    mSelectedRB.setChecked(false);
                }
                mSelectedPosition = position;
                mSelectedRB = holder.cb_spinner;
                onChoose.onItemChoose(b, model.getId(), model.getName(), model.getAmount(), model.getIsPercentage());
            });
        }

        if (mSelectedPosition != position) {
            holder.cb_spinner.setChecked(false);
        } else {
            holder.cb_spinner.setChecked(true);
            if (mSelectedRB != null && holder.cb_spinner != mSelectedRB) {
                mSelectedRB = holder.cb_spinner;
            }
        }
       /* if (text != null) {
            if (text.equals(model.getName())) {
                holder.cb_spinner.setChecked(true);
            }
        }*/
        holder.cb_spinner.setText(items.get(position).getName());
        if (facility_id == model.getId())
            holder.cb_spinner.setChecked(true);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_spinner;
        RadioButton cb_spinner;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_spinner = itemView.findViewById(R.id.tv_spinner);
            cb_spinner = itemView.findViewById(R.id.cb_spinner);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface onChoose {
        void onItemChoose(boolean check, int id, String name,int amount,int is_precentage);
    }
}