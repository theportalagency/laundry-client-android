package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletModel {

    @SerializedName("wallet")
    @Expose
    private double wallet;

    public double getWallet() {
        return wallet;
    }

}
