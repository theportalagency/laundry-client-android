package com.theportal.hand2hanger.basket.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.Utility.OnBackPressed;
import com.theportal.hand2hanger.addresses.MyAddressFragment;
import com.theportal.hand2hanger.basket.BasketItemsAdapter;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.Data;
import com.theportal.hand2hanger.data.model.DataFacility;
import com.theportal.hand2hanger.data.model.FacilityTypesResponse;
import com.theportal.hand2hanger.data.model.Order;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportal.hand2hanger.data.model.PromoCodeModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.model.SizesResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.DetailsOrder;
import com.theportal.hand2hanger.data.network.DetailsOrderData;
import com.theportal.hand2hanger.mainscreen.adapter.FacilitiesChooseAdapter;
import com.theportal.hand2hanger.mainscreen.adapter.FacilitiesChooseTailoringAdapter;
import com.theportal.hand2hanger.myorders.InvoiceBasic;
import com.theportal.hand2hanger.myorders.PaymentDetailsOrder;
import com.theportalagency.hand2hanger.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.CURRENT_CART_NUMBER;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID_SELECT;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.roundAvoid;

public class BasketFragment extends Fragment implements BasketItemsAdapter.RecyclerViewClickListener,
        FacilitiesChooseAdapter.onChoose,
        FacilitiesChooseTailoringAdapter.onChoose, OnBackPressed {

    @BindView(R.id.tv_applyPromo)
    TextView tv_applyPromo;
    @BindView(R.id.basket_rv)
    RecyclerView basketRv;
    @BindView(R.id.order_extra_notes)
    EditText orderExtraNotesEt;
    @BindView(R.id.promo_et)
    EditText promoEt;
    @BindView(R.id.next_btn)
    Button nextBtn;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.facility_switch)
    Switch facilitySwitch;
    @BindView(R.id.tvDeleteIncense)
    TextView tvDeleteIncense;
    @BindView(R.id.urgent_cb)
    CheckBox urgentCheckBox;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.facility_rv_basket)
    RecyclerView facility_rv;
    @BindView(R.id.btn_apply_promo)
    Button btn_apply_promo;
    @BindView(R.id.rv_prices_basket)
    RecyclerView rv_prices_basket;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.tv_deletepromo)
    TextView tv_deletepromo;
    private BasketItemsAdapter basketAdapter;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private String token;
    private Context context;
    private String extraNotes;
    private int isHanged;
    private int isUrgent;
    private String promoCode;
    private Map<String, Object> basketObjectMap = new HashMap<>();
    private Map<String, String> facilityDataMap = new HashMap<>();
    private List<FacilityTypesResponse> facilityList;
    private int facilityId = 0;
    private ArrayList<Integer> facilityIds = new ArrayList<>();
    private ArrayList<Integer> facilityIdsEdit = new ArrayList<>();
    private Map<String, Object> orderItemMap = new HashMap<>();
    private List<OrderItem> basketItemsList = new ArrayList<>();
    private OrderItem mItem;
    private NotificationCart notificationCart;
    private List<SizesResponseModel> sizesList;
    private List<SizesResponseModel> colorsList;
    FacilitiesChooseAdapter chooseAdapter;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private FacilitiesChooseTailoringAdapter facilitiesChooseTailoringAdapter;
    private List<FacilityTypesResponse> facilityEdit = new ArrayList<>();
    private List<FacilityTypesResponse> facility = new ArrayList<>();
    private List<FacilityTypesResponse> facilityTailoring = new ArrayList<>();
    private int order_id;
    private double total_amount, order_total_amount, discount_fees = 0.0, urgent_fees;
    private int category_id, service_id, typeToAdd;
    private String type_id;
    private List<InvoiceBasic> invoiceBasics = new ArrayList<>();
    private PaymentDetailsOrder payment_order;
    private int promocode_id = 0;
    private ProgressDialog dialog;
    String defination = null;
    private double amount_precentage;
    private String is_precentage;
    private boolean isFacilited = false;
    private boolean ISEDITING = false;
    private double facility_fee = 0.0;
    private int facility_id = 0;
    private boolean IS_DETAILS = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basket, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        token = pref.getString(TOKEN_KEY, token);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText("Basket");
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        dialog = new ProgressDialog(Constants.context);

        editor = pref.edit();
        editor.apply();
        facility_rv.setHasFixedSize(true);
        facility_rv.setNestedScrollingEnabled(false);
        facility_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        counterPanel_.setVisibility(View.GONE);
        facilityDataMap.put("service_id", "1");
        getData();
        nextBtn.setOnClickListener(view1 -> collectData());
        btn_apply_promo.setOnClickListener(view12 -> checkPromoCode());
        tv_deletepromo.setOnClickListener(view13 -> {
            deletePromo();
        });
        total_amount = 0.0;
        defination = null;
        order_total_amount = 0.0;
        amount_precentage = 0.0;
        urgent_fees = 0.0;
        is_precentage = null;
        facility_fee = 0.0;
        isUrgent = 0;
        isFacilited = false;
        ISEDITING = false;
        facilitySwitch.setChecked(false);

        tvDeleteIncense.setOnClickListener(view1 -> deleteFacility());
        facilitySwitch.setOnCheckedChangeListener((compoundButton, checked) -> {
            if (IS_DETAILS){
                IS_DETAILS = false;
                return;
            }
            if (checked) {
                facility_rv.setVisibility(View.VISIBLE);
                facilityIds.clear();
            } else {
                facility_rv.setVisibility(View.GONE);
                facilityIds.clear();
            }
        });
        urgentCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (isFacilited)
                return;
            if (b) {
                toggleUrgent(true);
                isFacilited = false;
            } else {
                toggleUrgent(false);
                isFacilited = false;

            }
        });
        rv_prices_basket.setNestedScrollingEnabled(false);
        rv_prices_basket.setHasFixedSize(true);
        rv_prices_basket.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    private void deletePromo() {
        promoEt.setText("");
        promocode_id = 0;
        tv_applyPromo.setVisibility(View.GONE);
        tv_deletepromo.setVisibility(View.GONE);
        is_precentage = null;
        order_total_amount = order_total_amount + discount_fees;
        defination = null;
        discount_fees = 0.0;
        invoiceBasics.clear();

        invoiceBasics.add(new InvoiceBasic(
                "Sub total", "" + (float) total_amount
        ));
        if (urgent_fees != 0) {
            invoiceBasics.add(new InvoiceBasic(
                    "Urgent Fees", "" + roundAvoid(urgent_fees)
            ));
        }
        invoiceBasics.add(new InvoiceBasic(
                "Incense fees", "" + facility_fee
        ));
        invoiceBasics.add(new InvoiceBasic(
                "Total", "" + roundAvoid(order_total_amount)
        ));
        payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
        rv_prices_basket.setAdapter(payment_order);
    }

    private void toggleUrgent(boolean check) {
        invoiceBasics.clear();
        promocode_id = 0;
        promoEt.setText("");
        if (check) {
            urgent_fees = total_amount;
            order_total_amount = (urgent_fees * 2)+facility_fee;
        } else {
            order_total_amount = order_total_amount - urgent_fees;
            urgent_fees = 0;
        }
        invoiceBasics.clear();
        invoiceBasics.add(new InvoiceBasic(
                "Sub Total", "" + (float) total_amount
        ));
        if (urgent_fees != 0.0) {
            invoiceBasics.add(new InvoiceBasic(
                    "Urgent Fees", "" + (float) urgent_fees
            ));
        }
        if (is_precentage != null) {
            String defination = null;
            if (is_precentage.equals(" %")) {
                discount_fees = order_total_amount * (amount_precentage / 100);
                order_total_amount = order_total_amount - discount_fees;
                defination = amount_precentage + " %";
            } else {
                discount_fees = amount_precentage;
                order_total_amount = order_total_amount - discount_fees;
                defination = amount_precentage + " QR";
            }
            discount_fees = roundAvoid(discount_fees);
            if (discount_fees != 0.0)
                invoiceBasics.add(new InvoiceBasic(
                        "Promo Code Discount" + " " + defination, "" + " -" + roundAvoid(discount_fees))
                );
        }
        invoiceBasics.add(new InvoiceBasic(
                "Incense fees", "" + roundAvoid(facility_fee)
        ));
        invoiceBasics.add(new InvoiceBasic(
                "Total", "" + roundAvoid(order_total_amount)
        ));
        payment_order = null;
        payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
        rv_prices_basket.setAdapter(payment_order);
    }

    private void checkPromoCode() {
        promoCode = promoEt.getText().toString();
        if (promoCode.equals("")) {
            promoEt.setError("Type Promo First");
            return;
        }

        if (is_precentage != null) {
            order_total_amount = (discount_fees / (amount_precentage / 100));
            discount_fees = 0.0;
            is_precentage = null;
            amount_precentage = 0.0;
        }

        dialog.setTitle("please wait");
        dialog.setCancelable(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        Map<String, String> promoCodeMap = new HashMap<>();
        promoCodeMap.put("promo_code", promoCode);
        promoCodeMap.put("order_id", String.valueOf(order_id));
        Call<PromoCodeModel> call = apiInterface.check_promo(token, promoCodeMap);
        call.enqueue(new Callback<PromoCodeModel>() {
            @Override
            public void onResponse(@NonNull Call<PromoCodeModel> call, @NonNull Response<PromoCodeModel> response) {
                PromoCodeModel model = response.body();
                dialog.dismiss();
                if (response.code() == 200) {
                    promocode_id = model.getId();
                    tv_applyPromo.setVisibility(View.VISIBLE);
                    tv_deletepromo.setVisibility(View.VISIBLE);
                    tv_applyPromo.setText("Succesfully Applied");
                    tv_applyPromo.setTextColor(Constants.context.getResources().getColor(R.color.GreenColor));
                    String defination;
                    if (model.getIsPercentage() == 1) {
                        is_precentage = " %";
                        amount_precentage = model.getAmount();
                        discount_fees = order_total_amount * (model.getAmount() / 100);
                        order_total_amount = order_total_amount - discount_fees;
                        defination = model.getAmount() + " %";
                    } else {
                        is_precentage = " QR";
                        amount_precentage = model.getAmount();
                        discount_fees = model.getAmount();
                        order_total_amount = order_total_amount - discount_fees;
                        defination = model.getAmount() + " QR";
                    }
                    discount_fees = roundAvoid(discount_fees);
                    invoiceBasics.clear();
                    invoiceBasics.add(new InvoiceBasic(
                            "Sub total", "" + (float) total_amount
                    ));
                    invoiceBasics.add(new InvoiceBasic(
                            "Urgent Fees", "" + (float) urgent_fees
                    ));

                    invoiceBasics.add(new InvoiceBasic(
                            "Incense fees", "" + facility_fee
                    ));
                    if (discount_fees != 0.0)
                        invoiceBasics.add(new InvoiceBasic(
                                "Promo Code Discount" + " " + defination, "" + " -" + roundAvoid(discount_fees))
                        );
                    invoiceBasics.add(new InvoiceBasic(
                            "Total", "" + roundAvoid(order_total_amount)
                    ));
                    payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                    rv_prices_basket.setAdapter(payment_order);
                } else {
                    promocode_id = 0;
                    tv_applyPromo.setVisibility(View.VISIBLE);
                    tv_applyPromo.setText("Promo Code Not Valid");
                    promoCode = "";
                    tv_applyPromo.setTextColor(Constants.context.getResources().getColor(R.color.RedColor));
                    payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                    rv_prices_basket.setAdapter(payment_order);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PromoCodeModel> call,@NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void getpromoOrNot() {
        Call<DetailsOrder> call = apiInterface.getDetailsOrder(token, order_id);
        call.enqueue(new Callback<DetailsOrder>() {
            @Override
            public void onResponse(
                    @NonNull Call<DetailsOrder> call,
                    @NonNull Response<DetailsOrder> response) {
                DetailsOrder detailsOrder = response.body();
                if (detailsOrder == null) return;
                isUrgent = detailsOrder.getData().getIs_urgent();
                DetailsOrderData detailsOrderData = detailsOrder.getData();

                total_amount = detailsOrderData.getSubtotal(); // amount
                invoiceBasics.clear();
                if (isUrgent == 1) {
                    isFacilited = true;
                    urgentCheckBox.setChecked(true);
                }

                isFacilited = false;

                facility_id = detailsOrderData.getFacility_id();
                PromoCodeModel codeModel = detailsOrderData.getPromocode();
                if (codeModel != null) {
                    tv_deletepromo.setVisibility(View.VISIBLE);
                    promoEt.setText(codeModel.getCode());
                    promocode_id = codeModel.getId();
                    btn_apply_promo.setEnabled(false);
                    tv_applyPromo.setVisibility(View.VISIBLE);
                    tv_applyPromo.setTextColor(Constants.context.getResources().getColor(R.color.GreenColor));
                    tv_applyPromo.setText("Succesfully Applied");

                    if (codeModel.getIsPercentage() == 1)
                        defination = codeModel.getAmount() + " %";
                    else
                        defination = codeModel.getAmount() + " QR";

                    discount_fees = detailsOrderData.getPromo_amount().getPromo_amount();
                }

                invoiceBasics.add(new InvoiceBasic(
                        "Sub total", "" + total_amount
                ));

                if (detailsOrderData.getFacility() != null) {
                    int is_percentage = detailsOrderData.getIsPercentage();
                    int facility_amount = detailsOrderData.getFacility_amount();
                    if (is_percentage == 1) {
                        facility_fee = (total_amount * facility_amount )/ 100;
                    } else {
                        facility_fee = facility_amount;
                    }
                    invoiceBasics.add(new InvoiceBasic(
                            "Incense fees", "" + roundAvoid(facility_fee)
                    ));
                    IS_DETAILS = true;
                    facilitySwitch.setChecked(true);
                    facility_rv.setVisibility(View.VISIBLE);
                    getFacilityItems(facility_rv,true);
                    tvDeleteIncense.setVisibility(View.VISIBLE);
                } else {
                    tvDeleteIncense.setVisibility(View.GONE);
                    getFacilityItems(facility_rv,false);
                }
                if (isUrgent == 1) {
                    urgent_fees = total_amount;
                    invoiceBasics.add(new InvoiceBasic(
                            "Urgent Fees", "" + urgent_fees
                    ));
                }
                if (defination != null && discount_fees != 0.0) {
                    discount_fees = roundAvoid(discount_fees);
                    invoiceBasics.add(new InvoiceBasic(
                            "Promo Code Discount" + " " + defination, "" + " -" + discount_fees
                    ));
                }
                invoiceBasics.add(new InvoiceBasic(
                        "Total", "" + roundAvoid(order_total_amount)
                ));
                payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                rv_prices_basket.setAdapter(payment_order);
            }

            @Override
            public void onFailure(Call<DetailsOrder> call, Throwable t) {

            }
        });
    }

    private void collectData() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.radio_hanged) {
            isHanged = 1;
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.radio_folding) {
            isHanged = 0;
        }
        if (urgentCheckBox.isChecked()) {
            isUrgent = 1;
        } else {
            isUrgent = 0;
        }
        extraNotes = orderExtraNotesEt.getText().toString();
        promoCode = promoEt.getText().toString();
        if (promocode_id != 0) {
            basketObjectMap.put("promo_code", String.valueOf(promocode_id));
            discount_fees = roundAvoid(discount_fees);
            basketObjectMap.put("promo_amount", discount_fees);
        } else {
            basketObjectMap.put("promo_code", "");
            basketObjectMap.put("promo_amount", 0.0);
        }


        basketObjectMap.put("is_urgent", isUrgent);
        basketObjectMap.put("is_hanged", isHanged);
        basketObjectMap.put("amount", total_amount);
        basketObjectMap.put("total_amount", order_total_amount);
        basketObjectMap.put("comment", extraNotes);
        sendData(basketObjectMap);
    }

    private void sendData(Map<String, Object> basketObjectMap) {
        Call<SignupResponseModel> call = apiInterface.sendBasketObject(token, basketObjectMap);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                if (response.code() == 200) {
                    Fragment fragment = new MyAddressFragment();
                    Bundle bundle = new Bundle();
                    bundle.putDouble("total", order_total_amount);
                    bundle.putDouble("amount", total_amount);
                    bundle.putDouble("order_id", order_id);
                    fragment.setArguments(bundle);
                    TAB_ID_SELECT = (TAB_ID_SELECT + 10);
                    getActivity().getSupportFragmentManager().beginTransaction().
                            replace(R.id.frame_layout, fragment).addToBackStack(null).commit();
                } else {
                    Toast.makeText(context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Toast.makeText(context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void getData() {
        getBasketItems();
    }

    private void getFacilityItems(RecyclerView rv,boolean data) {
        Call<List<FacilityTypesResponse>> call = apiInterface.getFacilityType(token);
        call.enqueue(new Callback<List<FacilityTypesResponse>>() {
            @Override
            public void onResponse(Call<List<FacilityTypesResponse>> call, Response<List<FacilityTypesResponse>> response) {
                facility = response.body();
                facilityEdit = response.body();
                chooseAdapter = null;
                chooseAdapter = new FacilitiesChooseAdapter(Constants.context, facility,
                        BasketFragment.this, facility_id,data);
                rv.setAdapter(chooseAdapter);
            }
            @Override
            public void onFailure(Call<List<FacilityTypesResponse>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void getBasketItems() {
        Call<BasketResponse> call = apiInterface.getBasketOrders(token);
        call.enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, Response<BasketResponse> response) {
                if (response.isSuccessful()) {
                    BasketResponse basketResponse = response.body();
                    if (basketResponse == null)
                        return;

                    if (basketResponse.getData() == null || basketResponse.getData().getItems().size() == 0) {
                        Toast.makeText(getActivity(), "Basket is Empty", Toast.LENGTH_SHORT).show();
                        applyCancelOrder(order_id);
                        nextBtn.setEnabled(false);
                        return;
                    }

                    order_id = basketResponse.getData().getOrder_id();
                    basketItemsList = basketResponse.getData().getItems();
                    if (basketItemsList == null) return;
                    prepareUi(basketResponse.getData(), basketItemsList);
                    order_total_amount = basketResponse.getData().getOrderTotalAmount();
                    amount_precentage = 0.0;
                    total_amount = 0.0;
                    discount_fees = 0.0;
                    urgent_fees = 0.0;
                    getpromoOrNot();
                } else {
                    if (response.body() == null) {
                        nextBtn.setEnabled(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                nextBtn.setEnabled(false);
                Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net),
                        Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    private void prepareUi(Data body, List<OrderItem> basketItemsList) {
        if (body != null) {
            prepareBasketItemsRv(basketItemsList);
        }
    }

    private void prepareBasketItemsRv(List<OrderItem> itemsList) {
        if (dialog != null)
            dialog.dismiss();
        promocode_id = 0;
        promoEt.setText("");
        urgent_fees = 0;
        tv_applyPromo.setVisibility(View.GONE);
        if (urgentCheckBox.isChecked()) {
            isFacilited = true;
            urgentCheckBox.setChecked(false);
        }
        isFacilited = false;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        basketRv.setLayoutManager(layoutManager);
        basketAdapter = new BasketItemsAdapter(itemsList, getActivity());
        basketRv.setHasFixedSize(true);
        basketRv.setAdapter(basketAdapter);
        basketAdapter.setItemListener(this);
    }

    private void removeItemFromBasket(int pos, int itemId, int orderId) {
        orderItemMap.put("item_id", itemId);
        orderItemMap.put("order_id", orderId);
        Call<OrderItem> call = apiInterface.deleteOrderItem(token, orderItemMap);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(@NonNull Call<OrderItem> call, @NonNull Response<OrderItem> response) {
                if (response.code() == 200) {
                    Toast.makeText(getContext(), R.string.success_delete, Toast.LENGTH_LONG).show();
                    basketItemsList.remove(pos);
                    if (basketItemsList.size() == 0) {
                        nextBtn.setEnabled(false);
                        tv_deletepromo.setVisibility(View.GONE);
                        tvDeleteIncense.setVisibility(View.GONE);
                        promoEt.setText("");
                        //applyCancelOrder(orderId);
                        tv_applyPromo.setVisibility(View.GONE);
                        IS_DETAILS = true;
                        facilitySwitch.setChecked(false);
                        Toast.makeText(Constants.context, "Basket is empty", Toast.LENGTH_SHORT).show();
                    }
                    basketAdapter.notifyDataSetChanged();
                    invoiceBasics.clear();
                    payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                    rv_prices_basket.setAdapter(payment_order);
                    getBasketItems();
                    CURRENT_CART_NUMBER = basketItemsList.size();
                    notificationCart = new NotificationCart(counterPanel_);
                    notificationCart.DecreaseNumber();
                }
            }

            @Override
            public void onFailure(Call<OrderItem> call, Throwable t) {

            }
        });
    }

    private void applyCancelOrder(int orderId) {
        Map<String, Object> orderItemMap = new HashMap<>();
        orderItemMap.put("order_id", orderId);
        Call<SignupResponseModel> call = apiInterface.cancelOrder(token, orderItemMap);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                SignupResponseModel model = response.body();
                if (model == null)
                    return;
                if (model.getSuccess() != null) {
                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {

            }
        });
    }

    private void ShowDeleteDialog(int position, int item_id, int order_id) {
        AlertDialog deleteItemAlertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.delete_basket_item)
                .show();
        deleteItemAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button deleteItemBtn = deleteItemAlertDialog.findViewById(R.id.delete_done_btn);
        Button cancelItemBtn = deleteItemAlertDialog.findViewById(R.id.delete_cancel_btn);
        deleteItemBtn.setOnClickListener(v -> {
            removeItemFromBasket(position, item_id, order_id);
            deleteItemAlertDialog.dismiss();
        });
        cancelItemBtn.setOnClickListener(v -> deleteItemAlertDialog.dismiss());
    }

    @Override
    public void onItemDelete(int position, int item_id, int orderid) {
        ShowDeleteDialog(position, item_id, order_id);
    }

    @Override
    public void onItemDecreaseCount(int position, int item_id, int type_id, int service_id,
                                    int category_id, int count, TextView tv_count,int orderID) {
        orderItemMap.clear();
        orderItemMap.put("type_id", type_id);
        orderItemMap.put("category_id", category_id);
        orderItemMap.put("count", (count - 1));
        orderItemMap.put("service_id", service_id);
        deleteOrderItem(orderItemMap, item_id, (count - 1), tv_count,orderID);
    }

    @Override
    public void onItemIncreaseCount(int position, int item_id, int type_id, int service_id,
                                    int category_id, int count, TextView tv_count, int order_id) {

        orderItemMap.clear();
        orderItemMap.put("type_id", type_id);
        orderItemMap.put("category_id", category_id);
        orderItemMap.put("count", (count + 1));
        orderItemMap.put("service_id", service_id);
        addOrderItem(orderItemMap);
    }

    private void addOrderItem(Map<String, Object> orderItemMap) {
        dialog.show();
        Call<Order> call = apiInterface.sendOrderDetails(token, orderItemMap);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    Toast.makeText(getContext(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    getBasketItems();
                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteOrderItem(Map<String, Object> orderItemMap, int item_id, int newCount, TextView tv,int orderID) {
        dialog.show();
        Call<Order> call = apiInterface.sendOrderDetails(token, orderItemMap);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    Order order = response.body();
                    Toast.makeText(getContext(), "Order removed Successfully", Toast.LENGTH_LONG).show();
                    if (order == null) return;
                    if (newCount == 0) {
                        removeItemFromBasket(item_id, order.getOrder_id());
                    }
                    getBasketItems();
                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void removeItemFromBasket(int itemId, int orderId) {
        orderItemMap.put("item_id", itemId);
        orderItemMap.put("order_id", orderId);
        Call<OrderItem> call = apiInterface.deleteOrderItem(token, orderItemMap);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(Call<OrderItem> call, Response<OrderItem> response) {
                if (response.code() == 200) {
                    Toast.makeText(getContext(), "Order remove From Basket Successfully", Toast.LENGTH_LONG).show();
                    getBasketItems();
                }
            }

            @Override
            public void onFailure(Call<OrderItem> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemChoose(boolean b, int id, String name, int amount, int is_precentage) {
        if (b) {
            facilityIds.clear();
            facilityIdsEdit.clear();
            facilityIds.add(id);
            facilityIdsEdit.add(id);
            if (!ISEDITING) {
                applyFacility(id);
            }
        } else {
            facilityIds.clear();
            facilityIdsEdit.clear();
        }
    }

    private void applyFacility(int facility_id) {
        dialog.setTitle("please wait");
        dialog.setCancelable(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        Map<String, Integer> facilityID = new HashMap<>();
        facilityID.put("facility_id", facility_id);
        Call<DataFacility> call = apiInterface.toogle_facility(token, facilityID);
        call.enqueue(new Callback<DataFacility>() {
            @Override
            public void onResponse(Call<DataFacility> call, Response<DataFacility> response) {
                dialog.dismiss();
                DataFacility data = response.body();
                if (data == null) {
                    return;
                }
                tvDeleteIncense.setVisibility(View.VISIBLE);
                total_amount = data.getAmount();
                urgentCheckBox.setChecked(false);
                order_total_amount = data.getTotal_amount();
                invoiceBasics.clear();
                promocode_id = 0;
                promoEt.setText("");
                tv_deletepromo.setVisibility(View.GONE);
                invoiceBasics.clear();
                invoiceBasics.add(new InvoiceBasic(
                        "Sub total", "" + total_amount
                ));
                facility_fee = (order_total_amount - total_amount);
                invoiceBasics.add(new InvoiceBasic(
                        "Incense fees", "" + roundAvoid(facility_fee)
                ));
                invoiceBasics.add(new InvoiceBasic(
                        "Total", "" + roundAvoid(order_total_amount)
                ));
                payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                rv_prices_basket.setAdapter(payment_order);
            }

            @Override
            public void onFailure(@NonNull Call<DataFacility> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void deleteFacility() {
        dialog.setTitle("please wait");
        dialog.setCancelable(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        Call<DataFacility> call = apiInterface.delete_facility(token);
        call.enqueue(new Callback<DataFacility>() {
            @Override
            public void onResponse(Call<DataFacility> call, Response<DataFacility> response) {
                dialog.dismiss();
                DataFacility data = response.body();
                if (data == null) {
                    return;
                }
                tvDeleteIncense.setVisibility(View.GONE);
                total_amount = data.getAmount();
                order_total_amount = data.getTotal_amount();
                invoiceBasics.clear();
                promocode_id = 0;
                facility_rv.setVisibility(View.VISIBLE);
                facility_fee = 0.0;
                promoEt.setText("");
                tv_deletepromo.setVisibility(View.GONE);
                invoiceBasics.clear();
                urgent_fees = 0.0;
                isUrgent = 0;
                urgentCheckBox.setChecked(false);
                facilitySwitch.setChecked(false);
               // rv_prices_basket.removeAllViewsInLayout();
                invoiceBasics.clear();
                invoiceBasics.add(new InvoiceBasic(
                        "Sub Total", "" + total_amount
                ));
                invoiceBasics.add(new InvoiceBasic(
                        "Total", "" + roundAvoid(total_amount)
                ));
                payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
                rv_prices_basket.setAdapter(payment_order);
            }

            @Override
            public void onFailure(@NonNull Call<DataFacility> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onItemChooseTailoring(int id, String name) {

    }

    @Override
    public void onBackPressed() {
        getActivity().onBackPressed();
    }
}