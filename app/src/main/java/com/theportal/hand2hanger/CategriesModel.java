package com.theportal.hand2hanger;

import com.google.gson.annotations.SerializedName;

public class CategriesModel {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
