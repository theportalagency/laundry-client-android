package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pricing {

    @SerializedName("data")
    @Expose
    private List<PricingItem> data = null;

    public List<PricingItem> getData() {
        return data;
    }

    public void setData(List<PricingItem> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return
                "Response{" +
                        "data = '" + data + '\'' +
                        "}";
    }
}