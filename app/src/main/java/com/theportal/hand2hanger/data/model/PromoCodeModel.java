package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoCodeModel {

    @SerializedName("amount")
    @Expose
    private double amount;


    @SerializedName("isPercentage")
    @Expose
    private int isPercentage;


    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public double getAmount() {
        return amount;
    }

    public int getIsPercentage() {
        return isPercentage;
    }

    public int getId() {
        return id;
    }
}
