package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceOrderDetails {

    @SerializedName("invoiceno")
    private String invoiceno;

    @SerializedName("subtotal")
    @Expose
    private double subtotal;

    @SerializedName("total")
    @Expose
    private double total;

    @SerializedName("tax")
    @Expose
    private double tax;

    @SerializedName("delivery_cost")
    @Expose
    private int delivery_cost;

    public String getInvoiceno() {
        return invoiceno;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public double getTotal() {
        return total;
    }

    public double getTax() {
        return tax;
    }

    public int getDelivery_cost() {
        return delivery_cost;
    }
}
