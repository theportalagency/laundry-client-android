package com.theportal.hand2hanger.tracking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.vipulasri.timelineview.TimelineView;
import com.theportalagency.hand2hanger.R;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.ViewHolder> {

    private Context context;
    private List<TimeLineMdel> items;


    public TimeLineAdapter(Context context, List<TimeLineMdel> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public TimeLineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_line_item, parent, false);
        return new ViewHolder(itemView,viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (items.get(position).getStatus() == 0) {
            holder.mTimelineView.setMarker(context.getDrawable(R.drawable.ic_dot_green));
            holder.mTimelineView.setEndLineColor(context.getResources().getColor(R.color.GreenColor),0);
        }
        else {
            holder.mTimelineView.setMarker(context.getDrawable(R.drawable.ic_dot_silver));
            holder.mTimelineView.setEndLineColor(context.getResources().getColor(R.color.silverColor),0);
        }
    }
    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TimelineView mTimelineView;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            mTimelineView = itemView.findViewById(R.id.timeline);
            mTimelineView.initLine(viewType);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
