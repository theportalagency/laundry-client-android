package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignupResponseModel {
    @SerializedName("success")
    @Expose
    private String success;


    @SerializedName("errors")
    private ErrorsSignUp error;

    @SerializedName("terms")
    private
    String terms;
    @SerializedName("about")
    private
    String about;

    @SerializedName("status")
    private
    String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTerms() {
        return terms;
    }

    public String getSuccess() {
        return success;
    }

    public ErrorsSignUp getError() {
        return error;
    }


    public String getAbout() {
        return about;
    }

   }
