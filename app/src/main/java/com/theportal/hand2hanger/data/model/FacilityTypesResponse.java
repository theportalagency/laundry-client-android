package com.theportal.hand2hanger.data.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityTypesResponse {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("amount")
    @Expose
    private int amount;

    @SerializedName("isPercentage")
    @Expose
    private int isPercentage;

    public FacilityTypesResponse(Integer id, String name, int amount, int isPercentage) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.isPercentage = isPercentage;
    }

    public String getPrice() {
        return price;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public int getIsPercentage() {
        return isPercentage;
    }
}