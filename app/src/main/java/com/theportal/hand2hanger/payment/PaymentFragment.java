package com.theportal.hand2hanger.payment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qpay.qpayandroidsdk.base.QpaySdk;
import com.qpay.qpayandroidsdk.base.SdkUtils;
import com.theportal.hand2hanger.MyError;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.ExpandCollapseExtention;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.Utility.OnBackPressed;
import com.theportal.hand2hanger.data.model.AddressOrderDetails;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.PromoCodeModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.model.WalletModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.DetailsOrder;
import com.theportal.hand2hanger.data.network.DetailsOrderData;
import com.theportal.hand2hanger.data.network.SupportModel;
import com.theportal.hand2hanger.mainscreen.HomeActivity;
import com.theportal.hand2hanger.myorders.InvoiceBasic;
import com.theportal.hand2hanger.myorders.PaymentDetailsOrder;
import com.theportalagency.hand2hanger.R;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.roundAvoid;

public class PaymentFragment extends AppCompatActivity {

    @BindView(R.id.credit_card_btn)
    Button creditCardBtn;

    @BindView(R.id.cod_btn)
    Button codBtn;

    @BindView(R.id.wallet_btn)
    Button wallet_btn;

    @BindView(R.id.rv_price_payment)
    RecyclerView rv_price_payment;

    @BindView(R.id.linear_credit)
    LinearLayout linear_credit;

    @BindView(R.id.remaining_balance__tv)
    TextView remaining_balance__tv;

    @BindView(R.id.rl_wallet)
    RelativeLayout rl_wallet;

    @BindView(R.id.checkout_payment)
    Button checkout_payment;

    @BindView(R.id.toolbarr)
    Toolbar toolbar;

    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;

    NotificationCart notificationCart;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;
    private int id = 1, order_id;
    private List<InvoiceBasic> invoiceBasics = new ArrayList<>();
    private PaymentDetailsOrder payment_order;
    private double total_amount,amount;
    private QpaySdk reqParams;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment);
        ButterKnife.bind(this,this);
        pref = Objects.requireNonNull(getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0));
        editor = pref.edit();
        editor.apply();
        reqParams = new QpaySdk(this);
        token = pref.getString(TOKEN_KEY, token);
        rv_price_payment.setHasFixedSize(true);
        rv_price_payment.setNestedScrollingEnabled(false);
        rv_price_payment.setLayoutManager(new LinearLayoutManager(PaymentFragment.this));
        notificationCart = new NotificationCart(counterPanel_);
        codBtn.setTextColor(getResources().getColor(R.color.BlackColor));
        wallet_btn.setOnClickListener(view1 -> {
            ExpandCollapseExtention.collapse(linear_credit);
            ExpandCollapseExtention.expand(rl_wallet);
            wallet_btn.setTextColor(getResources().getColor(R.color.BlackColor));
            codBtn.setTextColor(getResources().getColor(R.color.BlueColor));
            creditCardBtn.setTextColor(getResources().getColor(R.color.BlueColor));
            id = 2;
        });
        codBtn.setOnClickListener(view12 -> {
            ExpandCollapseExtention.collapse(linear_credit);
            ExpandCollapseExtention.collapse(rl_wallet);
            codBtn.setTextColor(getResources().getColor(R.color.BlackColor));
            creditCardBtn.setTextColor(getResources().getColor(R.color.BlueColor));
            wallet_btn.setTextColor(getResources().getColor(R.color.BlueColor));
            id = 1;
        });
        creditCardBtn.setOnClickListener(view13 -> {
            //ExpandCollapseExtention.expand(linear_credit);
            ExpandCollapseExtention.collapse(rl_wallet);
            creditCardBtn.setTextColor(getResources().getColor(R.color.BlackColor));
            codBtn.setTextColor(getResources().getColor(R.color.BlueColor));
            wallet_btn.setTextColor(getResources().getColor(R.color.BlueColor));
            id = 3;
            addPayment();
        });
        init();
    }

    private void init() {
        counterPanel_.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.payment);
        notificationCart = new NotificationCart(counterPanel_);
        toolbar.setNavigationOnClickListener(view1 -> {
            getFragmentManager().popBackStack();
        });
        checkout_payment.setOnClickListener(view -> confirmPaymentMethod());
        getBalance();
        getOrderID();
        checkBundleKEY();
    }

    private void checkBundleKEY() {
        Intent bundle = getIntent();
        if (bundle == null) return;
        total_amount = bundle.getDoubleExtra("total",0.0);
        amount = bundle.getDoubleExtra("amount",0.0);
        invoiceBasics.clear();
        invoiceBasics.add(new InvoiceBasic(
                "Total", "" + roundAvoid(total_amount)
        ));
        payment_order = new PaymentDetailsOrder(PaymentFragment.this, invoiceBasics);
        rv_price_payment.setAdapter(payment_order);
    }

    private void getBalance() {
        Call<WalletModel> call = apiInterface.wallet_balance(token);
        call.enqueue(new Callback<WalletModel>() {
            @Override
            public void onResponse(Call<WalletModel> call, Response<WalletModel> response) {
                WalletModel model = response.body();
                if (model == null) return;
                remaining_balance__tv.setText(model.getWallet() + " QR");
            }

            @Override
            public void onFailure(Call<WalletModel> call, Throwable t) {

            }
        });
    }

    private void confirmPaymentMethod() {
        Map<String, Integer> hash = new HashMap<>();
        hash.put("payment_type_id", id);
        Call<SignupResponseModel> call = apiInterface.confirmPayment(token, hash);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                if (response.code() == 200 && response.body() != null) {
                    Toast.makeText(PaymentFragment.this, response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    notificationCart.deleteCard();
                    startActivity(new Intent(PaymentFragment.this, HomeActivity.class));
                } else if (response.code() == 500){
                    JsonParser parser = new JsonParser();
                        JsonElement mJson;
                        try {
                            mJson = parser.parse(response.errorBody().string());
                            Gson gson = new Gson();
                            MyError errorResponse = gson.fromJson(mJson, MyError.class);
                            Toast.makeText(PaymentFragment.this, ""+errorResponse.getErrors(), Toast.LENGTH_SHORT).show();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void getPrices(int orderId) {
        Call<DetailsOrder> call = apiInterface.getDetailsOrder(token, orderId);
        call.enqueue(new Callback<DetailsOrder>() {
            @Override
            public void onResponse(Call<DetailsOrder> call, Response<DetailsOrder> response) {
                DetailsOrder detailsOrder = response.body();

                if (detailsOrder == null) return;


                DetailsOrderData detailsOrderData = detailsOrder.getData();
                if (detailsOrderData == null) return;

                AddressOrderDetails addressOrderDetails = detailsOrder.getData().getAddress();

                if (addressOrderDetails == null) return;

                invoiceBasics.add(new InvoiceBasic(
                        "Tax", "" + detailsOrderData.getTax()
                ));
                if (addressOrderDetails != null) {
                    invoiceBasics.add(new InvoiceBasic(
                            "Delivery cost", "" + addressOrderDetails.getDelivery_cost()
                    ));
                }
                if (detailsOrderData.getIs_urgent() == 1) {
                    invoiceBasics.add(new InvoiceBasic(
                            "Urgent Price", "" + (detailsOrderData.getSubtotal() * 2)
                    ));
                }

                PromoCodeModel model = detailsOrderData.getPromocode();

                if (model != null) {
                    String defination = null;
                    if (model.getIsPercentage() == 1)
                        defination = model.getAmount() + " %";
                    else
                        defination = model.getAmount() + "";

                    double new_total_amount = model.getAmount();

                    /* invoiceBasics.add(new InvoiceBasic(
                                "PromoCode Discount" + " "+ defination, "" + (order_total_amount - new_total_amount)
                        ));*/

                }

            }

            @Override
            public void onFailure(Call<DetailsOrder> call, Throwable t) {

            }
        });
    }

    private void getOrderID() {
        Call<BasketResponse> call = apiInterface.getBasketOrders(token);
        call.enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, Response<BasketResponse> response) {
                BasketResponse basketResponse = response.body();
                if (basketResponse == null) return;
                order_id = basketResponse.getData().getOrder_id();
                //getPrices(order_id);
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {

            }
        });
    }

    private void addPayment(){
        reqParams.setCurrency("QAR");
        reqParams.setCountry("QA");
        reqParams.setCity("Cairo");
        reqParams.setState("Islam salem State");
        reqParams.setEmail("islam@gmail.com");
        reqParams.setAddress("Islam salem Address");
        reqParams.setName("Islam salem");
        reqParams.setPhone("01112070653");
        reqParams.setAmount(BigDecimal.valueOf(total_amount));
        reqParams.setDescription("Hand 2 Hanger client App");
        reqParams.setReferenceId(String.valueOf(order_id));
        reqParams.setGatewayId("015518711");
        reqParams.setSecretKey("2-vFyVlSWFs6pCD7");
        reqParams.setMode("LIVE");
        reqParams.doPayment();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && data != null)
        {
            String dataString = data.getStringExtra("data");
            //Toast.makeText(PaymentFragment.this, dataString, Toast.LENGTH_SHORT).show();
            //SdkUtils.showAlertPopUp(PaymentFragment.this, dataString);
            sentPayment(dataString);
        }
    }

    private void sentPayment(String dataString) {
        JsonParser parser = new JsonParser();
        JsonElement mJson;
        mJson = parser.parse(dataString);
        Gson gson = new Gson();
        PaymentResponse paymentResponse = gson.fromJson(mJson, PaymentResponse.class);

        if (!paymentResponse.getStatus().equals("success")){
            Toast.makeText(PaymentFragment.this, paymentResponse.getStatus(), Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, Object> payment = new HashMap<>();
        payment.put("order_id", order_id);
        payment.put("amount", total_amount);
        payment.put("cardType", paymentResponse.getCardType());
        payment.put("datetime", paymentResponse.getDatetime());
        payment.put("maskedCardNumber",paymentResponse.getMaskedCardNumber());
        payment.put("reason","<null>");
        payment.put("orderId", paymentResponse.getOrderId());
        payment.put("status", paymentResponse.getStatus());
        payment.put("transactionId", paymentResponse.getTransactionStatus());
        payment.put("transactionStatus", paymentResponse.getTransactionStatus());
        Call<SupportModel> call = apiInterface.setPayment(token, payment);
        call.enqueue(new Callback<SupportModel>() {
            @Override
            public void onResponse(@NonNull Call<SupportModel> call, @NonNull Response<SupportModel> response) {
                SupportModel model = response.body();
                if (model != null){
                    confirmPaymentMethod();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SupportModel> call, @NonNull Throwable t) {

            }
        });
    }
}