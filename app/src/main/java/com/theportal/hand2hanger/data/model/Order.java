package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Order {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("order_id")
    @Expose
    private int order_id;


    @SerializedName("type_id")
    @Expose
    private Integer typeId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;
    @SerializedName("facilities")
    @Expose
    private List<Integer> facilities = null;
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("brand")
    @Expose
    private String brand;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public List<Integer> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Integer> facilities) {
        this.facilities = facilities;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSuccess() {
        return success;
    }

    public int getOrder_id() {
        return order_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}
