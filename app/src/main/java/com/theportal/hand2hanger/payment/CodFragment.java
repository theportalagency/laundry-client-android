package com.theportal.hand2hanger.payment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeActivity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.COD_PAYMENT_METHOD_ID;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_CONFIRMATION;
import static com.theportal.hand2hanger.Utility.Constants.PLACE_ORDER;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class CodFragment extends Fragment {
    @BindView(R.id.checkout_payment_cash)
    TextView checkoutBtn;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;
    private Map<String, Integer> paymentMethodMap = new HashMap<>();
    private NotificationCart notificationCart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cod_payment, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        editor.apply();
        ButterKnife.bind(this, view);
        paymentMethodMap.put("payment_type_id", COD_PAYMENT_METHOD_ID);
        checkoutBtn.setOnClickListener(view1 -> confirmPaymentMethod(paymentMethodMap));
        return view;
    }

    private void confirmPaymentMethod(Map<String, Integer> userData) {
        Call<SignupResponseModel> call = apiInterface.confirmPayment(token, userData);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                if (response.code() == 200) {
                    Toast.makeText(getContext(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    editor.apply();

                    Constants.context.startActivity(new Intent(getActivity(), HomeActivity.class));
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}