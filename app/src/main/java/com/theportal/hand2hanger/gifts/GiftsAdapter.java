package com.theportal.hand2hanger.gifts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.GiftDataResponseModel;

import java.util.List;

public class GiftsAdapter extends RecyclerView.Adapter<GiftsAdapter.MyViewHolder> {
    private static GiftsAdapter.RecyclerViewClickListener mItemListener;
    private final List<GiftDataResponseModel> giftList;
    private Context context;

    public GiftsAdapter(List<GiftDataResponseModel> giftList, Context context, RecyclerViewClickListener mItemListener) {
        this.giftList = giftList;
        this.context = context;
        this.mItemListener = mItemListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.gift_item_rv, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        GiftDataResponseModel giftItem = giftList.get(position);
        holder.giftNameTv.setText(giftItem.getName());
        holder.gift_expiration_tv.setText("From "+giftItem.getFrom()+" to "+giftItem.getTo());
        holder.giftPointsTv.setText(giftItem.getPotins() + " Points");
        holder.giftRedeemBtn.setOnClickListener(view -> mItemListener.onItemRedeemed(view, giftItem.getId(), giftItem.getPotins()));
    }

    @Override
    public int getItemCount() {
        assert giftList != null;
        return giftList.size();
    }

    public interface RecyclerViewClickListener {
        void onItemRedeemed(View v, int position, int needs);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView giftNameTv;
        private TextView giftPointsTv,gift_expiration_tv;
        private Button giftRedeemBtn;

        MyViewHolder(View view) {
            super(view);
            giftNameTv = view.findViewById(R.id.gift_name_tv);
            giftPointsTv = view.findViewById(R.id.gift_points_tv);
            giftRedeemBtn = view.findViewById(R.id.gift_reedem_btn);
            gift_expiration_tv = view.findViewById(R.id.gift_expiration_tv);
        }
    }
}