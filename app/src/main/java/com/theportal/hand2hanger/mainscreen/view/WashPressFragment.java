package com.theportal.hand2hanger.mainscreen.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.tabs.TabLayout;
import com.theportal.hand2hanger.CategriesModel;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.basket.view.BasketFragment;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.FacilityTypesResponse;
import com.theportal.hand2hanger.data.model.FilterItems;
import com.theportal.hand2hanger.data.model.FilterItemsData;
import com.theportal.hand2hanger.data.model.Order;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportal.hand2hanger.data.model.ServiceTypesResponse;
import com.theportal.hand2hanger.data.model.ServicesChilds;
import com.theportal.hand2hanger.data.model.SizesResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.SplitActivity;
import com.theportal.hand2hanger.mainscreen.adapter.FacilitiesChooseAdapter;
import com.theportal.hand2hanger.mainscreen.adapter.FacilitiesChooseTailoringAdapter;
import com.theportal.hand2hanger.mainscreen.adapter.WashItemAdapter;
import com.theportal.hand2hanger.mainscreen.adapter.WashRemoveItemAdapter;
import com.theportalagency.hand2hanger.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.CURRENT_CART_NUMBER;
import static com.theportal.hand2hanger.Utility.Constants.PHONE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TAB_NAME;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class WashPressFragment extends Fragment implements WashItemAdapter.WashItemClickListener,
        WashRemoveItemAdapter.RemoveItemClickListener,
        FacilitiesChooseAdapter.onChoose {

    @BindView(R.id.wash_press_service_rv)
    RecyclerView washPressRvl;
    @BindView(R.id.wash_press_tabLayout)
    TabLayout washPressTabLayout;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.tv_no_wash)
    TextView tv_no_wash;
    @BindView(R.id.ll_wash)
    LinearLayout ll_wash;
    @BindView(R.id.et_instruction)
    TextView et_instruction;
    @BindView(R.id.tv_selected_brand)
    TextView tv_selected_brand;
    @BindView(R.id.tv_selected_facilities)
    TextView tv_selected_facilities;
    @BindView(R.id.tv_selected_color)
    TextView tv_selected_color;
    @BindView(R.id.tv_selected_size)
    TextView tv_selected_size;
    @BindView(R.id.ll_confirm)
    LinearLayout ll_confirm;
    @BindView(R.id.btn_confirm)
    Button btn_confirm;
    @BindView(R.id.nested_wash_press)
    NestedScrollView nested_wash_press;
    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;

    List<FilterItemsData> filterItemsData;
    FacilitiesChooseAdapter chooseAdapter;
    FacilitiesChooseTailoringAdapter facilitiesChooseTailoringAdapter;
    private String color, size;
    private NotificationCart notificationCart;
    private int color_id, size_id, facility_id;
    private WashItemAdapter washItemAdapter;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;
    private Map<String, Integer> serviceTypesParam = new HashMap<>();
    private Map<String, Integer> filter_items = new HashMap<>();
    private List<ServiceTypesResponse> courseItemList;
    private List<SizesResponseModel> sizesList;
    private List<SizesResponseModel> colorsList;
    private List<FacilityTypesResponse> facilitiesList;
    private Map<String, String> facilitiesDate = new HashMap<>();
    private ArrayAdapter<SizesResponseModel> sizeAdapter;
    private ArrayAdapter<SizesResponseModel> colorAdapter;
    private ArrayAdapter<FacilityTypesResponse> facilityAdapter;
    private Map<String, Object> orderItemMap = new HashMap<>();
    private WashRemoveItemAdapter washRemoveItemAdapter;
    private OrderItem mItem;
    private List<Integer> facility_choose_list = new ArrayList<>();
    private List<String> facility_choose_list_names = new ArrayList<>();
    private int category_id, service_id_bundle;
    private int count;
    AlertDialog addItemAlertDialog;
    private String status = "", phoneNumber;
    private ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragmnet_wash_press, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        phoneNumber = pref.getString(PHONE_KEY, phoneNumber);
        if (phoneNumber == null) {
            Toast.makeText(Constants.context, "Please Complete Your Profile First", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Constants.context, SplitActivity.class);
            intent.putExtra("extra", "edit");
            startActivity(intent);
        }
        dialog = new ProgressDialog(Constants.context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Wait");
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            service_id_bundle = bundle.getInt("service_id");
            setToolbarText(service_id_bundle);
        }
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        facilitiesDate.put("service_id", String.valueOf(service_id_bundle));

        notificationCart = new NotificationCart(counterPanel_);
        getCategories();
        counterPanel_.setOnClickListener(view1 -> openBasket());
        return view;
    }

    private void openBasket() {
        Constants.loadFragment(Constants.context, R.id.frame_layout, new BasketFragment());
    }

    private void setToolbarText(int service) {
        switch (service) {
            case 1:
                tv_toolbar_inside.setText(R.string.wash_press);
                break;
            case 2:
                tv_toolbar_inside.setText(R.string.dry_clean);
                break;
            case 3:
                tv_toolbar_inside.setText(R.string.steam_press);
                break;
            case 4:
                tv_toolbar_inside.setText(R.string.tailoring);
                break;
        }
    }

    private void getClothesFacilitiesChilds(RecyclerView rv, String type, String service, String category) {
        Map<String, String> items = new HashMap<>();
        items.put("type_id", type);
        items.put("service_id", service);
        items.put("category_id", category);
        Call<List<ServicesChilds>> call = apiInterface.service_type_tailoring(token, items);
        call.enqueue(new Callback<List<ServicesChilds>>() {
            @Override
            public void onResponse(Call<List<ServicesChilds>> call, @NonNull Response<List<ServicesChilds>> response) {
                List<ServicesChilds> typesResponses = response.body();
                if (typesResponses == null) return;
                List<FacilityTypesResponse> facilityChilds = new ArrayList<>();
                for (int i = 0; i < typesResponses.size(); i++) {
                    String price = typesResponses.get(i).getPrice();
                    if (!price.equals("")) {
                        facilityChilds.add(new FacilityTypesResponse(
                                typesResponses.get(i).getId(), typesResponses.get(i).getName(),
                                Integer.parseInt(price), -1
                        ));
                    }
                }
                chooseAdapter = new FacilitiesChooseAdapter(Constants.context, facilityChilds,
                        WashPressFragment.this, true);
                rv.setAdapter(chooseAdapter);
            }

            @Override
            public void onFailure(Call<List<ServicesChilds>> call, Throwable t) {

            }
        });
    }

    private void getCategories() {
        Call<List<CategriesModel>> call = apiInterface.categories(token);
        call.enqueue(new Callback<List<CategriesModel>>() {
            @Override
            public void onResponse(Call<List<CategriesModel>> call, Response<List<CategriesModel>> response) {
                List<CategriesModel> models = response.body();
                if (models == null) return;
                category_id = models.get(0).getId();
                for (int i = 0; i < models.size(); i++) {
                    washPressTabLayout.addTab(washPressTabLayout.newTab().setText("" + models.get(i).getName()));
                }
                TAB_NAME = models.get(0).getName();
                animationView.setVisibility(View.GONE);
                animationView.cancelAnimation();
                nested_wash_press.setVisibility(View.VISIBLE);
                washPressTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        TAB_NAME = tab.getText().toString();
                        category_id = models.get(tab.getPosition()).getId();
                        Log.v("bdsjnkskadbskda", "tab pos = " + category_id);
                        serviceTypesParam.put("service_id", service_id_bundle);
                        serviceTypesParam.put("category_id", category_id);
                        getServiceTypes(serviceTypesParam);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
                serviceTypesParam.put("service_id", service_id_bundle);
                serviceTypesParam.put("category_id", category_id);
                getServiceTypes(serviceTypesParam);
            }

            @Override
            public void onFailure(Call<List<CategriesModel>> call, Throwable t) {
                ll_noConnection.setVisibility(View.VISIBLE);
                animationView.setVisibility(View.GONE);
            }
        });
    }

    private void getServiceTypes(Map<String, Integer> userData) {
        Call<List<ServiceTypesResponse>> call = apiInterface.getServiceTypes(token, userData);
        call.enqueue(new Callback<List<ServiceTypesResponse>>() {
            @Override
            public void onResponse(Call<List<ServiceTypesResponse>> call, Response<List<ServiceTypesResponse>> response) {
                if (response.code() == 200) {
                    List<ServiceTypesResponse> services = response.body();
                    if (services == null) return;
                    updateUi(services);
                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<ServiceTypesResponse>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void updateUi(List<ServiceTypesResponse> body) {
        if (body.size() == 0) {
            ll_wash.setVisibility(View.GONE);
            tv_no_wash.setVisibility(View.VISIBLE);
            return;
        }
        ll_wash.setVisibility(View.VISIBLE);
        tv_no_wash.setVisibility(View.GONE);

        washPressRvl.setLayoutManager(new LinearLayoutManager(getActivity()));
        washItemAdapter = new WashItemAdapter(body, getActivity());
        washPressRvl.setHasFixedSize(true);
        washPressRvl.setAdapter(washItemAdapter);
        washItemAdapter.setWashItemClickListener(this);
    }

    @Override
    public void onItemDeleted(int position, int id, int service_id, int num, int order_id,int itemId) {
        count = num;
        if (count == 0)
            return;

        orderItemMap.clear();

        if (service_id_bundle == 4)
            orderItemMap.put("service_id", service_id);
        else
            orderItemMap.put("service_id", service_id_bundle);

        orderItemMap.put("type_id", id);
        orderItemMap.put("category_id", category_id);
        orderItemMap.put("order_id", order_id);
        /*if ((count - 1) == 0) {
            removeItemFromBasket(itemId,order_id,position);
        } else*/
            deleteOrderItem(orderItemMap, position);
    }

    private void deleteOrderItem(Map<String, Object> orderItemMap, int pos) {
        dialog.show();
        Call<Order> call = apiInterface.decrement_order(token, orderItemMap);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    Order order = response.body();
                    if (order == null) return;
                    washItemAdapter.decreaseCount(count, pos);
                    ll_confirm.setVisibility(View.GONE);
                    getBasketItems(getView());
                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getRemoveItems(RecyclerView washPressDeleteItem_rv, int item_id,
                                int service_id, TextView tv_no_item_remove,
                                Button button) {
        filter_items.put("type_id", item_id);
        filter_items.put("service_id", service_id);
        filter_items.put("category_id", category_id);
        Call<FilterItems> call = apiInterface.get_items_by_filters(token, filter_items);
        call.enqueue(new Callback<FilterItems>() {
            @Override
            public void onResponse(Call<FilterItems> call, Response<FilterItems> response) {
                FilterItems filter = response.body();
                if (filter == null) return;
                filterItemsData = filter.getData();
                if (filterItemsData == null) return;
                if (filterItemsData.size() == 0) {
                    tv_no_item_remove.setVisibility(View.VISIBLE);
                    button.setEnabled(false);
                }
                updateDeleteUi(filterItemsData, washPressDeleteItem_rv);
            }

            @Override
            public void onFailure(Call<FilterItems> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateDeleteUi(List<FilterItemsData> itemList, RecyclerView washPressDeleteItem_rv) {
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        washPressDeleteItem_rv.setLayoutManager(layoutManager2);
        washRemoveItemAdapter = new WashRemoveItemAdapter(itemList, getActivity());
        washPressDeleteItem_rv.setHasFixedSize(true);
        washPressDeleteItem_rv.setAdapter(washRemoveItemAdapter);
        washRemoveItemAdapter.setRemoveItemClickListener(this);
    }

    private void startCreateOrderId(Map<String, Object> orderItemMap, int pos) {
        dialog.show();
        Call<Order> call = apiInterface.sendOrderDetails(token, orderItemMap);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    Toast.makeText(getContext(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    //notificationCart.increaseNumber();
                    getBasketItems(getView());
                    washItemAdapter.increaseCount(pos, count);
                    ll_confirm.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void incrementOrder(Map<String, Object> orderItemMap, int pos) {
        dialog.show();
        Call<Order> call = apiInterface.increment_order(token, orderItemMap);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                dialog.dismiss();
                if (response.code() == 200) {
                    Toast.makeText(Constants.context, response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    getBasketItems(getView());
                    washItemAdapter.increaseCount(pos, count);
                    ll_confirm.setVisibility(View.GONE);
                } else {
                    Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showItemDialog(int pos, int item_id, Integer order_id) {
        AlertDialog addItemAlertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.add_item_dialog)
                .show();
        addItemAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Map<String, Object> orderItemMap = new HashMap<>();
        Spinner colorSpinner = addItemAlertDialog.findViewById(R.id.color_spinner);
        Spinner sizeSpinner = addItemAlertDialog.findViewById(R.id.size_spinner);
        EditText brandEditText = addItemAlertDialog.findViewById(R.id.brand_et);

        RecyclerView rv_facility = addItemAlertDialog.findViewById(R.id.rv_facility);
        if (colorsList != null && colorsList.size() != 0)
            colorAdapter = new ArrayAdapter<>(Constants.context, R.layout.spinner_item, colorsList);

        if (sizesList != null && sizesList.size() != 0)
            sizeAdapter = new ArrayAdapter<>(Constants.context, R.layout.spinner_item, sizesList);


        getClothesFacilitiesChilds(rv_facility, String.valueOf(item_id),
                String.valueOf(service_id_bundle), String.valueOf(category_id));

        colorSpinner.setAdapter(colorAdapter);
        sizeSpinner.setAdapter(sizeAdapter);
        rv_facility.setHasFixedSize(true);
        rv_facility.setNestedScrollingEnabled(false);
        rv_facility.setLayoutManager(new LinearLayoutManager(Constants.context));
        rv_facility.setAdapter(chooseAdapter);

        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                color = colorsList.get(position).getName();
                color_id = colorsList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                size = sizesList.get(position).getName();
                size_id = sizesList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Button doneBtn = addItemAlertDialog.findViewById(R.id.done_btn);
        doneBtn.setOnClickListener(v -> {
            if (service_id_bundle == 4 && facility_choose_list.size() == 0) {
                Toast.makeText(Constants.context, "please choose one service first", Toast.LENGTH_SHORT).show();
                return;
            }

            String brandName = brandEditText.getText().toString();
            tv_selected_size.setText(size);
            tv_selected_brand.setText(brandName);
            tv_selected_color.setText(color);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < facility_choose_list_names.size(); i++) {
                sb.append(facility_choose_list_names.get(i));
            }
            tv_selected_facilities.setText(sb.toString());

            orderItemMap.put("type_id", item_id);
            orderItemMap.put("category_id", category_id);
            orderItemMap.put("service_id", facility_choose_list.get(0));

            facility_choose_list.clear();
            orderItemMap.put("count", (count + 1));
            startCreateOrderId(orderItemMap,pos);
           /* if (count == 0){

            }
            else {
                orderItemMap.put("order_id", order_id);
                incrementOrder(orderItemMap, pos);
            }*/
            addItemAlertDialog.dismiss();
        });
    }

    @Override
    public void onItemAdded(int position, int item_id, int num, Integer order_id) {
        count = num;
        if (service_id_bundle == 4) {
            showItemDialog(position, item_id,order_id);
        } else {
            orderItemMap.put("service_id", service_id_bundle);
            orderItemMap.put("type_id", item_id);
            orderItemMap.put("category_id", category_id);
            if (count == 0){
                orderItemMap.put("count", (count + 1));
                startCreateOrderId(orderItemMap,position);
            }
            else {
                orderItemMap.put("order_id", order_id);
                incrementOrder(orderItemMap, position);
            }
        }
    }

    private void removeItemFromBasket(int itemId, int orderId, int pos) {
        orderItemMap.put("item_id", itemId);
        orderItemMap.put("order_id", orderId);
        Call<OrderItem> call = apiInterface.deleteOrderItem(token, orderItemMap);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(Call<OrderItem> call, Response<OrderItem> response) {
                if (response.code() == 200) {
                    Toast.makeText(getContext(), "Order remove Successfully", Toast.LENGTH_LONG).show();
                    getBasketItems(getView());
                    washItemAdapter.decreaseCount(count, pos);
                    if (addItemAlertDialog != null)
                        addItemAlertDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<OrderItem> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRemoveItemClick(int position, int order_id, int id) {
        AlertDialog deleteItemAlertDialog = new AlertDialog.Builder(getActivity())
                .setView(R.layout.delete_basket_item)
                .show();
        deleteItemAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button deleteItemBtn = deleteItemAlertDialog.findViewById(R.id.delete_done_btn);
        Button cancelItemBtn = deleteItemAlertDialog.findViewById(R.id.delete_cancel_btn);
        deleteItemBtn.setOnClickListener(v -> {
            removeItemFromBasket(id, order_id, position);
            deleteItemAlertDialog.dismiss();
        });
        cancelItemBtn.setOnClickListener(v -> deleteItemAlertDialog.dismiss());

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }

    @Override
    public void onItemChoose(boolean check, int id, String name, int amount, int is_precentage) {
        if (check) {
            facility_choose_list.clear();
            facility_choose_list_names.clear();
            facility_choose_list.add(id);
            facility_choose_list_names.add(name);
        } else {
            facility_choose_list.clear();
            facility_choose_list_names.clear();
        }
    }

    private void getBasketItems(View v) {
        Call<BasketResponse> call = apiInterface.getBasketOrders(token);
        call.enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, Response<BasketResponse> response) {
                if (response.isSuccessful()) {
                    CURRENT_CART_NUMBER = 0;
                    notificationCart = new NotificationCart(v.findViewById(R.id.counterPanel_));
                    BasketResponse basketResponse = response.body();
                    if (basketResponse == null) {
                        return;
                    }
                    if (basketResponse.getData() == null) {
                        return;
                    }
                    CURRENT_CART_NUMBER = basketResponse.getData().getItems().size();
                    notificationCart = new NotificationCart(v.findViewById(R.id.counterPanel_));
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }
}