package com.theportal.hand2hanger.data.network;

public class TestSendSupportBody {

    private String title,description;

    public TestSendSupportBody(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
