package com.theportal.hand2hanger.tracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.github.vipulasri.timelineview.TimelineView;
import com.theportal.hand2hanger.Utility.OnBackPressed;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.ORDER_ID;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class TrackingFragment extends Fragment implements OnBackPressed {

    @BindView(R.id.fifth_phase_iv)
    ImageButton fifthPhaseIv;
    @BindView(R.id.first_phase_iv)
    ImageButton firstPhaseIv;
    @BindView(R.id.third_phase_iv)
    ImageButton thirdPhaseIv;
    @BindView(R.id.rv_timeline)
    RecyclerView rv_timeline;
    @BindView(R.id.invoice_id_tv)
    TextView invoiceIdTv;
    @BindView(R.id.fourth_phase_iv)
    ImageButton fourthPhaseIv;
    @BindView(R.id.second_phase_iv)
    ImageButton secondPhaseIv;

    @BindView(R.id.tv_DeliveryTime_tracking)
    TextView tv_DeliveryTime_tracking;
    @BindView(R.id.tv_DeliveryData_tracking)
    TextView tv_DeliveryData_tracking;
    @BindView(R.id.tv_pickupData_tracking)
    TextView tv_pickupData_tracking;
    @BindView(R.id.tv_pickupTime_tracking)
    TextView tv_pickupTime_tracking;

    private Map<String, Object> orderIdMap = new HashMap<>();
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;
    private int orderId;
    private String order_no;
    private TimeLineAdapter timeLineAdapter;
    private List<TimeLineMdel> timeLineMdels = new ArrayList<>();
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    LinearLayoutManager linearLayoutManager;
    private String pickupTime,pickup_d,delivery_t,delivery_d;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_shipment, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.invoice);
        counterPanel_.setVisibility(View.GONE);
        toolbar.setNavigationOnClickListener(view1 -> {
            getFragmentManager().popBackStack();
        });
        linearLayoutManager = new LinearLayoutManager(Constants.context);
        rv_timeline.setHasFixedSize(true);
        rv_timeline.setNestedScrollingEnabled(false);
        rv_timeline.setLayoutManager(linearLayoutManager);

        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv_timeline.getContext(),
                linearLayoutManager.getOrientation());
        rv_timeline.addItemDecoration(dividerItemDecoration);*/

        orderId = getArguments() != null ? getArguments().getInt(ORDER_ID) : 0;
        order_no = getArguments() != null ? getArguments().getString("order_no") : "";
        pickupTime = getArguments() != null ? getArguments().getString("pickup_t") : "";
        pickup_d = getArguments() != null ? getArguments().getString("pickup_d") : "";
        delivery_t = getArguments() != null ? getArguments().getString("delivery_t") : "";
        delivery_d = getArguments() != null ? getArguments().getString("delivery_d") : "";

        invoiceIdTv.setText("INV # " + order_no);
        tv_pickupData_tracking.setText(pickup_d);
        tv_DeliveryData_tracking.setText(delivery_d);
        tv_pickupTime_tracking.setText(pickupTime);
        tv_DeliveryTime_tracking.setText(delivery_t);
        updateUi(orderId);
        return view;
    }

    private void updateUi(int status) {
        switch (status) {
            case 3:
                changeFirstPhaseUi();
                break;
            case 5:
                changeFirstPhaseUi();
                break;
            case 6:
                changeSecondPhaseUi();
                break;
            case 7:
                changeThirdPhaseUi();
                break;
            case 8:
                changeFourthPhaseUi();
                break;
            case 9:
                changeFifthPhaseUi();
                break;
        }
        timeLineAdapter = new TimeLineAdapter(Constants.context, timeLineMdels);
        rv_timeline.setAdapter(timeLineAdapter);
    }

    private void changeThirdPhaseUi() {
        firstPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_first_phase_tracking));
        secondPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_secondphase_tracking));
        thirdPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_third_phase_tracking));
        fourthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fourth_phase_tracking_white));
        fifthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fifth_phase_tracking_white));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
    }

    private void changeFourthPhaseUi() {
        firstPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_first_phase_tracking));
        secondPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_secondphase_tracking));
        thirdPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_third_phase_tracking));
        fourthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fourth_phase_tracking));
        fifthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fifth_phase_tracking_white));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(1));
    }

    private void changeFifthPhaseUi() {
        firstPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_first_phase_tracking));
        secondPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_secondphase_tracking));
        thirdPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_third_phase_tracking));
        fourthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fourth_phase_tracking));
        fifthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fifth_phase_tracking));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
    }

    private void changeSecondPhaseUi() {
        firstPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_first_phase_tracking));
        secondPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_secondphase_tracking));
        thirdPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_third_phase_tracking_white));
        fourthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fourth_phase_tracking_white));
        fifthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fifth_phase_tracking_white));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
    }

    private void changeFirstPhaseUi() {
        firstPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_first_phase_tracking));
        secondPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_secondphase_tracking_white));
        thirdPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_third_phase_tracking_white));
        fourthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fourth_phase_tracking_white));
        fifthPhaseIv.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_fifth_phase_tracking_white));
        timeLineMdels.add(new TimeLineMdel(0));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
        timeLineMdels.add(new TimeLineMdel(1));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }

    @Override
    public void onBackPressed() {
        Objects.requireNonNull(getFragmentManager()).popBackStack();
    }
}
