package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsOrder {

    @SerializedName("data")
    @Expose
    private DetailsOrderData data;

    public DetailsOrderData getData() {
        return data;
    }
}
