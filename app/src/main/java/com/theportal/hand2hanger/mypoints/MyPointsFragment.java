package com.theportal.hand2hanger.mypoints;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.theportal.hand2hanger.data.model.RedeemsPoints;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.PointsResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.gifts.GiftsFragment;

import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.POINTS_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class MyPointsFragment extends Fragment {
    @BindView(R.id.redeem_points_btn)
    Button redeemPointsBtn;
    @BindView(R.id.user_points_tv)
    TextView userPointsTv;

    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    @BindView(R.id.tv_noresult_points)
    TextView no_result_found;

    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;

    @BindView(R.id.mypoints_rv)
    RecyclerView mypoints_rv;

    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;


    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypoints, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, view);
        mypoints_rv.setHasFixedSize(true);
        mypoints_rv.setNestedScrollingEnabled(false);
        mypoints_rv.setLayoutManager(new LinearLayoutManager(Constants.context));
        updateUi();
        getReedems();
        counterPanel_.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.points);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        redeemPointsBtn.setOnClickListener(view1 -> loadFragment(new GiftsFragment()));
        return view;
    }

    private void updateUi() {
        getPoints();
    }

    private void getPoints() {
        Call<PointsResponseModel> call = apiInterface.getUserPoints(token);
        call.enqueue(new Callback<PointsResponseModel>() {
            @Override
            public void onResponse(Call<PointsResponseModel> call, Response<PointsResponseModel> response) {
                PointsResponseModel points = response.body();
                if (points == null)
                    return;
                if (response.code() == 200) {
                    userPointsTv.setText("Total Points: " + points.getPoints());
                    editor.putInt(POINTS_KEY,points.getPoints());
                    editor.apply();
                } else {
                    Toast.makeText(getContext(), Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PointsResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void getReedems() {
        Call<RedeemsPoints> call = apiInterface.getReddems(token);
        call.enqueue(new Callback<RedeemsPoints>() {
            @Override
            public void onResponse(Call<RedeemsPoints> call, Response<RedeemsPoints> response) {
                RedeemsPoints points = response.body();
                if (points == null)
                    return;

                if (points.getData().size() == 0){
                    no_result_found.setVisibility(View.VISIBLE);
                    return;
                }

                MyGiftsAdapter myGiftsAdapter = new MyGiftsAdapter(Constants.context,points.getData());
                mypoints_rv.setAdapter(myGiftsAdapter);
            }

            @Override
            public void onFailure(Call<RedeemsPoints> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }


    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}