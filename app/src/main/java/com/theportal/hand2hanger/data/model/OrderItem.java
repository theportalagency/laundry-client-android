package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderItem {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("count")
    @Expose
    private Integer count;

    @SerializedName("order_id")
    @Expose
    private int order_id;

    @SerializedName("amount")
    @Expose
    private Double amount;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    @SerializedName("unit_cost")
    @Expose
    private Integer unitCost;

    @SerializedName("total_amount")
    @Expose
    private Double totalAmount;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("color")
    @Expose
    private String color;

    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("type_id")
    @Expose
    private int type_id;

    @SerializedName("service_id")
    @Expose
    private int service_id;

    @SerializedName("category_id")
    @Expose
    private int category_id;

    @SerializedName("facility")
    @Expose
    private Facility facilities = null;


    @SerializedName("facility_name")
    @Expose
    private String facility_name;


    public OrderItem(Integer id, int order_id, Double amount, Integer quantity, Integer unitCost,
                     Double totalAmount, String type, String service, String color, String size,
                     String brand, int type_id, int service_id, int category_id,
                     Facility facilities, String facility_name) {
        this.id = id;
        this.order_id = order_id;
        this.amount = amount;
        this.quantity = quantity;
        this.unitCost = unitCost;
        this.totalAmount = totalAmount;
        this.type = type;
        this.service = service;
        this.color = color;
        this.size = size;
        this.brand = brand;
        this.type_id = type_id;
        this.service_id = service_id;
        this.category_id = category_id;
        this.facilities = facilities;
        this.facility_name = facility_name;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public int getType_id() {
        return type_id;
    }

    public int getService_id() {
        return service_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public int getOrder_id() {
        return order_id;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Facility getFacilities() {
        return facilities;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Integer unitCost) {
        this.unitCost = unitCost;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

}
