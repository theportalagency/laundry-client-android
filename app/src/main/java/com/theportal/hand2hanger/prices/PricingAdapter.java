package com.theportal.hand2hanger.prices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.PricingTypes;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PricingAdapter extends RecyclerView.Adapter<PricingAdapter.MyViewHolder> {
    private final List<PricingTypes> courseItemList;
    private Context context;
//    private int categoryID;

    public PricingAdapter(List<PricingTypes> courseItemList, Context context) {
        this.courseItemList = courseItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.pricing_item_list_rv, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PricingTypes courseItem = courseItemList.get(position);
        holder.itemType.setText(courseItem.getName());
        if (courseItem.getPrice() == null) {
            holder.itemCost.setText(("-"));

        } else {
            holder.itemCost.setText((courseItem.getPrice() + " QR"));
        }
    }


    @Override
    public int getItemCount() {
        assert courseItemList != null;
        return courseItemList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_type)
        TextView itemType;
        @BindView(R.id.item_cost)
        TextView itemCost;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}