package com.theportal.hand2hanger.authentication.login.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hbb20.CountryCodePicker;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.forget_password.ForgetPassFragment;
import com.theportal.hand2hanger.authentication.phone_auth.ui.PhoneCollectFragment;
import com.theportal.hand2hanger.authentication.signup.ui.SignUpFragment;
import com.theportal.hand2hanger.data.model.LoginResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeActivity;
import com.theportal.hand2hanger.notifications.FirebaseMessageService;
import com.theportalagency.hand2hanger.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.provider.Settings.Secure;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.plus.Plus.API;
import static com.theportal.hand2hanger.Utility.Constants.DATE_OF_BIRTH_KEY;
import static com.theportal.hand2hanger.Utility.Constants.EMAIL_KEY;
import static com.theportal.hand2hanger.Utility.Constants.GENDER_KEY;
import static com.theportal.hand2hanger.Utility.Constants.IMAGE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.NAME_KEY;
import static com.theportal.hand2hanger.Utility.Constants.PHONE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class LoginFragment extends Fragment implements android.text.TextWatcher,
        View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.not_amember_tv)
    TextView already_member;
    @BindView(R.id.signup_btn)
    Button nextBtn;
    @BindView(R.id.loginFacebookButton)
    RelativeLayout loginButton;
    @BindView(R.id.sign_in_button)
    RelativeLayout sign_in_button;
    @BindView(R.id.login_button_fb)
    LoginButton login_button_fb;
    @BindView(R.id.email_et)
    EditText emailEt;
    @BindView(R.id.password_et)
    EditText passwordEt;
    @BindView(R.id.tv_forget)
    TextView tv_forget;
    @BindView(R.id.ccp_login)
    CountryCodePicker ccp;
    private FirebaseAuth mAuth;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String TAG = "AMMMMMMMMMMMMMMMMMR";
    private CallbackManager mCallbackManager;
    private Map<String, String> userData = new HashMap<>();
    private boolean isValidEmail, isValidPass;
    private CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    private static final String EMAIL = "email";
    private String android_id;
    //gmail
    private String idToken, name, email;
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 1;
    String device_token,code;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private SignInButton signInButton;
    private ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mCallbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        emailEt.addTextChangedListener(this);
        passwordEt.addTextChangedListener(this);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        editor.apply();
        dialog = new ProgressDialog(Constants.context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Wait");

        code = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(() -> {
            code = ccp.getSelectedCountryCodeWithPlus();
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode("737039249671-qjtp8grfv3pv1aeult2eloqivns8e079.apps.googleusercontent.com")
                .requestEmail()
                .build();
        try {
            googleApiClient = new GoogleApiClient.Builder(Constants.context)
                    .enableAutoManage(getActivity(), this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Plus.API)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        nextBtn.setOnClickListener(view1 -> validateInputs());
        tv_forget.setOnClickListener(this);
        already_member.setOnClickListener(v -> loadFragment(new PhoneCollectFragment()));
        callbackManager = CallbackManager.Factory.create();
        fbInit();

        /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), instanceIdResult -> {
            device_token = instanceIdResult.getToken();
        });*/

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.e("Firebase", "getInstanceId failed", task.getException());
                        return;
                    }
                    device_token = Objects.requireNonNull(task.getResult()).getToken();
                });

        gmailInit(view);
        return view;
    }

    private void gmailInit(View v) {
        sign_in_button.setOnClickListener(view -> gmailLogin());
    }

    private void gmailLogin() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void fbInit() {
        FacebookSdk.sdkInitialize(Constants.context);
        login_button_fb.setFragment(this);
        login_button_fb.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.setOnClickListener(view -> login_button_fb.performClick());
        login_button_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                String token = accessToken.getToken();
                Log.v("TOKENFACEEEBOOK", token);
            }
            @Override
            public void onCancel() {
                Toast.makeText(Constants.context, "fe problem", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(Constants.context, "error", Toast.LENGTH_SHORT).show();
            }
        });
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        AccessToken accessToken = loginResult.getAccessToken();
                        String token = accessToken.getToken();
                        Log.v("TOKENNNNNNNNNNNNNNNNN", token);
                    }
                    @Override
                    public void onCancel() {
                        Log.v("TOKEN", "cancel2");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.v("TOKEN", "error2");
                    }
                });
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                if (currentAccessToken == null) return;
                if (currentAccessToken.getToken() != null) {
                    registerAsFb(currentAccessToken.getToken(), "facebook");
                }
            }
        };
    }

    private void registerAsFb(String token, String provider) {
        Map<String, String> socialData = new HashMap<>();
        socialData.put("provider", provider);
        socialData.put("token", token);
        socialData.put("password", "123456");
        socialData.put("device_id", device_token);
        Call<LoginResponseModel> call = apiInterface.social_login(socialData);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                LoginResponseModel login = response.body();
                if (login == null) return;
                String access_token = login.getAccessToken();
                String token_type = login.getTokenType();
                String name = login.getName();
                String email = login.getEmail();
                String dob = login.getDate_of_birth();
                String phone = login.getPhone();
                String gender = login.getGender();
                String photo = login.getImage();
                editor.putString(TOKEN_KEY, (token_type + " " + access_token));
                editor.putString(NAME_KEY, name);
                editor.putString(IMAGE_KEY, photo);
                editor.putString(EMAIL_KEY, email);
                editor.putString(DATE_OF_BIRTH_KEY, dob);
                editor.putString(PHONE_KEY, phone);
                editor.putString(GENDER_KEY, gender);
                editor.apply();
                startApplication();
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

            }
        });
    }

    private void validateInputs() {

        String email = emailEt.getText().toString();
        String pass = passwordEt.getText().toString();

        if (pass.equals("")) {
            passwordEt.setError("Please Type Password");
            return;
        }

        if (email.equals("")) {
            passwordEt.setError("Please Type Email or Phone");
            return;
        }

        boolean isPhone = isNumeric(email);

        if (isPhone){
            email = code+email;
        }

        userData.put("email", email);
        userData.put("password", pass);
        userData.put("device_id", device_token);

        loginUser(userData);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (Objects.requireNonNull(Objects.requireNonNull(getActivity()).getCurrentFocus()).getId()
                == R.id.email_et) {
            Log.e("email_et", String.valueOf(isValidEmail));
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void loginUser(Map<String, String> userData) {
        dialog.show();
        Call<LoginResponseModel> call = apiInterface.getLoginResponse(userData);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                clearEt();
                dialog.dismiss();
                LoginResponseModel login = response.body();
                if (login == null) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), R.string.error_valid, Toast.LENGTH_SHORT).show();
                    return;
                }
                String access_token = login.getAccessToken();
                String token_type = login.getTokenType();
                String name = login.getName();
                String email = login.getEmail();
                String dob = login.getDate_of_birth();
                String phone = login.getPhone();
                String gender = login.getGender();
                String photo = login.getImage();
                editor.putString(TOKEN_KEY, (token_type + " " + access_token));
                editor.putString(NAME_KEY, name);
                editor.putString(IMAGE_KEY, photo);
                editor.putString(EMAIL_KEY, email);
                editor.putString(DATE_OF_BIRTH_KEY, dob);
                editor.putString(PHONE_KEY, phone);
                editor.putString(GENDER_KEY, gender);
                editor.apply();
                startApplication();
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void startApplication() {
        Intent intent = new Intent(Constants.context, HomeActivity.class);
        Constants.context.startActivity(intent);
        getActivity().finish();
    }

    private void clearEt() {
        passwordEt.getText().clear();
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm != null ? fm.beginTransaction() : null;
        Objects.requireNonNull(fragmentTransaction).add(R.id.intro_slider_container, fragment).addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            //Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            //handleSignInResult(task);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                final GoogleSignInAccount account = result.getSignInAccount();
                Runnable runnable = () -> {
                    try {
                        String scope = "oauth2:" + Scopes.EMAIL + " " + Scopes.PROFILE;

                        String accessToken = GoogleAuthUtil.getToken(Constants.context,
                                account.getAccount(), scope, new Bundle());
                        Log.d(TAG, "accessToken:" + accessToken);
                        registerAsFb(accessToken, "google");
                    } catch (IOException e) {
                        Toast.makeText(Constants.context, "pls try again", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } catch (GoogleAuthException e) {
                        e.printStackTrace();
                        Toast.makeText(Constants.context, "pls try again", Toast.LENGTH_SHORT).show();
                    }
                };
                AsyncTask.execute(runnable);
            } else {
            }

        }


        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_forget:
                loadFragment(new ForgetPassFragment());
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.stopAutoManage(getActivity());
            googleApiClient.disconnect();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
}