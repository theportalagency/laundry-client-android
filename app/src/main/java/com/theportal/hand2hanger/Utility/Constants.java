package com.theportal.hand2hanger.Utility;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.PhoneAuthProvider;
import com.theportalagency.hand2hanger.R;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Constants {
    static public final String USER_PHONE_NUMBER_KEY = "user_phone";
    static public final String USER_SHARED_PREFERENCES_KEY = "UserDataPreferences";
    static public final String BASE_URL = "http://hand2hanger.com/";
    static public final int WASH_ID = 1;
    static public final int DRY_CLEAN_ID = 2;
    static public final int STREAM_ID = 3;
    static public final int TAILORING_ID = 4;
    //WASH PRESS CATEGORY
    static public final int MEN_CATEGORY_ID = 1;
    //Address Type ID
    static public final int WOMEN_CATEGORY_ID = 2;
    static public final int CHILDREN_CATEGORY_ID = 3;
    static public final int HOUSE_CATEGORY_ID = 4;
    static public final int COD_PAYMENT_METHOD_ID = 1;

    //Shared Preferences
    static public final String TOKEN_KEY = "" + "token";
    static public final String NAME_KEY = "name";
    static public final String EMAIL_KEY = "email";
    static public final String PHONE_KEY = "phone";
    static public final String DATE_OF_BIRTH_KEY = "date_of_birth";
    static public final String GENDER_KEY = "gender";
    static public final String IMAGE_KEY = "image";

    //for reservation
    static public final String ADD_ORDER = "add_order";
    static public final String UPDATE_ORDER = "update_order";
    static public final String PLACE_ORDER = "place_order";
    static public final String ORDER_CONFIRMATION = "order_confirmation";


    static public double DELIVERY_FEE = 0;


    //Gender Type
    static public final int GENDER_MALE_ID = 1;
    static public final int GENDER_FEMALE_ID = 2;

    static public final String POINTS_KEY = "points";
    static public final String ORDER_ID = "ORDER_ID";
    static public final String ORDER_COLLECTION_DATE = "ORDER_COLLECTION_DATE";
    static public final String ORDER_COLLECTION_TIME = "ORDER_COLLECTION_TIME";
    static public final String ORDER_ADDRESS = "ORDER_ADDRESS";
    static public final String ORDER_AMOUNT = "ORDER_AMOUNT";
    static public int CURRENT_CART_NUMBER;
    static public String NUMBER = "";
    //Type ID
    static public int WASH_TYPE_ID;
    static public int DRY_TYPE_ID;
    static public int STREAM_TYPE_ID;
    static public int TAILORING_TYPE_ID;
    static public int HOME_ADDRESS_TYPE_ID = 1;
    static public int OFFICE_ADDRESS_TYPE_ID = 2;
    static public int OTHER_ADDRESS_TYPE_ID = 3;

    public static Context context;
    public static Activity activity;
    public static int FRAGMENT_NUMBER = 0;
    public static String TAB_NAME = "";
    public static String mVerificationId;
    public static int TAB_ID = 1;
    public static int SERVICE_ID = 1;
    public static PhoneAuthProvider.ForceResendingToken mResendToken;


    public static Double LAT,LNG;

    public static int TAB_ID_SELECT = 1;

    public static void loadFragment(Context context, @IdRes int res, Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(res, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    public static void loadFragmentADD(Context context, @IdRes int res, Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(res, fragment).addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    public static double roundAvoid(double value) {
        if ((value % 1) == 0) {
            return value;
        } else
            return Math.round(10.0 * value) / 10.0;
    }

    public static void animateFragmentAuthActivity(Context cxt, Fragment fragment) {
        ((FragmentActivity) cxt).getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,
                        R.anim.exit_to_right, R.anim.exit_to_right)
                .add(R.id.intro_slider_container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }
}
