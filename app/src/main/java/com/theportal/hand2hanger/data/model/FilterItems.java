package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterItems {

    @SerializedName("data")
    @Expose
    private List<FilterItemsData> data;

    public List<FilterItemsData> getData() {
        return data;
    }
}
