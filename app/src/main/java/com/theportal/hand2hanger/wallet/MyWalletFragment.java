package com.theportal.hand2hanger.wallet;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.theportal.hand2hanger.data.model.WalletModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.WalletModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.SupportModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class MyWalletFragment extends Fragment {
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;

    @BindView(R.id.remaining_balance_tv)
    TextView remaining_balance_tv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mywallet, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, view);
        getBalance();
        return view;
    }

    private void getBalance() {
        Call<WalletModel> call = apiInterface.wallet_balance(token );
        call.enqueue(new Callback<WalletModel>() {
            @Override
            public void onResponse(Call<WalletModel> call, Response<WalletModel> response) {
                WalletModel model = response.body();
                if(model == null)return;
                remaining_balance_tv.setText(model.getWallet()+" QR");
            }

            @Override
            public void onFailure(Call<WalletModel> call, Throwable t) {

            }
        });
    }
}
