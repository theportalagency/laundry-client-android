package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FullOrderResponseDataModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("orderNo")
    @Expose
    private String orderNo;

    @SerializedName("pickup_time")
    @Expose
    private PickUpTimes pickup_time;

    @SerializedName("address")
    @Expose
    private AddressCustomer address;

    @SerializedName("is_urgent")
    @Expose
    private Integer isUrgent;
    @SerializedName("is_hanged")
    @Expose
    private Integer isHanged;
    @SerializedName("total_quantity")
    @Expose
    private double totalQuantity;
    @SerializedName("total_amount")
    @Expose
    private double totalAmount;
    @SerializedName("amount")
    @Expose
    private double amount;


    @SerializedName("tax")
    @Expose
    private double tax;

    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("address_id")
    @Expose
    private Integer addressId;
    @SerializedName("promo_code_id")
    @Expose
    private Object promoCodeId;
    @SerializedName("payment_type_id")
    @Expose
    private Integer paymentTypeId;
    @SerializedName("collection_date")
    @Expose
    private String collectionDate;
    @SerializedName("collection_time")
    @Expose
    private String collectionTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public Integer getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(Integer isUrgent) {
        this.isUrgent = isUrgent;
    }

    public Integer getIsHanged() {
        return isHanged;
    }

    public void setIsHanged(Integer isHanged) {
        this.isHanged = isHanged;
    }

    public double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTax() {
        return tax;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public PickUpTimes getPickup_time() {
        return pickup_time;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Object getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(Object promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public Integer getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getStatus() {
        return status;
    }

    public AddressCustomer getAddress() {
        return address;
    }
}
