package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorsSignUp {

    @SerializedName("email")
    private String[] email;

    public String[] getEmail() {
        return email;
    }

}
