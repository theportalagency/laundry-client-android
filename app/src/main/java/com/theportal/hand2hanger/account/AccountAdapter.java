package com.theportal.hand2hanger.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.AccountItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.MyViewHolder> {
    private final List<AccountItem> accountItems;
    private Context context;
    private OnItemClickListener mListener;


    AccountAdapter(List<AccountItem> accountItems, Context context) {
        this.accountItems = accountItems;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AccountItem accountItem = accountItems.get(position);
        holder.itemTitle.setText(accountItem.getItemTitle());
        holder.iconId.setImageResource(accountItem.getIconId());

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.account_item_rv, parent, false);
        return new MyViewHolder(itemView);
    }

//    public void loadFragment(Fragment fragment) {
//        // create a FragmentManager
//        FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
//        // create a FragmentTransaction to begin the transaction and replace the Fragment
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        // replace the FrameLayout with new Fragment
//        fragmentTransaction.replace(R.id.intro_slider_container, fragment);
//        fragmentTransaction.commit(); // save the changes
//
//    }

    @Override
    public int getItemCount() {
        return accountItems.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.account_item_title)
        TextView itemTitle;
        @BindView(R.id.account_item_icon)
        ImageView iconId;
        @BindView(R.id.account_item)
        RelativeLayout accountItem;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            itemView.setOnClickListener(v -> {
                if (mListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        mListener.onItemClick(position);
                    }
                }
            });
        }
    }
}
