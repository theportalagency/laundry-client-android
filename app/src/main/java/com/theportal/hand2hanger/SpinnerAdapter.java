package com.theportal.hand2hanger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import com.theportal.hand2hanger.data.model.FacilityTypesResponse;
import com.theportalagency.hand2hanger.R;

import java.util.List;
import java.util.zip.Inflater;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<FacilityTypesResponse> items;
    LayoutInflater inflter;


    public SpinnerAdapter(Context context, List<FacilityTypesResponse> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View v, ViewGroup viewGroup) {
        View views = v.inflate(context, R.layout.spinner_custom_design_list,null);
        TextView tv_spinner = views.findViewById(R.id.tv_spinner);
        TextView cb_spinner = views.findViewById(R.id.cb_spinner);
        tv_spinner.setText(i + " %");
        cb_spinner.setText(items.get(i).getName());
        return views;
    }


}
