package com.theportal.hand2hanger.Utility;

import com.theportalagency.hand2hanger.R;

public class ColorIconProvider {
    public static int getColorIcon(String colorName) {
        int colorIcon;
        switch (colorName) {
            case "red":
                colorIcon = R.drawable.red_circle;
                break;
            case "silver":
                colorIcon = R.drawable.silver_circle;
                break;
            case "purple":
                colorIcon = R.drawable.purple_circle;
                break;
            case "fuchsia":
                colorIcon = R.drawable.fuchsia_circle;
                break;
            case "navy":
                colorIcon = R.drawable.navy_circle;
                break;
            case "yellow":
                colorIcon = R.drawable.yellow_circle;
                break;
            case "blue":
                colorIcon = R.drawable.blue_circle;
                break;
            case "black":
                colorIcon = R.drawable.black_circle;
                break;
            case "teal":
                colorIcon = R.drawable.teal_circle;
                break;
            case "gray":
                colorIcon = R.drawable.gray_circle;
                break;
            case "green":
                colorIcon = R.drawable.green_circle;
                break;
            case "white":
                colorIcon = R.drawable.white_circle;
                break;
            case "lime":
                //00FF00
                colorIcon = R.drawable.lime_circle;
                break;
            case "olive":
                colorIcon = R.drawable.olive_circle;
                break;
            default:
                colorIcon = R.drawable.black_circle;
        }
        return colorIcon;
    }
}
