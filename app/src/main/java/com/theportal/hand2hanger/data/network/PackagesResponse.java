package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackagesResponse {
    @SerializedName("current_page")
    private int current_page;

    @SerializedName("data")
    private List<DataPackages> data;

    @SerializedName("last_page")
    private int last_page;

    public int getCurrent_page() {
        return current_page;
    }

    public List<DataPackages> getData() {
        return data;
    }

    public int getLast_page() {
        return last_page;
    }
}
