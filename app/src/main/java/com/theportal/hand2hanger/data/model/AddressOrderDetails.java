package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressOrderDetails {

    @SerializedName("building_no")
    @Expose
    private String building_no;

    @SerializedName("floor_no")
    @Expose
    private int floor_no;

    @SerializedName("flat_no")
    @Expose
    private int flat_no;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("delivery_cost")
    @Expose
    private double delivery_cost;

    public double getDelivery_cost() {
        return delivery_cost;
    }

    public String getBuilding_no() {
        return building_no;
    }

    public int getFloor_no() {
        return floor_no;
    }

    public int getFlat_no() {
        return flat_no;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }
}
