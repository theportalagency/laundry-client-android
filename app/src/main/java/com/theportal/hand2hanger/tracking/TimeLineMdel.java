package com.theportal.hand2hanger.tracking;

public class TimeLineMdel {

    private int status;

    public TimeLineMdel(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
