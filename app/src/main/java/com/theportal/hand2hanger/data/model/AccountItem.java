package com.theportal.hand2hanger.data.model;

public class AccountItem {

    private int itemTitle;
    private int iconId;

    public AccountItem(int itemTitle, int iconId) {
        this.itemTitle = itemTitle;
        this.iconId = iconId;
    }

    public int getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(int itemTitle) {
        this.itemTitle = itemTitle;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
