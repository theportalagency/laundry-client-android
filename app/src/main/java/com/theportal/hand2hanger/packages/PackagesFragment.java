package com.theportal.hand2hanger.packages;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qpay.qpayandroidsdk.base.QpaySdk;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.DataPackages;
import com.theportal.hand2hanger.data.network.PackagesResponse;
import com.theportal.hand2hanger.payment.PaymentResponse;
import com.theportalagency.hand2hanger.R;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class PackagesFragment extends AppCompatActivity implements PackagesAdapter.onPackageSelect {

    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.rv_packages)
    RecyclerView rv_packages;
    @BindView(R.id.purchase_package)
    Button purchase_package;
    @BindView(R.id.tv_selectedPackage)
    TextView tv_selectedPackage;
    private PackagesAdapter packagesAdapter;
    private APIInterface apiInterface;
    private String token;
    private SharedPreferences pref;
    private int id = -1;
    private double costPackage;
    private QpaySdk reqParams;
    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;
    final int min = 2;
    final int max = 200;
    private int random;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_packages);
        init();
    }

    private void init() {
        random = new Random().nextInt((max - min) + 1) + min;
        ButterKnife.bind(this,this);
        reqParams = new QpaySdk(this);
        pref = Objects.requireNonNull(getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0));
        token = pref.getString(TOKEN_KEY, token);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        counterPanel_.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_packages.setHasFixedSize(true);
        rv_packages.setNestedScrollingEnabled(false);
        rv_packages.setLayoutManager(new LinearLayoutManager(Constants.context));
        tv_toolbar_inside.setText(R.string.packages);
        toolbar.setNavigationOnClickListener(view1 -> {
            onBackPressed();
        });
        purchase_package.setOnClickListener(view1 -> purchase(id));
        getPageUrl();
    }

    private void getPageUrl() {
        Call<PackagesResponse> call = apiInterface.getPackagesResponse(token);
        call.enqueue(new Callback<PackagesResponse>() {
            @Override
            public void onResponse(Call<PackagesResponse> call, Response<PackagesResponse> response) {
                PackagesResponse response1 = response.body();
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);
                if (response1 == null) return;
                List<DataPackages> dataPackages = response1.getData();
                if (dataPackages == null) return;
                packagesAdapter = new PackagesAdapter(Constants.context, dataPackages, PackagesFragment.this);
                rv_packages.setAdapter(packagesAdapter);
            }

            @Override
            public void onFailure(Call<PackagesResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);
                ll_noConnection.setVisibility(View.VISIBLE);
                call.cancel();
            }
        });

    }

    @Override
    public void select(int package_id, String name, double cost, TextView tv_name) {
        tv_selectedPackage.setText(name);
        costPackage = cost;
        id = package_id;
    }

    private void purchase(int id) {
        if (id == -1) {
            Toast.makeText(Constants.context, "Choose Package First", Toast.LENGTH_SHORT).show();
        } else {
            addPayment();
        }
    }

    private void addPayment() {
        reqParams.setCurrency("QAR");
        reqParams.setCountry("QA"); // 2 digit of country code for e.g: QA for Qatar
        reqParams.setCity("Cairo");
        reqParams.setState("Islam salem State");
        reqParams.setEmail("islam@gmail.com");
        reqParams.setAddress("Islam salem Address");
        reqParams.setName("Islam salem");
        reqParams.setPhone("01112070653");
        reqParams.setAmount(BigDecimal.valueOf(costPackage));
        reqParams.setDescription("anwayasjsjanjans");
        reqParams.setReferenceId(String.valueOf(random));
        reqParams.setGatewayId("015518711");
        reqParams.setSecretKey("2-vFyVlSWFs6pCD7");
        reqParams.setMode("LIVE");
        reqParams.doPayment();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && data != null)
        {
            String dataString = data.getStringExtra("data");
            sentPayment(dataString);
        }
    }

    private void sentPayment(String dataString) {
        JsonParser parser = new JsonParser();
        JsonElement mJson;
        mJson = parser.parse(dataString);
        Gson gson = new Gson();
        PaymentResponse paymentResponse = gson.fromJson(mJson, PaymentResponse.class);
        if (!paymentResponse.getStatus().equals("success")){
            Toast.makeText(PackagesFragment.this, paymentResponse.getStatus(), Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, Object> payment = new HashMap<>();
        payment.put("package_id", id);
        payment.put("amount", costPackage);
        payment.put("cardType", paymentResponse.getCardType());
        payment.put("datetime", paymentResponse.getDatetime());
        payment.put("maskedCardNumber",paymentResponse.getMaskedCardNumber());
        payment.put("reason","<null>");
        payment.put("orderId", paymentResponse.getOrderId());
        payment.put("status", paymentResponse.getStatus());
        payment.put("transactionId", paymentResponse.getTransactionId());
        payment.put("transactionStatus", paymentResponse.getTransactionStatus());
        Call<SignupResponseModel> call = apiInterface.subscribe(token,payment);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                if (response.code() == 200){
                    SignupResponseModel model = response.body();
                    if (model == null)return;
                    Toast.makeText(Constants.context, model.getSuccess(), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(Constants.context, "Purchase error Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {

            }
        });
    }
}
