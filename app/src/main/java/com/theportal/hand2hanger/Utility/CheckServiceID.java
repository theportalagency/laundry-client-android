package com.theportal.hand2hanger.Utility;

public class CheckServiceID {
    public static int checkServiceID(String serviceName) {
        int serviceID = 0;
        switch (serviceName) {
            case "Wash & Press":
                serviceID = Constants.WASH_ID;
                break;
            case "Dry Clean":
                serviceID = Constants.DRY_CLEAN_ID;
                break;
            case "Tailoring":
                serviceID = Constants.TAILORING_ID;
                break;
            case "Steam Press":
                serviceID = Constants.STREAM_ID;
                break;
        }
        return serviceID;
    }
}
