package com.theportal.hand2hanger.wallet;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.WalletHistory;
import com.theportal.hand2hanger.data.model.WalletModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;

import java.util.List;
import java.util.Objects;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class WalletFragment extends Fragment {

    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.tv_no_history)
    TextView tv_no_history;
    @BindView(R.id.remaining_history_rv)
    RecyclerView remaining_history_rv;

    private HistoryAdapter historyAdapter;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;

    @BindView(R.id.remaining_balance_tv)
    TextView remaining_balance_tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_mywallet, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.my_Wallet);
        counterPanel_.setVisibility(View.GONE);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, v);
        getBalance();
        getHistory();
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
    }

    private void getHistory() {
        Call<WalletHistory> call = apiInterface.history(token );
        call.enqueue(new Callback<WalletHistory>() {
            @Override
            public void onResponse(Call<WalletHistory> call, Response<WalletHistory> response) {
                WalletHistory history = response.body();
                if (history == null)return;
                List<HistoryModel> models = history.getData();
                if (models.size() == 0){
                    tv_no_history.setVisibility(View.VISIBLE);
                    return;
                }
                historyAdapter = new HistoryAdapter(Constants.context,models);
                remaining_history_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                remaining_history_rv.setHasFixedSize(true);
                remaining_history_rv.setNestedScrollingEnabled(false);
                remaining_history_rv.setAdapter(historyAdapter);
            }

            @Override
            public void onFailure(Call<WalletHistory> call, Throwable t) {

            }
        });
    }

    private void getBalance() {
        Call<WalletModel> call = apiInterface.wallet_balance(token );
        call.enqueue(new Callback<WalletModel>() {
            @Override
            public void onResponse(Call<WalletModel> call, Response<WalletModel> response) {
                WalletModel model = response.body();
                if(model == null)return;
                remaining_balance_tv.setText(model.getWallet()+" QR");
            }

            @Override
            public void onFailure(Call<WalletModel> call, Throwable t) {

            }
        });
    }
}