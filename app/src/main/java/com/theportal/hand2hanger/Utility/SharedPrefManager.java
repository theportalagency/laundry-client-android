package com.theportal.hand2hanger.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by amrka on 1/13/2018.
 */

public class SharedPrefManager {

    private static final String USERDATA = "MyVariables";
    private static final String PREFS_WORK = "WORK_APP";
    private static final String WORK = "work";


    public static void saveDataString(Context context, String variable, String data) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        prefs.edit().putString(variable, data).apply();
    }

    public static void saveDataInt(Context context, String variable, int data) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        prefs.edit().putInt(variable, data).apply();
    }

    public static String getDataString(Context context, String variable, String defaultValue) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getString(variable, defaultValue);
    }

    /*public static void deleteString(Context context, String value) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        prefs.edit().remove(value).commit();
    }*/

    public static int getDataInt(Context context, String variable, int defaultValue) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getInt(variable, defaultValue);
    }

    public static void hash_code(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "ma.moh",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}