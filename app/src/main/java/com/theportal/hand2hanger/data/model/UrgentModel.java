package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrgentModel {

    @SerializedName("total_amount")
    @Expose
    private double total_amount;

    @SerializedName("amount")
    @Expose
    private double amount;

    public double getTotal_amount() {
        return total_amount;
    }

    public double getAmount() {
        return amount;
    }
}
