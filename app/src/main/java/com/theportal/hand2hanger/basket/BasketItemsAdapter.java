package com.theportal.hand2hanger.basket;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BasketItemsAdapter extends RecyclerView.Adapter<BasketItemsAdapter.ViewHolder> {
    private static RecyclerViewClickListener mItemListener;
    private final List<OrderItem> basketItemList;
    private Context context;

    public BasketItemsAdapter(List<OrderItem> basketItemList, Context context) {
        this.basketItemList = basketItemList;
        this.context = context;
    }

    public void setItemListener(RecyclerViewClickListener itemListener) {
        mItemListener = itemListener;
    }

    @NonNull
    @Override
    public BasketItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.basket_item_rv, parent, false);
        return new BasketItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BasketItemsAdapter.ViewHolder holder, int position) {

        OrderItem basketItem = basketItemList.get(position);
        holder.itemTypeTv.setText(basketItem.getType());

        holder.itemsPriceTv.setText(basketItem.getTotalAmount() + " QR");

        if (basketItem.getService_id() > 3) {
            holder.itemServiceTv.setText(basketItem.getService());
        } else {
            holder.itemServiceTv.setText(basketItem.getService());
        }

        holder.item_count.setText(basketItem.getCount() + "");
        double cost = (basketItem.getTotalAmount() / basketItem.getCount());
        holder.item_cost.setText(basketItem.getAmount() + " QR");
        holder.btn_add_remove.setOnClickListener(view -> mItemListener.onItemIncreaseCount(position,basketItem.getId(),basketItem.getType_id(),basketItem.getService_id(),
                basketItem.getCategory_id(),basketItem.getCount(),holder.item_count,basketItem.getOrder_id()));
        holder.btn_remove_basket.setOnClickListener(view -> mItemListener.onItemDecreaseCount(position,basketItem.getId(),basketItem.getType_id(),basketItem.getService_id(),
                basketItem.getCategory_id(),basketItem.getCount(),holder.item_count,basketItem.getOrder_id()));
        holder.removeItemIb.setOnClickListener(v -> {
            mItemListener.onItemDelete(position, basketItem.getId(), basketItem.getOrder_id());
        });
    }

    @Override
    public int getItemCount() {
        return basketItemList.size();
    }

    public interface RecyclerViewClickListener {
        void onItemDelete(int position, int item_id, int order_id);
        void onItemDecreaseCount(int position,int item_id, int type_id, int service_id,int category_id,int count,TextView tv_count,int orderID);
        void onItemIncreaseCount(int position, int item_id, int type_id, int service_id, int category_id, int count, TextView tv_count, int order_id);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_type)
        TextView itemTypeTv;
        @BindView(R.id.item_service)
        TextView itemServiceTv;
        @BindView(R.id.item_price)
        TextView itemsPriceTv;
        @BindView(R.id.remove_ib)
        ImageButton removeItemIb;
        @BindView(R.id.item_count)
        TextView item_count;
        @BindView(R.id.item_cost)
        TextView item_cost;
        @BindView(R.id.btn_remove_basket)
        ImageView btn_remove_basket;
        @BindView(R.id.btn_add_remove)
        ImageView btn_add_remove;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

    }
}
