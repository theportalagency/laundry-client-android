package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataFacility {
    @SerializedName("total_amount")
    @Expose
    private double total_amount;

    @SerializedName("isPercentage")
    @Expose
    private int isPercentage;

    @SerializedName("facility_amount")
    @Expose
    private double facility_amount;

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("id")
    @Expose
    private int id;

    public double getTotal_amount() {
        return total_amount;
    }

    public int getIsPercentage() {
        return isPercentage;
    }

    public double getFacility_amount() {
        return facility_amount;
    }

    public double getAmount() {
        return amount;
    }

    public int getId() {
        return id;
    }
}
