package com.theportal.hand2hanger.myorders;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.FullOrderResponseDataModel;
import com.theportal.hand2hanger.data.model.FullOrderResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class OrdersFragment extends Fragment {

    @BindView(R.id.recyclerview_orders)
    RecyclerView ordersRv;
    @BindView(R.id.tv_no_result_orders)
    TextView tv_no_result_orders;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private TextView fragmentTitle;
    private ImageButton fab, homeButton, ordersButton, profileButton;
    private APIInterface apiInterface;
    private String token;
    private OrdersAdapter ordersAdapter;
    int page =1;
    int last_page;
    List<FullOrderResponseDataModel> body = new ArrayList<>();

    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        fab = Objects.requireNonNull(getActivity()).findViewById(R.id.fab);
        homeButton = getActivity().findViewById(R.id.home_navigation_btn);
        ordersButton = getActivity().findViewById(R.id.orders_navigation_btn);
        profileButton = getActivity().findViewById(R.id.profile_navigation_btn);
        Constants.FRAGMENT_NUMBER = 1;
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);
        fab.setOnClickListener(v -> {
            fab.setBackground(getResources().getDrawable(R.drawable.ic_plus2));
            fab.setEnabled(false);
            loadFragment(new HomeFragment());
            setButtonBackground();
            fragmentTitle.setText("");
        });
        getUserOrders();
        return view;
    }

    private void getUserOrders() {
        Call<FullOrderResponseModel> call = apiInterface.getUserAllOrders(token,page);
        call.enqueue(new Callback<FullOrderResponseModel>() {
            @Override
            public void onResponse(Call<FullOrderResponseModel> call, Response<FullOrderResponseModel> response) {
                FullOrderResponseModel data = response.body();
                animationView.setVisibility(View.GONE);
                animationView.cancelAnimation();
                if (data == null)return;
                body.addAll(data.getData());
                last_page = data.getMeta().getLastPage();
                setupUI();
                if (last_page > page){
                    page +=1;
                    getUserOrders();
                }
            }
            @Override
            public void onFailure(Call<FullOrderResponseModel> call, Throwable t) {
                ll_noConnection.setVisibility(View.VISIBLE);
                animationView.setVisibility(View.GONE);
                animationView.cancelAnimation();
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void setupUI() {
        if (body.size() == 0){
            tv_no_result_orders.setVisibility(View.VISIBLE);
            return;
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        ordersRv.setLayoutManager(layoutManager);
        //TODO we need to make reverse list
        ordersAdapter = new OrdersAdapter(body, getActivity());
        ordersRv.setHasFixedSize(true);
        ordersRv.setAdapter(ordersAdapter);
    }

    private void setButtonBackground() {
        homeButton.setBackground(getResources().getDrawable(R.drawable.navigation_iv_yellow));
        ordersButton.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm != null ? fm.beginTransaction() : null;
        // replace the FrameLayout with new Fragment
        if (fragmentTransaction != null) {
            fragmentTransaction.add(R.id.intro_slider_container, fragment);
        }
        Objects.requireNonNull(fragmentTransaction).disallowAddToBackStack();
//        fm.addOnBackStackChangedListener(() -> getActivity().finish());
        fragmentTransaction.commit(); // save the changes
    }
}
