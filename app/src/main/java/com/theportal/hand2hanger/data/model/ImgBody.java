package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImgBody {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("link")
    @Expose
    private String link;

    public String getSuccess() {
        return success;
    }

    public String getLink() {
        return link;
    }
}
