package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prices {
    @SerializedName("count")
    @Expose
    private int count;

    public int getCount() {
        return count;
    }
}
