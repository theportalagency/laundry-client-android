package com.theportal.hand2hanger.addresses;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.OnBackPressed;
import com.theportal.hand2hanger.data.model.AddressUser;
import com.theportal.hand2hanger.data.model.AddressData;
import com.theportal.hand2hanger.data.model.CityName;
import com.theportal.hand2hanger.data.model.CityZone;
import com.theportal.hand2hanger.data.model.PickUpTimer;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.SignupModel;
import com.theportal.hand2hanger.myorders.InvoiceBasic;
import com.theportal.hand2hanger.myorders.PaymentDetailsOrder;
import com.theportal.hand2hanger.payment.PaymentFragment;
import com.theportalagency.hand2hanger.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.DELIVERY_FEE;
import static com.theportal.hand2hanger.Utility.Constants.HOME_ADDRESS_TYPE_ID;
import static com.theportal.hand2hanger.Utility.Constants.OFFICE_ADDRESS_TYPE_ID;
import static com.theportal.hand2hanger.Utility.Constants.OTHER_ADDRESS_TYPE_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID_SELECT;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.roundAvoid;


public class MyAddressFragment extends Fragment implements
        TextWatcher, AddressAdapter.WashItemClickListener, OnBackPressed {

    @BindView(R.id.signup_btn)
    Button homeBtn;
    @BindView(R.id.other_btn)
    Button otherBtn;
    @BindView(R.id.office_btn)
    Button officeBtn;
    @BindView(R.id.place_holder_btn)
    Button placeOlderBtn;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.rv_price_address)
    RecyclerView rv_price_address;

    //TODO implement bottomsheet
    BottomSheetBehavior sheetBehavior;

    int pickupTimeId, addressId;
    private Map<String, Object> orderMap = new HashMap<>();
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private String token;
    private int i;
    private Context context;
    private String collectionDate;
    private SharedPreferences.Editor editor;
    private List<InvoiceBasic> invoiceBasics = new ArrayList<>();
    private List<AddressUser> addressList;
    private AddressAdapter addressAdapter;

    @BindView(R.id.expandable_add_address_lay)
    ExpandableLayout expandableAddAddressLay;
    @BindView(R.id.plus_add_address_btn)
    ImageButton plusAddAddressBtn;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    @BindView(R.id.street_et)
    EditText street_et;
    @BindView(R.id.zoneSpinner)
    Spinner zoneSpinner;
    @BindView(R.id.building_et)
    EditText building_et;
    @BindView(R.id.flat_et)
    EditText flat_et;
    @BindView(R.id.floor_et)
    EditText floor_et;

    @BindView(R.id.pickup_spinner)
    Spinner pickupSpinner;
    @BindView(R.id.delivery_date_et)
    EditText deliveryDateEt;
    @BindView(R.id.show_address_rv)
    RecyclerView showAddressRv;
    @BindView(R.id.textView7)
    TextView textView7;
    @BindView(R.id.textView10)
    TextView textView10;
    @BindView(R.id.tv_pick_loc)
    TextView tv_pick_loc;
    private Map<String, Object> addAddressMap = new HashMap<>();
    private boolean isExpand = true;
    private List<CityName> cityNameList;
    private List<PickUpTimer> pickUpTimersList;
    private int cityPosition, zonePosition, pickupPosition;
    private List<CityZone> cityZoneList;
    private boolean isValidBuild;
    private int curr_address;
    private double total_amount, amount;
    private PaymentDetailsOrder payment_order;
    private LocationManager mLocationManager;
    private List<String> cityName = new ArrayList<>();
    private List<Double> deliveryCity = new ArrayList<>();
    private double lat,lng;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery_address, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        token = pref.getString(TOKEN_KEY, token);
        editor = pref.edit();
        editor.apply();
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText("Choose Address");
        counterPanel_.setVisibility(View.GONE);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        rv_price_address.setHasFixedSize(true);
        rv_price_address.setNestedScrollingEnabled(false);
        rv_price_address.setLayoutManager(new LinearLayoutManager(getActivity()));

        curr_address = HOME_ADDRESS_TYPE_ID;
        loadData(curr_address);
        homeBtn.setBackground(getResources().getDrawable(R.drawable.corners_btn3));
        homeBtn.setOnClickListener(v -> {
            curr_address = HOME_ADDRESS_TYPE_ID;
            loadData(curr_address);
            //loadFragment(new HomeAddressFragment());
            changeButtonColor(homeBtn, otherBtn, officeBtn);
        });
        officeBtn.setOnClickListener(v -> {
            curr_address = OFFICE_ADDRESS_TYPE_ID;
            loadData(curr_address);
            //loadFragment(new OfficeAddressFragment());
            changeButtonColor(officeBtn, homeBtn, otherBtn);
        });
        otherBtn.setOnClickListener(v -> {
            curr_address = OTHER_ADDRESS_TYPE_ID;
            loadData(curr_address);
            changeButtonColor(otherBtn, homeBtn, officeBtn);
        });
        placeOlderBtn.setOnClickListener(v -> {
            placeOrder();
        });
        DELIVERY_FEE = 0;
        if (addressAdapter != null)
            addressAdapter.notifyDataSetChanged();

        getAddressData();

        collectionDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        deliveryDateEt.setText(collectionDate);

        plusAddAddressBtn.setOnClickListener(v -> {
            if (isExpand) {
                expandableAddAddressLay.expand();
                isExpand = false;
            } else {
                expandableAddAddressLay.collapse();
                isExpand = true;
            }
        });

        tv_pick_loc.setOnClickListener(view1 -> {
            pickLocOnMap();
        });


        building_et.addTextChangedListener(this);
        floor_et.addTextChangedListener(this);
        flat_et.addTextChangedListener(this);
        street_et.addTextChangedListener(this);
        //addAddressBtn.setOnClickListener(v -> validateInputs());
        checkBundleKEY();
        deliveryDateEt.setOnClickListener(v -> {
            Calendar c = Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(Constants.context, (view1, year, month, dayOfMonth) -> {
                String _year = String.valueOf(year);
                String _month = (month + 1) < 10 ? "0" + (month + 1) : String.valueOf(month + 1);
                String _date = dayOfMonth < 10 ? "0" + dayOfMonth : String.valueOf(dayOfMonth);
                collectionDate = _year + "-" + _month + "-" + _date;
                deliveryDateEt.setText(collectionDate);
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.MONTH));
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            dialog.show();
        });
        getCity();
        getTimeFromTo();
        return view;
    }


    private void pickLocOnMap() {
        validateInputs();
    }

    private void checkBundleKEY() {
        Bundle bundle = getArguments();
        if (bundle == null) return;
        int extra = bundle.getInt("KEY", -1);
        if (extra == 0) {
            placeOlderBtn.setVisibility(View.GONE);
            pickupSpinner.setVisibility(View.GONE);
            deliveryDateEt.setVisibility(View.GONE);
            textView7.setVisibility(View.GONE);
            textView10.setVisibility(View.GONE);
            rv_price_address.setVisibility(View.GONE);
        } else {
            total_amount = bundle.getDouble("total");
            amount = bundle.getDouble("amount");
            updatePrice();
        }
    }

    private void getAddressData() {
        Call<AddressData> call = apiInterface.getAddress(token, curr_address);
        call.enqueue(new Callback<AddressData>() {
            @Override
            public void onResponse(Call<AddressData> call, Response<AddressData> response) {
                Log.e("onResponse", response.message());

                if (response.isSuccessful()) {
                    addressList = response.body().getData();
                    updateUi(addressList);
                }
            }

            @Override
            public void onFailure(Call<AddressData> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(List<AddressUser> body) {
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(Constants.context, LinearLayoutManager.VERTICAL, false);
        showAddressRv.setLayoutManager(layoutManager2);
        addressAdapter = new AddressAdapter(body, Constants.context, MyAddressFragment.this);
        showAddressRv.setHasFixedSize(true);
        showAddressRv.setAdapter(addressAdapter);
    }

    private void getTimeFromTo() {
        Call<List<PickUpTimer>> call = apiInterface.getPickupTimes(token);
        call.enqueue(new Callback<List<PickUpTimer>>() {
            @Override
            public void onResponse(Call<List<PickUpTimer>> call, Response<List<PickUpTimer>> response) {
                if (response.code() == 200) {
                    if (response.errorBody() == null) {
                        pickUpTimersList = response.body();
                        if (pickUpTimersList == null) return;
                        ArrayAdapter<PickUpTimer> timeFromToAdapter = new ArrayAdapter<>(Constants.context,
                                R.layout.spinner_item, pickUpTimersList);
                        pickupSpinner.setAdapter(timeFromToAdapter);
                        pickupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                pickupPosition = pickUpTimersList.get(position).getId();
                                if (pickUpTimersList.get(position).getId() != null) {
                                    pickupTimeId = pickupPosition;
                                }
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } else {
                        Toast.makeText(Constants.context, "Time is empty", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PickUpTimer>> call, Throwable t) {

            }
        });
    }

    private void validateInputs() {
        if (isValidBuild) {
            if (street_et.getText().toString().equals("")) {
                street_et.setError("Can't be empty");
                return;
            }
            Intent map = new Intent(getActivity(),MapsActivity.class);
            map.putExtra("address_type_id",curr_address);
            map.putExtra("city_id",cityPosition);
            map.putExtra("building_no", building_et.getText().toString());
            map.putExtra("floor_no", floor_et.getText().toString());
            map.putExtra("flat_no",flat_et.getText().toString());
            map.putExtra("region_id",zonePosition);
            map.putExtra("street",street_et.getText().toString());
            startActivity(map);
        }
    }

    private void getZone(int cityID) {
        Call<List<CityZone>> call = apiInterface.getCityZone(token, cityID);
        call.enqueue(new Callback<List<CityZone>>() {
            @Override
            public void onResponse(Call<List<CityZone>> call, Response<List<CityZone>> response) {
                if (response.code() == 200) {
                    if (response.errorBody() == null) {
                        cityZoneList = response.body();
                        if (cityZoneList == null) return;
                        ArrayAdapter<CityZone> zoneAdapter = new ArrayAdapter<>(Constants.context,
                                R.layout.spinner_item, cityZoneList);
                        zoneSpinner.setAdapter(zoneAdapter);
                        zoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                zonePosition = cityZoneList.get(position).getId();
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } else {
                        Toast.makeText(Constants.context, "Zone is empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<CityZone>> call, Throwable t) {

            }
        });
    }

    private void getCity() {
        Call<List<CityName>> call = apiInterface.getCityName(token);
        call.enqueue(new Callback<List<CityName>>() {
            @Override
            public void onResponse(Call<List<CityName>> call, Response<List<CityName>> response) {
                if (response.code() == 200) {
                    if (response.errorBody() == null) {
                        cityNameList = response.body();
                        if (cityNameList == null) return;
                        ArrayAdapter<CityName> cityAdapter = new ArrayAdapter<>(Constants.context, R.layout.spinner_item, cityNameList);
                        citySpinner.setAdapter(cityAdapter);
                        for (int i = 0; i < cityNameList.size(); i++) {
                            cityName.add(cityNameList.get(i).getName());
                            deliveryCity.add(cityNameList.get(i).getDeliveryCost());
                        }
                        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                cityPosition = cityNameList.get(position).getId();
                                getZone(cityPosition);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } else {
                        Toast.makeText(Constants.context, "City is empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<CityName>> call, Throwable t) {

            }
        });

        getZone(cityPosition);
    }

    private void updatePrice() {
        invoiceBasics.clear();
        invoiceBasics.add(new InvoiceBasic(
                "Delivery Cost ", String.valueOf(DELIVERY_FEE)
        ));
        invoiceBasics.add(new InvoiceBasic(
                "Total ", roundAvoid(total_amount) + ""
        ));
        payment_order = new PaymentDetailsOrder(Constants.context, invoiceBasics);
        rv_price_address.setAdapter(payment_order);
    }

    private void loadData(int id) {
        Call<AddressData> call = apiInterface.getAddress(token, id);
        call.enqueue(new Callback<AddressData>() {
            @Override
            public void onResponse(Call<AddressData> call, Response<AddressData> response) {
                if (response.isSuccessful()) {
                    addressList = response.body().getData();
                    updateUi(addressList);
                } else {
                }
            }

            @Override
            public void onFailure(Call<AddressData> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void placeOrder() {
        if (addressId == 0) {
            Toast.makeText(Constants.context, "Please Choose Address First", Toast.LENGTH_SHORT).show();
            return;
        }
        orderMap.put("address_id", addressId);
        orderMap.put("collection_date", collectionDate);
        orderMap.put("pickup_time_id", pickupTimeId);
        orderMap.put("lat", lat);
        orderMap.put("lng", lng);
        Call<SignupResponseModel> call = apiInterface.placeOrder(token, orderMap);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                if (response.code() == 200) {
                    DELIVERY_FEE = 0;
                    if (response.body() != null && response.body().getSuccess() != null) {
                        Toast.makeText(getActivity(), response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    }
                    Intent intent = new Intent(Constants.context,PaymentFragment.class);
                    intent.putExtra("total", total_amount);
                    intent.putExtra("amount", amount);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, Constants.context.getResources().
                            getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void changeButtonColor(View yellowBtn, View blueBtn1, View blueBtn2) {
        yellowBtn.setBackground(getResources().getDrawable(R.drawable.corners_btn3));
        blueBtn1.setBackground(getResources().getDrawable(R.drawable.corners_btn));
        blueBtn2.setBackground(getResources().getDrawable(R.drawable.corners_btn));
    }

    private boolean isValidBuilding(String buildingNo) {
        if (buildingNo.length() >= 1) {
            return true;
        } else {
            building_et.setError("Enter the Building Number");
            return false;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (Objects.requireNonNull(getActivity()).getCurrentFocus() == null) return;

        switch (Objects.requireNonNull(getActivity().getCurrentFocus()).getId()) {
            case R.id.building_et:
                isValidBuild = isValidBuilding(building_et.getText().toString());
                Log.e("is name", String.valueOf(isValidBuild));
                break;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        Constants.activity = activity;
    }

    @Override
    public void onItemAdded(int position, int address_id, String city_id,String latitude,String lngutude) {
        lat = Double.parseDouble(latitude);
        lng = Double.parseDouble(lngutude);
        addressId = address_id;
        int index = 0;
        total_amount = total_amount - DELIVERY_FEE;
        for (String value : cityName) {
            if (value.equals(city_id)) {
                DELIVERY_FEE = deliveryCity.get(index);
                total_amount = total_amount + DELIVERY_FEE;
                updatePrice();
            } else {
                index++;
            }
        }

    }

    @Override
    public void onDeleteAddress(int position, int address_id) {
        Call<SignupModel> call = apiInterface.delete_address(token, address_id);
        call.enqueue(new Callback<SignupModel>() {
            @Override
            public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                SignupModel model = response.body();
                if (model == null) {
                    Toast.makeText(Constants.context, "Failed To Delete", Toast.LENGTH_SHORT).show();

                    return;
                }
                getAddressData();
                Toast.makeText(Constants.context, ""+model.getSuccess(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SignupModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getAddressData();
    }

    @Override
    public void onBackPressed() {
        TAB_ID_SELECT = (TAB_ID_SELECT - 10);
        getActivity().getSupportFragmentManager().popBackStack();
    }
}