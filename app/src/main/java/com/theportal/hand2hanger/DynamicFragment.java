package com.theportal.hand2hanger;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.Pricing;
import com.theportal.hand2hanger.data.model.PricingItem;
import com.theportal.hand2hanger.data.model.PricingTypes;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.prices.PricingAdapter;
import com.theportalagency.hand2hanger.R;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.SERVICE_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_NAME;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;


public class DynamicFragment extends Fragment {

    View view;

    public static DynamicFragment newInstance(int val) {
        DynamicFragment fragment = new DynamicFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", val);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rv_pricing)
    RecyclerView rv_pricing;
    private APIInterface apiInterface;
    int val;
    private SharedPreferences pref;
    private String token;
    private List<PricingTypes> pricingList;
    private PricingAdapter pricingAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) return;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this,view);
        val = getArguments().getInt("someInt", 0);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        token = pref.getString(TOKEN_KEY, token);
        rv_pricing.setHasFixedSize(true);
        rv_pricing.setNestedScrollingEnabled(false);
        rv_pricing.setLayoutManager(new LinearLayoutManager(getActivity()));
        getPricing();
    }

    private void getPricing() {
        Log.v("AMMMMMMMMMMMMMR","tab parent id is "+TAB_ID);
        Log.v("AMMMMMMMMMMMMMR","tab child id is "+SERVICE_ID);
        Call<Pricing> call = apiInterface.getItemPrice(token, TAB_ID);
        call.enqueue(new Callback<Pricing>() {
            @Override
            public void onResponse(@NonNull Call<Pricing> call, @NonNull Response<Pricing> response) {
                if (response.isSuccessful()) {
                    Pricing pricing = response.body();
                    if (pricing == null)return;
                    List<PricingItem> pricingItems = pricing.getData();
                    for (int i=0; i<pricingItems.size();i++){
                        String name = pricingItems.get(i).getName();
                        int id = pricingItems.get(i).getId();
                        if (id == SERVICE_ID){
                            pricingList = pricingItems.get(i).getTypes();
                            updateUi(pricingList);
                            return;
                        }
                    }

                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getText(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Pricing> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(List<PricingTypes> itemList) {
        pricingAdapter = new PricingAdapter(itemList, getActivity());
        rv_pricing.setHasFixedSize(true);
        rv_pricing.setAdapter(pricingAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}