package com.theportal.hand2hanger.notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.addresses.MyAddressFragment;
import com.theportal.hand2hanger.basket.view.BasketFragment;
import com.theportal.hand2hanger.data.model.LoginResponseModel;
import com.theportal.hand2hanger.data.model.Notifcation;
import com.theportal.hand2hanger.data.model.NotificationItem;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeFragment;
import com.theportal.hand2hanger.payment.PaymentFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.BackButtonUtil.hideBackButton;
import static com.theportal.hand2hanger.Utility.Constants.ADD_ORDER;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_CONFIRMATION;
import static com.theportal.hand2hanger.Utility.Constants.PLACE_ORDER;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.UPDATE_ORDER;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class NotificationFragment extends Fragment {
    private final ArrayList<NotificationItem> NotificationItemArrayList = new ArrayList<>();

    @BindView(R.id.notification_rv)
    RecyclerView notificationRv;

    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.tv_no_notifcation)
    TextView tv_no_notifcation;

    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    //private Toolbar toolbar;
    private TextView fragmentTitle;
    private String token;

    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    private APIInterface apiInterface;
    private String status = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        editor.apply();
        counterPanel_.setVisibility(View.GONE);
        token = pref.getString(TOKEN_KEY, token);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText("Notification");
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        counterPanel_.setOnClickListener(view1 -> openBasket());
        // toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);

        notificationRv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        notificationRv.setLayoutManager(layoutManager);
        getNotification();
        return view;
    }

    private void getNotification() {
        Call<List<Notifcation>> call = apiInterface.notification(token);
        call.enqueue(new Callback<List<Notifcation>>() {
            @Override
            public void onResponse(Call<List<Notifcation>> call, Response<List<Notifcation>> response) {
                List<Notifcation> notifcations = response.body();
                if (notifcations == null) return;
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);
                if (notifcations.size() == 0) {
                    tv_no_notifcation.setVisibility(View.VISIBLE);
                    return;
                }
                RecyclerView.Adapter mAdapter = new NotificationAdapter(notifcations, getActivity());
                notificationRv.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<List<Notifcation>> call, Throwable t) {
                ll_noConnection.setVisibility(View.VISIBLE);
                animationView.setVisibility(View.GONE);
            }
        });
    }

    private void openBasket() {
        Constants.loadFragment(Constants.context, R.id.frame_layout, new BasketFragment());
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}