package com.theportal.hand2hanger.Utility;

import com.theportal.hand2hanger.DynamicFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PlansPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public PlansPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int id) {
        return DynamicFragment.newInstance((id+1));
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}