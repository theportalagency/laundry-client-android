package com.theportal.hand2hanger.gifts;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.GiftDataResponseModel;
import com.theportal.hand2hanger.data.model.GiftResponseModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.SignupModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.POINTS_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;


public class GiftsFragment extends Fragment implements GiftsAdapter.RecyclerViewClickListener {
    @BindView(R.id.gifts_rv)
    RecyclerView gifts_rv;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.tv_no_gifts)
    TextView tv_no_gifts;
    private GiftsAdapter giftsAdapter;
    private List<GiftDataResponseModel> giftDataResponseModelList;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String token;
    private int my_points;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gifts_fragement, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        my_points = pref.getInt(POINTS_KEY,my_points);
        ButterKnife.bind(this, view);
        getGiftsList();
        counterPanel_.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.points);
        toolbar.setNavigationOnClickListener(view1 -> {
            getFragmentManager().popBackStack();
        });
        return view;
    }

    private void getGiftsList() {
        Call<GiftResponseModel> call = apiInterface.getGiftsList(token);
        call.enqueue(new Callback<GiftResponseModel>() {
            @Override
            public void onResponse(Call<GiftResponseModel> call, Response<GiftResponseModel> response) {
                GiftResponseModel giftResponseModel = response.body();
                if (giftResponseModel == null)return;
                giftDataResponseModelList = giftResponseModel.getData();
                if (giftDataResponseModelList.size() == 0){
                    tv_no_gifts.setVisibility(View.VISIBLE);
                    return;
                }
                updateUi(giftDataResponseModelList);
            }

            @Override
            public void onFailure(Call<GiftResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(List<GiftDataResponseModel> giftDataResponseModelList) {
        gifts_rv.setLayoutManager(new LinearLayoutManager(Constants.context));
        gifts_rv.setHasFixedSize(true);
        gifts_rv.setNestedScrollingEnabled(false);
        giftsAdapter = new GiftsAdapter(giftDataResponseModelList, getActivity(), GiftsFragment.this);
        gifts_rv.setAdapter(giftsAdapter);
    }

    @Override
    public void onItemRedeemed(View v, int position,int needs) {
        redeemItem(position,needs);
    }

    private void redeemItem(int id,int needs) {
        if (needs > my_points){
            Toast.makeText(Constants.context, "You Can't redeem this item , your points not enough !", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, Object> giftIdMap = new HashMap<>();
        giftIdMap.put("gift_id", id);
        Call<SignupModel> call = apiInterface.redeemGift(token, giftIdMap);
        call.enqueue(new Callback<SignupModel>() {
            @Override
            public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                Log.e("onResponse", response.message());
                if (response.code() == 200) {
                    Toast.makeText(Constants.context, response.body().getSuccess(), Toast.LENGTH_LONG).show();
                    call.cancel();
                }
            }

            @Override
            public void onFailure(Call<SignupModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}
