package com.theportal.hand2hanger.Utility;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.mainscreen.MainActivity;

class LoadFragmentUtility {
    static public void loadFragment(Fragment fragment, String fragmentTag, MainActivity context) {
        FragmentManager fm = context.getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.add(R.id.intro_slider_container, fragment, fragmentTag);
        fragmentTransaction.disallowAddToBackStack();
//        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }
}
