package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PointsResponseModel {

    @SerializedName("points")
    @Expose
    private int points;

    public int getPoints() {
        return points;
    }
}
