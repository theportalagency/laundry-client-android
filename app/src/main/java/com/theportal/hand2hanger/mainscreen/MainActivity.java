package com.theportal.hand2hanger.mainscreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.theportal.hand2hanger.authentication.phone_auth.ui.PhoneCollectFragment;
import com.theportal.hand2hanger.notifications.FirebaseMessageService;
import com.theportalagency.hand2hanger.R;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class MainActivity extends AppCompatActivity {

    private Boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HideStatusBar();
        setContentView(R.layout.activity_main);
        SharedPreferences pref = Objects.requireNonNull(this.getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0));
        SharedPreferences.Editor editor = pref.edit();
        editor.apply();
        if (pref.contains(TOKEN_KEY)) {
            startApplication();
        } else {
            loadFragment(new PhoneCollectFragment());
        }
        printHashKey();
    }

    private void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.theportalagency.hand2hanger", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void startApplication() {
        startService(new Intent(this, FirebaseMessageService.class));
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

    //    void sendNotification() {
//        createNotificationChannel();
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
//                .setSmallIcon(R.drawable.ic_home)
//                .setContentTitle("Hello")
//                .setContentText("Tesr")
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//        notificationManager.notify(1, builder.build());
//    }
//
//    private void createNotificationChannel() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = "Hello";
//            String description = "Hello";
//            int importance = NotificationManager.IMPORTANCE_DEFAULT;
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
//            channel.setDescription(description);
//            // Register the channel with the system; you can't change the importance
//            // or other notification behaviors after this
//            NotificationManager notificationManager = getSystemService(NotificationManager.class);
//            assert notificationManager != null;
//            notificationManager.createNotificationChannel(channel);
//            setupFirebaseMessagingService();
//        }
//    }
//
//    private void setupFirebaseMessagingService() {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(task -> {
//                    if (!task.isSuccessful()) {
//                        Log.e("TAG", "getInstanceId failed", task.getException());
//                        return;
//                    }
//
//                    // Get new Instance ID token
//                    String token = Objects.requireNonNull(task.getResult()).getToken();
//
//                    // Log and toast
//                    Log.d("TAG", token);
//                    Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
//                });
//    }

    private void HideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.intro_slider_container, fragment);
        fragmentTransaction.disallowAddToBackStack();
        fragmentTransaction.commit();
    }
}