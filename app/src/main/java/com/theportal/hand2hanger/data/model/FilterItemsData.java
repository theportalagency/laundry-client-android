package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterItemsData {

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("total_amount")
    @Expose
    private double total_amount;

    @SerializedName("order_id")
    @Expose
    private int order_id;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("facility_id")
    @Expose
    private String facility_id;

    @SerializedName("facility_name")
    @Expose
    private String facility_name;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("color")
    @Expose
    private String color;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("type_id")
    @Expose
    private int type_id;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("size")
    @Expose
    private String size;

    @SerializedName("instruction")
    @Expose
    private String instruction;

    @SerializedName("category_id")
    @Expose
    private int category_id;

    @SerializedName("service_id")
    @Expose
    private int service_id;

    public String getInstruction() {
        return instruction;
    }

    public int getCategory_id() {
        return category_id;
    }

    public int getService_id() {
        return service_id;
    }

    public double getAmount() {
        return amount;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public String getFacility_id() {
        return facility_id;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public String getType() {
        return type;
    }

    public int getType_id() {
        return type_id;
    }

    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    public String getService() {
        return service;
    }

    public int getOrder_id() {
        return order_id;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getSize() {
        return size;
    }
}
