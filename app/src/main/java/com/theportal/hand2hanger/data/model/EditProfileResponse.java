package com.theportal.hand2hanger.data.model;

import android.annotation.SuppressLint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditProfileResponse {
    @SerializedName("success")
    private String msg;

    public String getMsg() {
        return msg;
    }

}
