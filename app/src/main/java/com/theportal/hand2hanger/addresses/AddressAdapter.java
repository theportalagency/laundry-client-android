package com.theportal.hand2hanger.addresses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportal.hand2hanger.data.model.AddressUser;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    private static CheckBox lastChecked = null;
    private static int lastCheckedPos = 0;
    private final List<AddressUser> courseItemList;
    private Context context;
    private WashItemClickListener mListener;
    private int mSelectedPosition = -1;
    private RadioButton mSelectedRB;


    AddressAdapter(List<AddressUser> courseItemList, Context context, WashItemClickListener mListener) {
        this.courseItemList = courseItemList;
        this.context = context;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.address_item_rv, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddressUser courseItem = courseItemList.get(position);
        holder.address_cb.setText("Building No: " + courseItem.getBuildingNo() + " - Floor NO:" + courseItem.getFloorNo()
                + " - Flat NO:" + courseItem.getFlatNo() + " - Street :" + courseItem.getStreetNo() + " - City :" + courseItem.getCityId() + " - Region: " + courseItem.getRegionId());
        holder.address_cb.setChecked(courseItem.isChecked());
        holder.address_cb.setTag(position);
        holder.ivDeleteAddress.setOnClickListener(view -> {
            mListener.onDeleteAddress(position,courseItem.getAddressId());
        });
        holder.address_cb.setOnClickListener(view -> {
            if (position != mSelectedPosition && mSelectedRB != null) {
                mSelectedRB.setChecked(false);

            }
            mSelectedPosition = position;
            mSelectedRB = holder.address_cb;
            mListener.onItemAdded(position, courseItem.getAddressId(), courseItem.getCityId(),courseItem.getLat(),courseItem.getLng());
        });
        if (mSelectedPosition != position) {
            holder.address_cb.setChecked(false);
        } else {
            holder.address_cb.setChecked(true);
            if (mSelectedRB != null && holder.address_cb != mSelectedRB) {
                mSelectedRB = holder.address_cb;
            }
        }
    }

    @Override
    public int getItemCount() {
        assert courseItemList != null;
        return courseItemList.size();
    }

    public interface WashItemClickListener {
        void onItemAdded(int position, int address_id, String city_id,String lat,String lng);
        void onDeleteAddress(int position, int address_id);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_cb)
        RadioButton address_cb;
        @BindView(R.id.ivDeleteAddress)
        TextView ivDeleteAddress;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
