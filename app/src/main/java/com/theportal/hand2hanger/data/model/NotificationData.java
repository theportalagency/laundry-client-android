package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("status_id")
    @Expose
    private int status_id;

    public int getStatus_id() {
        return status_id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
