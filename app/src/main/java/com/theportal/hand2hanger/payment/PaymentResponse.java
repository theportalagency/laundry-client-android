package com.theportal.hand2hanger.payment;

public class PaymentResponse {
    String status;
    String transactionId;
    String orderId;
    String datetime;
    String reason;
    String transactionStatus;
    String maskedCardNumber;
    String cardType;

    public String getStatus() {
        return status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getReason() {
        return reason;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public String getCardType() {
        return cardType;
    }
}
