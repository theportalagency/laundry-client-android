package com.theportal.hand2hanger.mainscreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.theportal.hand2hanger.DetailedOrderFragment;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.about.AboutFragment;
import com.theportal.hand2hanger.addresses.MyAddressFragment;
import com.theportal.hand2hanger.basket.view.BasketFragment;
import com.theportal.hand2hanger.mainscreen.view.WashPressFragment;
import com.theportal.hand2hanger.mypoints.MyPointsFragment;
import com.theportal.hand2hanger.notifications.NotificationFragment;
import com.theportal.hand2hanger.packages.PackagesFragment;
import com.theportal.hand2hanger.payment.CreditCardFragment;
import com.theportal.hand2hanger.payment.PaymentFragment;
import com.theportal.hand2hanger.privacy.PrivacyFragment;
import com.theportal.hand2hanger.profile.ProfileFragment;
import com.theportal.hand2hanger.support.SupportFragment;
import com.theportal.hand2hanger.tracking.TrackingFragment;
import com.theportal.hand2hanger.wallet.WalletFragment;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import static com.theportal.hand2hanger.Utility.Constants.ORDER_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID_SELECT;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class SplitActivity extends AppCompatActivity {

    String extra;
    private String status, orderAddress, orderCollectionDate, orderCollectionTime;
    private double orderAmount;
    private int orderId;
    Intent intent;
    NotificationCart notificationCart;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);
        intent = getIntent();
        pref = getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        extra = intent.getStringExtra("extra");
        checkExtra(extra);
    }

    private void checkExtra(String extra) {
        Bundle bundle_service = new Bundle();
        Fragment fragment = new WashPressFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        switch (extra) {
            case "basket":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new BasketFragment());
                break;
            case "notification":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new NotificationFragment());
                break;
            case "wash":
                bundle_service.putInt("service_id", 1);
                fragment.setArguments(bundle_service);
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                break;
            case "dry":
                bundle_service.putInt("service_id", 2);
                fragment.setArguments(bundle_service);
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                break;
            case "steam":
                bundle_service.putInt("service_id", 3);
                fragment.setArguments(bundle_service);
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                break;
            case "tailor":
                bundle_service.putInt("service_id", 4);
                fragment.setArguments(bundle_service);
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                break;
            case "details":
                orderId = intent != null ? intent.getIntExtra(ORDER_ID, -1) : 0;
                Bundle bundle = new Bundle();
                bundle.putInt(ORDER_ID, orderId);
                Fragment fragment1 = new DetailedOrderFragment();
                fragment1.setArguments(bundle);
                FragmentManager fm1 = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction1 = fm1.beginTransaction();
                fragmentTransaction1.add(R.id.frame_layout, fragment1);
                fragmentTransaction1.commit();
                break;
            case "address":
                Bundle b = new Bundle();
                Fragment my_address = new MyAddressFragment();
                b.putInt("KEY",0);
                my_address.setArguments(b);
                FragmentManager fm_acc = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction_acc = fm_acc.beginTransaction();
                fragmentTransaction_acc.replace(R.id.frame_layout, my_address);
                fragmentTransaction_acc.commit();
                break;
            case "wallet":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new WalletFragment());
                break;
            case "payment":
                Bundle bundle_credit = new Bundle();
                bundle_credit.putInt("credit", 1);
                Fragment credit = new CreditCardFragment();
                credit.setArguments(bundle_credit);
                FragmentManager fm_credit = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction_credit = fm_credit.beginTransaction();
                fragmentTransaction_credit.replace(R.id.frame_layout, credit);
                fragmentTransaction_credit.commit();
                break;
            case "package":
                startActivity(new Intent(this,PackagesFragment.class));
                break;
            case "support":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new SupportFragment());
                break;
            case "about":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new AboutFragment());
                break;
            case "privacy":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new PrivacyFragment());
                break;
            case "edit":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new ProfileFragment());
                break;
            case "points":
                Constants.loadFragment(SplitActivity.this, R.id.frame_layout, new MyPointsFragment());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            //finish();
            Intent i = new Intent(getApplicationContext(), HomeActivity.class);
            switch (TAB_ID_SELECT){
                case 1:
                    i.putExtra("back",1);
                    startActivity(i);
                    break;
                case 2:
                    i.putExtra("back",2);
                    startActivity(i);
                    break;
                case 3:
                    i.putExtra("back",3);
                    startActivity(i);
                    break;
                case 4:
                    i.putExtra("back",4);
                    startActivity(i);
                    break;
                default:
                    tellFragments();
                    break;
            }
        }
    }
    private void tellFragments(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for(Fragment f : fragments){
            if(f instanceof TrackingFragment)
                ((TrackingFragment)f).onBackPressed();
            if(f instanceof DetailedOrderFragment)
                ((DetailedOrderFragment)f).onBackPressed();
            if(f instanceof MyAddressFragment)
                ((MyAddressFragment)f).onBackPressed();
            if(f instanceof BasketFragment)
                ((BasketFragment)f).onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
