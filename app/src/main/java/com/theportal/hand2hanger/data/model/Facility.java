package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Facility {

    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("isPercentage")
    @Expose
    private Integer isPercentage;

    @SerializedName("service_id")
    @Expose
    private Integer service_id;

    @SerializedName("facility_id")
    @Expose
    private Integer facility_id;

    @SerializedName("id")
    @Expose
    private Integer id;


    public Integer getService_id() {
        return service_id;
    }

    public Integer getFacility_id() {
        return facility_id;
    }

    public Integer getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getIsPercentage() {
        return isPercentage;
    }

    public void setIsPercentage(Integer isPercentage) {
        this.isPercentage = isPercentage;
    }

}
