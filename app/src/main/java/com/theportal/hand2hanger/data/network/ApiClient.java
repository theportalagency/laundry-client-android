package com.theportal.hand2hanger.data.network;

import java.io.File;
import java.util.Objects;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.theportal.hand2hanger.Utility.Constants.BASE_URL;


public class ApiClient {

    public static Retrofit getClient() {

        int cacheSize = 30 * 1024 * 1024; // 30 MB
        Cache cache = new Cache(new File(Objects.requireNonNull(System.getProperty("user.dir"))), cacheSize);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().cache(cache).addInterceptor(interceptor).build();


        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }
}


