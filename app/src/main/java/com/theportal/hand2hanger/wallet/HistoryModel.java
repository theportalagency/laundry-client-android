package com.theportal.hand2hanger.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryModel {

    @SerializedName("invoice")
    @Expose
    private String invoice;

    @SerializedName("remaining")
    @Expose
    private double remaining;


    @SerializedName("draw")
    @Expose
    private double draw;


    public String getInvoice() {
        return invoice;
    }

    public double getRemaining() {
        return remaining;
    }

    public double getDraw() {
        return draw;
    }
}
