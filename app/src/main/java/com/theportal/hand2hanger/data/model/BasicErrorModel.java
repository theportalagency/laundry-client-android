package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicErrorModel {

    @SerializedName("phone")
    @Expose
    private String[] phone;

    @SerializedName("error")
    @Expose
    private String[] error;

    @SerializedName("password")
    @Expose
    private String[] password;


    @SerializedName("errors")
    @Expose
    private BasicErrorModel errors;


    public String[] getError() {
        return error;
    }

    public BasicErrorModel getErrors() {
        return errors;
    }

    public String[] getPassword() {
        return password;
    }

    public String[] getPhone() {
        return phone;
    }
}
