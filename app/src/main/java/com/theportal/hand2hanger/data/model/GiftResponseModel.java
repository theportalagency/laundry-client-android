package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiftResponseModel {

    @SerializedName("data")
    @Expose
    private List<GiftDataResponseModel> data = null;


    public List<GiftDataResponseModel> getData() {
        return data;
    }

    public void setData(List<GiftDataResponseModel> data) {
        this.data = data;
    }


}
