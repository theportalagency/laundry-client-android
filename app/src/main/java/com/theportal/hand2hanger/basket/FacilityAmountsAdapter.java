package com.theportal.hand2hanger.basket;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.Facility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacilityAmountsAdapter extends RecyclerView.Adapter<FacilityAmountsAdapter.ViewHolder> {
    private final List<Facility> basketItemList;
    private Context context;

    public FacilityAmountsAdapter(List<Facility> basketItemList, Context context) {
        this.basketItemList = basketItemList;
        this.context = context;
    }


    @NonNull
    @Override
    public FacilityAmountsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.facility_amount_item_rv, parent, false);

        return new FacilityAmountsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityAmountsAdapter.ViewHolder holder, int position) {
        Facility facilityAmountItem = basketItemList.get(position);
        //holder.facilityNameTv.setText(facilityAmountItem.getFacility_name() + " " + facilityAmountItem.getIsPercentage() + "%");
        //holder.facilityAmountTv.setText(Double.toString(facilityAmountItem.getAmount()));
    }

    @Override
    public int getItemCount() {
        return basketItemList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.facility_name_tv)
        TextView facilityNameTv;
        @BindView(R.id.facility_amount_tv)
        TextView facilityAmountTv;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }
}
