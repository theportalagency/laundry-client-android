package com.theportal.hand2hanger.Utility;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.airbnb.lottie.LottieAnimationView;
import com.theportalagency.hand2hanger.R;

import static com.theportal.hand2hanger.Utility.Constants.CURRENT_CART_NUMBER;

public class NotificationCart {

    private static final String TAG = "NotificationCart";
    private final int MAX_NUMBER = 99;
    private final int MIN_NUMBER = 0;

    private TextView cartNumber_tv;
    private LottieAnimationView av_cart;

    public NotificationCart(View view) {
        cartNumber_tv = view.findViewById(R.id.cartCount);
        av_cart = view.findViewById(R.id.av_cart);
        checkCartNumber();
    }

    public void deleteCard(){
        CURRENT_CART_NUMBER = 0;
        cartNumber_tv.setText(String.valueOf(CURRENT_CART_NUMBER));
    }

    private void checkCartNumber() {
        if (CURRENT_CART_NUMBER == 0) {
            cartNumber_tv.setText(String.valueOf(CURRENT_CART_NUMBER));
        } else {
            cartNumber_tv.setText(String.valueOf(CURRENT_CART_NUMBER));
        }
    }

    public void increaseNumber() {
        CURRENT_CART_NUMBER++;
        av_cart.playAnimation();
        if (CURRENT_CART_NUMBER > MAX_NUMBER) {
            Log.d(TAG, "increaseNumber: Max Number Reached");
            cartNumber_tv.setText(String.valueOf(MAX_NUMBER));
        } else {
            cartNumber_tv.setText(String.valueOf(CURRENT_CART_NUMBER));
        }
    }

    public void DecreaseNumber() {
        CURRENT_CART_NUMBER--;
        if (CURRENT_CART_NUMBER == MIN_NUMBER) {
            cartNumber_tv.setText(String.valueOf(MIN_NUMBER));

        } else {
            cartNumber_tv.setText(String.valueOf(CURRENT_CART_NUMBER));
        }

    }
}
