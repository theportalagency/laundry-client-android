package com.theportal.hand2hanger.myorders;

public class InvoiceBasic {

    String key,value;

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public InvoiceBasic(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
