package com.theportal.hand2hanger.myorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.InvoiceOrderDetails;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentDetailsOrder extends RecyclerView.Adapter<PaymentDetailsOrder.ViewHolder> {

    private Context context;
    private List<InvoiceBasic> items;


    public PaymentDetailsOrder(Context context, List<InvoiceBasic> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.payment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InvoiceBasic model = items.get(position);

        if (model == null)
            return;


        holder.tv_name_payment.setText(model.getKey());
        if (model.getKey().equals("Msg")){
            holder.tv_price_payment.setText(model.getValue());
        }
        else {
            holder.tv_price_payment.setText(model.getValue()+ " QR");
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_price_payment;
        TextView tv_name_payment;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_price_payment = itemView.findViewById(R.id.tv_price_payment);
            tv_name_payment = itemView.findViewById(R.id.tv_name_payment);

        }

        @Override
        public void onClick(View view) {

        }
    }

}
