package com.theportal.hand2hanger.authentication.signup.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.hbb20.CountryCodePicker;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.SignupModel;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.login.ui.LoginFragment;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportalagency.hand2hanger.R;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class SignUpFragment extends Fragment implements android.text.TextWatcher {


    @BindView(R.id.already_amember_tv)
    TextView alreadyMember_TV;
    @BindView(R.id.full_name_et)
    EditText fullNameEt;
    @BindView(R.id.email_et)
    EditText emailEt;
    @BindView(R.id.password_et)
    EditText passEt;
    @BindView(R.id.confirm_pass_et)
    EditText confirmPassEt;
    @BindView(R.id.signup_btn)
    Button signupBtn;
    @BindView(R.id.number_et)
    EditText number_et;
    @BindView(R.id.second_name_et)
    EditText second_name_et;
    private boolean isValidEmail, isValidPass, isValidConfirmPass, isValidName,isValidSecond;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Map<String, String> userData = new HashMap<>();
    private ProgressDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = getActivity().getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0); // 0 - for private mode
        editor = pref.edit();
        fullNameEt.addTextChangedListener(this);
        emailEt.addTextChangedListener(this);
        passEt.addTextChangedListener(this);
        confirmPassEt.addTextChangedListener(this);
        alreadyMember_TV.setOnClickListener(v -> loadFragment(new LoginFragment()));
        signupBtn.setOnClickListener(view1 -> validateInputs());
        number_et.setText(Constants.NUMBER);
        number_et.setEnabled(false);
        dialog = new ProgressDialog(Constants.context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Wait");
        return view;
    }

    private void validateInputs() {
        if (isValidEmail && isValidConfirmPass && isValidName && !second_name_et.getText().toString().equals("") && isValidPass) {
            userData.put("name", fullNameEt.getText().toString()+" "+second_name_et.getText().toString());
            userData.put("email", emailEt.getText().toString());
            userData.put("password", passEt.getText().toString());
            userData.put("password_confirmation", confirmPassEt.getText().toString());
            userData.put("phone", number_et.getText().toString());
            sendSignUpData(userData);
        } else {
            Log.e("confirm_pass_et", String.valueOf(isValidConfirmPass));
            Log.e("email_et", String.valueOf(isValidEmail));
            Log.e("password_et", String.valueOf(isValidPass));
            Log.e("is name", String.valueOf(isValidName));
        }
    }

    private void sendSignUpData(Map<String, String> userData) {
        dialog.show();
        Call<SignupModel> call = apiInterface.getSignupResponse(userData);
        call.enqueue(new Callback<SignupModel>() {
            @Override
            public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                dialog.dismiss();
                SignupModel signupResponseModel = response.body();
                if (response.code() == 200 || response.code() == 201) {
                    if (signupResponseModel == null) return;
                    Toast.makeText(Constants.context, signupResponseModel.getSuccess(), Toast.LENGTH_SHORT).show();
                    loadFragment(new LoginFragment());
                } else if (response.code() == 422) {
                    Toast.makeText(Constants.context, "Email or Phone Maybe Taken", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupModel> call, Throwable t) {
                dialog.dismiss();
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void clearEt() {
        fullNameEt.getText().clear();
        passEt.getText().clear();
        confirmPassEt.getText().clear();
        emailEt.getText().clear();
    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.intro_slider_container, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    private boolean isValidEmail(String email) {
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        } else {
            emailEt.setError("Wrong Email Format.");
            return false;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        switch (getActivity().getCurrentFocus().getId()) {
            case R.id.full_name_et:
                isValidName = isValidName(fullNameEt.getText().toString());
                Log.e("is name", String.valueOf(isValidName));
                break;
            case R.id.second_name_et:
                isValidSecond = isValidSecondName(second_name_et.getText().toString());
                Log.e("is name", String.valueOf(isValidSecond));
                break;
            case R.id.email_et:
                isValidEmail = isValidEmail(emailEt.getText().toString());
                Log.e("email_et", String.valueOf(isValidEmail));
                break;
            case R.id.password_et:
                isValidPass = isValidPassword(passEt.getText().toString());
                Log.e("password_et", String.valueOf(isValidPass));
                break;
            case R.id.confirm_pass_et:
                isValidConfirmPass = isValidConfirmationPassword(passEt.getText().toString(), confirmPassEt.getText().toString());
                Log.e("confirm_pass_et", String.valueOf(isValidConfirmPass));
                break;
        }
    }


    private boolean isValidPassword(String password) {
        Pattern pattern;
        Matcher matcher;
        //TODO Check Pattern
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z]|[a-z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$";
//        pattern = Pattern.compile(PASSWORD_PATTERN);
//        matcher = pattern.matcher(password);
        if (password.length() >= 6) {
            return true;
        } else {
            passEt.setError("Your password must be more than 6 digits.");
            return false;
        }
    }

    private boolean isValidConfirmationPassword(String password, String confirmationPassword) {
        if (password.equals(confirmationPassword)) {
            return true;
        } else {
            confirmPassEt.setError("Confirmation Password is not like Password.");
            return false;
        }
    }


    private boolean isValidName(String name) {
        if (name.length() >= 3)
            return true;
        else {
            fullNameEt.setError("Name must be more than 2 digits");
            return false;
        }
    }
    private boolean isValidSecondName(String name) {
        if (name.length() >= 3)
            return true;
        else {
            fullNameEt.setError("Name must be more than 2 digits");
            return false;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}