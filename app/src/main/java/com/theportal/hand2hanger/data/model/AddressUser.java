package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressUser {

    @SerializedName("building_no")
    @Expose
    private String buildingNo;

    @SerializedName("id")
    @Expose
    private Integer addressId;

    @SerializedName("floor_no")
    @Expose
    private Integer floorNo;

    @SerializedName("flat_no")
    @Expose
    private Integer flatNo;

    @SerializedName("street")
    @Expose
    private String streetNo;

    @SerializedName("lat")
    @Expose
    private String lat;

    @SerializedName("lng")
    @Expose
    private String lng;


    @SerializedName("city")
    @Expose
    private String cityId;

    @SerializedName("region")
    @Expose
    private String regionId;

    private boolean isChecked = false;

    public String getBuildingNo() {
        return buildingNo;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public Integer getFloorNo() {
        return floorNo;
    }

    public Integer getFlatNo() {
        return flatNo;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public String getCityId() {
        return cityId;
    }

    public String getRegionId() {
        return regionId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
