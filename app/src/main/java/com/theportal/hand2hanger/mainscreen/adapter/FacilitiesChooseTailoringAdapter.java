package com.theportal.hand2hanger.mainscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.FacilityTypesResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FacilitiesChooseTailoringAdapter extends RecyclerView.Adapter<FacilitiesChooseTailoringAdapter.ViewHolder> {

    private Context context;
    private List<FacilityTypesResponse> items;
    private onChoose onChoose;
    private int mSelectedPosition = -1;
    private RadioButton mSelectedRB;

    public FacilitiesChooseTailoringAdapter(Context context, List<FacilityTypesResponse> items,
                                            FacilitiesChooseTailoringAdapter.onChoose onChoose) {
        this.context = context;
        this.items = items;
        this.onChoose = onChoose;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.spinner_custom_design_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FacilityTypesResponse model = items.get(position);
        if (model == null)
            return;

        holder.cb_spinner_tailoring.setOnClickListener(v -> {
            if(position != mSelectedPosition && mSelectedRB != null){
                mSelectedRB.setChecked(false);
            }
            mSelectedPosition = position;
            mSelectedRB = (RadioButton)v;
            onChoose.onItemChooseTailoring(model.getId(),model.getName());
        });


        if(mSelectedPosition != position){
            holder.cb_spinner_tailoring.setChecked(false);
        }else{
            holder.cb_spinner_tailoring.setChecked(true);
            if(mSelectedRB != null && holder.cb_spinner_tailoring != mSelectedRB){
                mSelectedRB = holder.cb_spinner_tailoring;
            }
        }
        holder.cb_spinner_tailoring.setText(items.get(position).getName());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RadioButton cb_spinner_tailoring;

        public ViewHolder(View itemView) {
            super(itemView);
            cb_spinner_tailoring = itemView.findViewById(R.id.cb_spinner_tailoring);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface onChoose {
        void onItemChooseTailoring(int id, String name);
    }

}
