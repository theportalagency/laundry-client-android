package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.SerializedName;

public class DataPackages {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;


    @SerializedName("cost")
    private double cost;

    @SerializedName("isActive")
    private int isActive;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public double getCost() {
        return cost;
    }

    public int getIsActive() {
        return isActive;
    }
}
