package com.theportal.hand2hanger.mainscreen;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.account.AccountFragment;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.myorders.OrdersFragment;
import com.theportal.hand2hanger.prices.PricingFragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.BackButtonUtil.hideBackButton;
import static com.theportal.hand2hanger.Utility.Constants.CURRENT_CART_NUMBER;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID_SELECT;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    @BindView(R.id.profile_navigation_btn)
    ImageButton profileButton;
    @BindView(R.id.home_navigation_btn)
    ImageButton homeButton;
    @BindView(R.id.offers_navigation_btn)
    ImageButton pricingButton;
    @BindView(R.id.orders_navigation_btn)
    ImageButton ordersButton;
    @BindView(R.id.fab)
    ImageButton fab;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.top_bar_title)
    TextView fragmentTitle;
    //    NotificationBell notificationBell;
//    NotificationCart notificationCart;
    @BindView(R.id.notification_bell_include)
    View notificationBell_view;
    @BindView(R.id.notification_cart_include)
    View notificationCart_view;
    private boolean doubleBackToExitPressedOnce;
    private int count = 0;
    private SharedPreferences pref;
    private String token;
    private APIInterface apiInterface;
    private List<OrderItem> basketItemsList;
    private NotificationCart notificationCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HideStatusBar();
        setContentView(R.layout.bottom_navigation_component);
        ButterKnife.bind(this);
        SetupToolBar();
        loadFragment(new HomeFragment());
        setButtonBackground(homeButton, ordersButton, pricingButton, profileButton);
        setupFAB(0);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0));
        token = pref.getString(TOKEN_KEY, token);
        getBasketItems();
        profileButton.setOnClickListener(v -> updateUi(R.string.account));
        homeButton.setOnClickListener(v -> updateUi(R.string.home));
        pricingButton.setOnClickListener(v -> updateUi(R.string.pricing));
        ordersButton.setOnClickListener(v -> updateUi(R.string.orders));
        notificationBell_view.setOnClickListener(v -> updateUi(R.string.notification));
        notificationCart_view.setOnClickListener(v -> updateUi(R.string.basket));
        this.fragmentTitle.setText(R.string.home);
        toolbar.setNavigationOnClickListener(view -> {
            loadFragment(new HomeFragment());
            hideBackButton(toolbar);
            setupFAB(0);
            this.fragmentTitle.setText("");
        });
        notificationCart = new NotificationCart(findViewById(R.id.notification_cart_include));
        int back = getIntent().getIntExtra("back",0);
        backSetup(back);
    }

    private void backSetup(int back) {
        if (back == 0)
            return;

        getBasketItems();
        switch (back){
            case 1:
                TAB_ID_SELECT = 1;
                loadFragment(new HomeFragment());
                hideBackButton(toolbar);
                setupFAB(0);
                setButtonBackground(homeButton, ordersButton, pricingButton, profileButton);
                this.fragmentTitle.setText(R.string.home);
                break;
            case 2:
                TAB_ID_SELECT = 2;
                loadFragment(new OrdersFragment());
                hideBackButton(toolbar);
                setupFAB(2);
                setButtonBackground(ordersButton, homeButton, pricingButton, profileButton);
                this.fragmentTitle.setText(R.string.orders);
                break;
            case 3:
                TAB_ID_SELECT = 3;
                loadFragment(new PricingFragment());
                hideBackButton(toolbar);
                setupFAB(2);
                setButtonBackground(pricingButton, homeButton, ordersButton, profileButton);
                this.fragmentTitle.setText(R.string.pricing);
                break;
            case 4:
                TAB_ID_SELECT = 4;
                loadFragment(new AccountFragment());
                hideBackButton(toolbar);
                setupFAB(1);
                setButtonBackground(profileButton, homeButton, ordersButton, pricingButton);
                this.fragmentTitle.setText(R.string.account);
                break;
        }
    }

    private void SetupToolBar() {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
    }

    private void getBasketItems() {
        Call<BasketResponse> call = apiInterface.getBasketOrders(token);
        call.enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, Response<BasketResponse> response) {
                if (response.isSuccessful()) {
                    CURRENT_CART_NUMBER = 0;
                    notificationCart = new NotificationCart(findViewById(R.id.notification_cart_include));
                    BasketResponse basketResponse = response.body();
                    if (basketResponse == null) {
                        return;
                    }
                    if (basketResponse.getData() == null) {
                        return;
                    }
                    CURRENT_CART_NUMBER = basketResponse.getData().getItems().size();
                    notificationCart = new NotificationCart(findViewById(R.id.notification_cart_include));
                }
            }
            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(int fragmentTitle) {
        Intent intent = new Intent(HomeActivity.this,SplitActivity.class);
        switch (fragmentTitle) {
            case R.string.notification:
                intent.putExtra("extra","notification");
                startActivity(intent);
                break;
            case R.string.account:
                TAB_ID_SELECT = 4;
                loadFragment(new AccountFragment());
                hideBackButton(toolbar);
                setupFAB(1);
                setButtonBackground(profileButton, homeButton, ordersButton, pricingButton);
                this.fragmentTitle.setText(R.string.account);
                break;
            case R.string.home:
                TAB_ID_SELECT = 1;
                loadFragment(new HomeFragment());
                hideBackButton(toolbar);
                setupFAB(0);
                setButtonBackground(homeButton, ordersButton, pricingButton, profileButton);
                this.fragmentTitle.setText(R.string.home);
                break;
            case R.string.orders:
                TAB_ID_SELECT = 2;
                loadFragment(new OrdersFragment());
                hideBackButton(toolbar);
                setupFAB(2);
                setButtonBackground(ordersButton, homeButton, pricingButton, profileButton);
                this.fragmentTitle.setText(R.string.orders);
                break;
            case R.string.pricing:
                TAB_ID_SELECT = 3;
                loadFragment(new PricingFragment());
                hideBackButton(toolbar);
                setupFAB(2);
                setButtonBackground(pricingButton, homeButton, ordersButton, profileButton);
                this.fragmentTitle.setText(R.string.pricing);
                break;
            case R.string.basket:
                intent.putExtra("extra","basket");
                startActivity(intent);
        }
    }

    private void setupFAB(int visibility) {
        switch (visibility) {
            case 0:
                //Home Button
                fab.setVisibility(View.GONE);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus2));
                fab.setEnabled(false);
                break;
            case 1:
                //Account Button
                fab.setVisibility(View.GONE);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                fab.setEnabled(true);
                break;
            case 2:
                fab.setVisibility(View.VISIBLE);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                fab.setEnabled(true);
                break;
        }
    }

    private void HideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        if (Constants.FRAGMENT_NUMBER == 0){
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
        else {
            loadFragment(new HomeFragment());
            setButtonBackground(homeButton, ordersButton, pricingButton, profileButton);
            this.fragmentTitle.setText(R.string.home);
        }
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    private void setButtonBackground(View yellowBtn, View blueBtn1, View blueBtn2, View blueBtn3) {
        //For Home , Order , Pricing and Account button navigation color
        yellowBtn.setBackground(getResources().getDrawable(R.drawable.navigation_iv_yellow));
        blueBtn1.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
        blueBtn2.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
        blueBtn3.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
    }

    private void setButtonBackground_basketAndNotification(View blueBtn1, View blueBtn2, View blueBtn3, View blueBtn4) {
        //For Notification and Basket button navigation color
        blueBtn4.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
        blueBtn1.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
        blueBtn2.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
        blueBtn3.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.intro_slider_container, fragment);
        fragmentTransaction.commit();
    }
}
