package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.theportal.hand2hanger.data.model.ErrorsSignUp;

public class SignupModel {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("errors")
    private ErrorsSignUp errors;

    public String getSuccess() {
        return success;
    }

    public ErrorsSignUp getError() {
        return errors;
    }
}
