package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewModel {
    @SerializedName("rate")
    @Expose
    private String rate;

    public String getRate() {
        return rate;
    }
}
