package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressData {
    @SerializedName("data")
    @Expose
    private List<AddressUser> data = null;

    public List<AddressUser> getData() {
        return data;
    }

    public void setData(List<AddressUser> data) {
        this.data = data;
    }
}
