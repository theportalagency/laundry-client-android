package com.theportal.hand2hanger.mainscreen;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.NotificationCart;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportalagency.hand2hanger.R;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.BackButtonUtil.hideBackButton;
import static com.theportal.hand2hanger.Utility.Constants.CURRENT_CART_NUMBER;
import static com.theportal.hand2hanger.Utility.Constants.NAME_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class HomeFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.dry_clean_lay)
    RelativeLayout dryCleanLay;
    @BindView(R.id.steam_press_lay)
    RelativeLayout steamPressLay;
    @BindView(R.id.wash_press_lay)
    RelativeLayout washPressLay;
    @BindView(R.id.talioring_lay)
    RelativeLayout tailoringLay;
    @BindView(R.id.name_tv)
    TextView nameTv;

    private TextView fragmentTitle;
    private Toolbar toolbar;
    private ImageButton fab;
    private SharedPreferences pref;
    private String token, name;
    private APIInterface apiInterface;
    private List<OrderItem> basketItemsList;
    private NotificationCart notificationCart;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wash_press_lay:
                updateUi(R.string.wash_press);
                break;

            case R.id.dry_clean_lay:
                updateUi(R.string.dry_clean);
                break;

            case R.id.steam_press_lay:
                updateUi(R.string.steam_press);

                break;
            case R.id.talioring_lay:
                updateUi(R.string.tailoring);
                break;
            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view1);
        toolbar = getActivity().findViewById(R.id.toolbar);
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);
        fab = getActivity().findViewById(R.id.fab);
        Constants.FRAGMENT_NUMBER = 0;
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        token = pref.getString(TOKEN_KEY, token);
        name = pref.getString(NAME_KEY, name);
        nameTv.setText("Hello " + name);

        getBasketItems();

        toolbar.setNavigationOnClickListener(view -> {
            loadFragment(new HomeFragment());
            fab.setBackground(Constants.context.getResources().getDrawable(R.drawable.ic_plus2));
            hideBackButton(toolbar);
            this.fragmentTitle.setText("");
        });
        dryCleanLay.setOnClickListener(this);
        steamPressLay.setOnClickListener(this);
        washPressLay.setOnClickListener(this);
        tailoringLay.setOnClickListener(this);

        return view1;
    }


    private void getBasketItems() {
        Call<BasketResponse> call = apiInterface.getBasketOrders(token);
        call.enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(@NonNull Call<BasketResponse> call, @NonNull Response<BasketResponse> response) {
                if (response.code() == 200 && response.body() != null && response.body().getData() != null) {
                    basketItemsList = response.body().getData().getItems();
                    CURRENT_CART_NUMBER = basketItemsList.size();
                    call.cancel();
                } else {
//                    Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<BasketResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(int fragmentTitle) {
        Intent intent = new Intent(getActivity(), SplitActivity.class);
        switch (fragmentTitle) {
            case R.string.wash_press:
                intent.putExtra("extra","wash");
                startActivity(intent);
                /*loadFragment(new WashPressFragment());
                showBackButton(toolbar);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                this.fragmentTitle.setText(R.string.wash_press);*/
                break;
            case R.string.dry_clean:
                intent.putExtra("extra","dry");
                startActivity(intent);
                /*loadFragment(new DryCleanFragment());
                showBackButton(toolbar);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                this.fragmentTitle.setText(R.string.dry_clean);*/
                break;
            case R.string.steam_press:
                intent.putExtra("extra","steam");
                startActivity(intent);
                /*loadFragment(new SteamPressFragment());
                showBackButton(toolbar);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                this.fragmentTitle.setText(R.string.steam_press);*/
                break;
            case R.string.tailoring:
                intent.putExtra("extra","tailor");
                startActivity(intent);
                /*loadFragment(new TalioringFragment());
                showBackButton(toolbar);
                fab.setBackground(getResources().getDrawable(R.drawable.ic_plus));
                this.fragmentTitle.setText(R.string.tailoring);*/
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.intro_slider_container, fragment);
            fragmentTransaction.disallowAddToBackStack();
            fragmentTransaction.commit(); // save the changes
        }
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}