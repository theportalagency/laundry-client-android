package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RedeemsPointsData {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("gift_id")
    @Expose
    private int gift_id;

    @SerializedName("gift")
    @Expose
    private Gift gift;


    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public int getGift_id() {
        return gift_id;
    }

    public Gift getGift() {
        return gift;
    }
}
