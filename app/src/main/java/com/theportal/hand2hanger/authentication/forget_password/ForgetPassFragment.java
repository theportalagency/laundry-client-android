package com.theportal.hand2hanger.authentication.forget_password;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.login.ui.LoginFragment;
import com.theportal.hand2hanger.data.model.LoginResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassFragment extends Fragment {

    @BindView(R.id.btn_submit_forget)
    Button btn_submit_forget;

    @BindView(R.id.et_forgetpw)
    EditText et_forgetpw;

   /* @BindView(R.id.icArrow)
    ImageView icArrow;*/

    @BindView(R.id.passwordConfirm_forget_et)
    EditText passwordConfirm_forget_et;

    @BindView(R.id.password_forget_et)
    EditText password_forget_et;

    @BindView(R.id.code_forget_et)
    EditText code_forget_et;

    private APIInterface apiInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        ButterKnife.bind(this,view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        btn_submit_forget.setOnClickListener(view1 -> forget());
        /*icArrow.setOnClickListener(view1 -> forget());*/
    }

    private void forget() {
        String email = et_forgetpw.getText().toString();
        if (email.equals("")){
            Toast.makeText(Constants.context, "please enter mail", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> forgetData = new HashMap<>();
        forgetData.put("email",email);
        Call<LoginResponseModel> call = apiInterface.forget_password(forgetData);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                if (response.code() == 200){
                    Toast.makeText(Constants.context, "Email Sent Succesfully please check it", Toast.LENGTH_SHORT).show();
                    password_forget_et.setVisibility(View.VISIBLE);
                    passwordConfirm_forget_et.setVisibility(View.VISIBLE);
                    code_forget_et.setVisibility(View.VISIBLE);
                    btn_submit_forget.setText("Submit");
                    et_forgetpw.setEnabled(false);
                    btn_submit_forget.setOnClickListener(view -> resetPassword());
                }
                else {
                    Toast.makeText(Constants.context, "Please Check if email registered to us or not", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

            }
        });
    }

    private void resetPassword() {
        String password = password_forget_et.getText().toString();
        String confirm_password = passwordConfirm_forget_et.getText().toString();
        String code = code_forget_et.getText().toString();
        String email = et_forgetpw.getText().toString();
        if (!password.equals(confirm_password)){
            passwordConfirm_forget_et.setError("Please Make Password the same as confirm Password");
            return;
        }
        if (code.equals("")){
            code_forget_et.setError("Please Type Code");
            return;
        }
        Map<String, String> reset = new HashMap<>();
        reset.put("email",email);
        reset.put("password",password);
        reset.put("password_confirmation",confirm_password);
        reset.put("token",code);
        Call<LoginResponseModel> call = apiInterface.reset_pass(reset);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                if (response.code() == 200){
                    Toast.makeText(Constants.context, "Succesfully Change Password", Toast.LENGTH_SHORT).show();
                    Constants.loadFragment(Constants.context,R.id.intro_slider_container,new LoginFragment());
                }
                else {
                    code_forget_et.setError("Check Code");
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}