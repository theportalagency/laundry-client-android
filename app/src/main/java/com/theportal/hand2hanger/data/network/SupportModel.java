package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupportModel {

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }
}
