package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RedeemsPoints {

    @SerializedName("data")
    @Expose
    private List<RedeemsPointsData> data;

    public List<RedeemsPointsData> getData() {
        return data;
    }
}
