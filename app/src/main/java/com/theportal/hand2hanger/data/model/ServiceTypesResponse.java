package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceTypesResponse {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("price")
    @Expose
    private Integer price;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("item_id")
    @Expose
    private Integer item_id;

    //

    @SerializedName("order_id")
    @Expose
    private Integer order_id;

    @SerializedName("count")
    @Expose
    private Integer itemsCount;

    @SerializedName("service_id")
    @Expose
    private Integer service_id;

    public int getService_id() {
        return service_id;
    }

    public ServiceTypesResponse(String name, Integer price, Integer id, int count) {
        this.name = name;
        this.price = price;
        this.id = id;
    }


    public Integer getItemsCount() {
        return itemsCount;
    }

    public Integer getId() {
        return id;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
