package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FullOrderResponseModel {


    @SerializedName("data")
    @Expose
    private List<FullOrderResponseDataModel> data = null;

    @SerializedName("meta")
    @Expose
    private MetaDataOrder meta;

    @SerializedName("links")
    @Expose
    private LinksOrder links;

    public List<FullOrderResponseDataModel> getData() {
        return data;
    }

    public MetaDataOrder getMeta() {
        return meta;
    }

    public LinksOrder getLinks() {
        return links;
    }
}
