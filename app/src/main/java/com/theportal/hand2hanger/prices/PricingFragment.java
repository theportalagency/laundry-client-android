package com.theportal.hand2hanger.prices;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.tabs.TabLayout;
import com.theportal.hand2hanger.CategriesModel;
import com.theportal.hand2hanger.DynamicFragment;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.Utility.PlansPagerAdapter;
import com.theportal.hand2hanger.data.ServiceModelData;
import com.theportal.hand2hanger.data.ServicesModel;
import com.theportal.hand2hanger.data.model.Pricing;
import com.theportal.hand2hanger.data.model.PricingItem;
import com.theportal.hand2hanger.data.model.PricingTypes;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeFragment;
import com.theportalagency.hand2hanger.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.CHILDREN_CATEGORY_ID;
import static com.theportal.hand2hanger.Utility.Constants.HOUSE_CATEGORY_ID;
import static com.theportal.hand2hanger.Utility.Constants.MEN_CATEGORY_ID;
import static com.theportal.hand2hanger.Utility.Constants.SERVICE_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_ID;
import static com.theportal.hand2hanger.Utility.Constants.TAB_NAME;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.WOMEN_CATEGORY_ID;

public class PricingFragment extends Fragment {

    private ImageButton fab, homeButton, ordersButton;
    private TextView fragmentTitle;

    private List<PricingTypes> pricingList;
    private PricingAdapter pricingAdapter;
    private APIInterface apiInterface;
    private SharedPreferences pref;
    private String token;
    @BindView(R.id.tabs)
    TabLayout tab;
    @BindView(R.id.tab_parent)
    TabLayout tab_parent;
    @BindView(R.id.rv_pricing)
    RecyclerView rv_pricing;
    private int tab_parent_id, service_id;
    private int tab_child_id;
    private List<Integer> tabId = new ArrayList<>();
    private List<String> tabName = new ArrayList<>();


    @BindView(R.id.ll_pricing)
    LinearLayout ll_pricing;
    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricing, container, false);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        Constants.FRAGMENT_NUMBER = 2;
        token = pref.getString(TOKEN_KEY, token);
        ButterKnife.bind(this, view);
        fab = Objects.requireNonNull(getActivity()).findViewById(R.id.fab);
        homeButton = getActivity().findViewById(R.id.home_navigation_btn);
        ordersButton = getActivity().findViewById(R.id.orders_navigation_btn);
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);
        fab.setOnClickListener(v -> {
            fab.setBackground(getResources().getDrawable(R.drawable.ic_plus2));
            fab.setEnabled(false);
            setButtonBackground();
            loadFragment(new HomeFragment());
            fragmentTitle.setText("");
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        getServices();
        if (tab.getTabCount() == 4) {
            tab.setTabMode(TabLayout.MODE_FIXED);
        } else if (tab.getTabCount() > 4) {
            tab.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        if (tab_parent.getTabCount() == 4) {
            tab_parent.setTabMode(TabLayout.MODE_FIXED);
        } else if (tab_parent.getTabCount() > 4) {
            tab_parent.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }

    private void getServices() {
        Call<ServicesModel> call = apiInterface.services(token);
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel model = response.body();
               ll_pricing.setVisibility(View.VISIBLE);
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);

                if (model == null) return;
                List<ServiceModelData> serviceModelData = model.getData();
                for (int i = 0; i < serviceModelData.size(); i++) {
                    tab_parent.addTab(tab_parent.newTab().setText("" + serviceModelData.get(i).getName()));
                    tabId.add(serviceModelData.get(i).getId());
                    tabName.add(serviceModelData.get(i).getName());
                }
                service_id = tabId.get(0);
                initTabs();
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                ll_noConnection.setVisibility(View.GONE);
                ll_pricing.setVisibility(View.VISIBLE);
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);
            }
        });
    }

    private void initTabs() {
        getCategories();
        tab_parent.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                service_id = tabId.get(pos);
                getCategories();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void getCategories() {
        tab.removeAllTabs();
        Call<List<CategriesModel>> call = apiInterface.categories(token);
        call.enqueue(new Callback<List<CategriesModel>>() {
            @Override
            public void onResponse(Call<List<CategriesModel>> call, Response<List<CategriesModel>> response) {
                List<CategriesModel> models = response.body();
                if (models == null) return;
                for (int i = 0; i < models.size(); i++) {
                    tab.addTab(tab.newTab().setText("" + models.get(i).getName()));
                }

                tab_child_id = models.get(0).getId();
                getPricing();
                /*PlansPagerAdapter adapter = new PlansPagerAdapter
                        (Objects.requireNonNull(getActivity()).getSupportFragmentManager(), tab.getTabCount());
                vp.setAdapter(adapter);
                vp.setOffscreenPageLimit(1);
                vp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));
                tab.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(vp));*/
                tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tab_child_id = models.get(tab.getPosition()).getId();
                        getPricing();
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<CategriesModel>> call, Throwable t) {

            }
        });
    }

    private void getPricing() {
        Call<Pricing> call = apiInterface.getItemPrice(token, service_id);
        call.enqueue(new Callback<Pricing>() {
            @Override
            public void onResponse(@NonNull Call<Pricing> call, @NonNull Response<Pricing> response) {
                if (response.isSuccessful()) {
                    Pricing pricing = response.body();
                    if (pricing == null) return;
                    List<PricingItem> pricingItems = pricing.getData();
                    for (int i = 0; i < pricingItems.size(); i++) {
                        int id = pricingItems.get(i).getId();
                        if (id == tab_child_id) {
                            pricingList = pricingItems.get(i).getTypes();
                            updateUi(pricingList);
                            return;
                        }
                    }

                } else {
                    Toast.makeText(getActivity(), Constants.context.getResources().getText(R.string.check_net), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Pricing> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi(List<PricingTypes> itemList) {
        pricingAdapter = new PricingAdapter(itemList, getActivity());
        rv_pricing.setHasFixedSize(true);
        rv_pricing.setNestedScrollingEnabled(false);
        rv_pricing.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_pricing.setAdapter(pricingAdapter);
    }


    private void setButtonBackground() {
        homeButton.setBackground(getResources().getDrawable(R.drawable.navigation_iv_yellow));
        ordersButton.setBackground(getResources().getDrawable(R.drawable.navigation_iv_blue));
    }


    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(fm).beginTransaction();
        fragmentTransaction.add(R.id.intro_slider_container, fragment);
        fragmentTransaction.disallowAddToBackStack();
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}
