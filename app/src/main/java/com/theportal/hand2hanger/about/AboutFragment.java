package com.theportal.hand2hanger.about;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutFragment extends Fragment {
    //    @BindView(R.id.aboutWebview)
//    WebView aboutWebView;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    @BindView(R.id.animationView)
    LottieAnimationView animationView;
    @BindView(R.id.ll_noConnection)
    LinearLayout ll_noConnection;
    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.about_tv)
    TextView aboutTextView;
    private APIInterface apiInterface;
    private String pageUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        getPageUrl();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        counterPanel_.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.about);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
    }

    private void setupPage(String about) {
        aboutTextView.setText(about);
    }

    private void getPageUrl() {
        Call<SignupResponseModel> call = apiInterface.getAboutResponse();
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                switch (response.code()) {
                    case 200: {
                        animationView.cancelAnimation();
                        animationView.setVisibility(View.GONE);
                        pageUrl = response.body().getAbout();
                        setupPage(pageUrl);
                    }
                    break;
                    default:

                        animationView.setVisibility(View.GONE);
                        animationView.cancelAnimation();
                        setupPage("No Result Found");
                        break;

                }

            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                ll_noConnection.setVisibility(View.VISIBLE);
                animationView.cancelAnimation();
                animationView.setVisibility(View.GONE);
                call.cancel();
            }
        });

    }
}
