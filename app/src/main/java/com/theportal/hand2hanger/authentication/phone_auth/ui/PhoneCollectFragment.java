package com.theportal.hand2hanger.authentication.phone_auth.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.login.ui.LoginFragment;
import com.theportal.hand2hanger.authentication.signup.ui.SignUpFragment;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.theportal.hand2hanger.Utility.Constants.USER_PHONE_NUMBER_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;
import static com.theportal.hand2hanger.Utility.Constants.context;

public class PhoneCollectFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    @Nullable
    @BindView(R.id.signup_btn)
    Button nextBtn;
    @BindView(R.id.et_number)
    EditText PhoneNumber_Et;
    @BindView(R.id.already_amember_tv)
    TextView alreadyMember_TV;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    String code;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone_signup, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        assert nextBtn != null;
        code = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(() -> {
            code = ccp.getSelectedCountryCodeWithPlus();
        });
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential credential) {
                Constants.animateFragmentAuthActivity(Constants.context,new SignUpFragment());
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                PhoneNumber_Et.setError("Enter right number");
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                Log.e("haaaaaaaaaaaaaaaaa", "onCodeSent:" + verificationId);
                Constants.mResendToken = token;
                Constants.mVerificationId = verificationId;
                Constants.animateFragmentAuthActivity(Constants.context,new PhoneAuhFragment());
            }
        };

        nextBtn.setOnClickListener(view1 -> {
            if (validateInput()) {
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        Constants.NUMBER,        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        getActivity(),               // Activity (for callback binding)
                        mCallbacks);             // ForceResendingToken from callbacks
            }
        });
        alreadyMember_TV.setOnClickListener(v ->
                Constants.animateFragmentAuthActivity(Constants.context,new LoginFragment())
        );
        return view;
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private boolean validateInput() {
        if (code.equals(""))
            code = ccp.getSelectedCountryCodeWithPlus();
        if (PhoneNumber_Et.getText().toString().equals("")) {
            PhoneNumber_Et.setError("Please Enter Number");
            return false;
        } else {
            Constants.NUMBER = code + PhoneNumber_Et.getText().toString();
            return true;
        }
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
    }
}
