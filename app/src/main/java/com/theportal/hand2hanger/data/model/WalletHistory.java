package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.theportal.hand2hanger.wallet.HistoryModel;

import java.util.List;

public class WalletHistory {

    @SerializedName("data")
    @Expose
    private List<HistoryModel> data;

    public List<HistoryModel> getData() {
        return data;
    }
}
