package com.theportal.hand2hanger.authentication.phone_auth.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.login.ui.LoginFragment;
import com.theportal.hand2hanger.authentication.phone_auth.ui.PhoneCollectFragment;
import com.theportal.hand2hanger.authentication.signup.ui.SignUpFragment;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.authentication.signup.ui.SignUpFragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.theportal.hand2hanger.Utility.Constants.USER_PHONE_NUMBER_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class PhoneAuhFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "PhoneAuthActivity";
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    @BindView(R.id.verfication_num_1_et)
    EditText mVerificationNum1_ET;
    @BindView(R.id.verfication_num_2_et)
    EditText mVerificationNum2_ET;
    @BindView(R.id.verfication_num_3_et)
    EditText mVerificationNum3_ET;
    @BindView(R.id.verfication_num_4_et)
    EditText mVerificationNum4_ET;
    @BindView(R.id.verfication_num_5_et)
    EditText mVerificationNum5_ET;
    @BindView(R.id.verfication_num_6_et)
    EditText mVerificationNum6_ET;
    @BindView(R.id.timer_tv)
    TextView timerTV;
    @BindView(R.id.verify_button)
    Button mVerifyButton;
    @BindView(R.id.sent_it_again_tv)
    TextView mResend_TV;
    @BindView(R.id.tv_change_number)
    TextView tv_change_number;
    private SharedPreferences pref;
    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String phoneNumber;
    private String code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone_code_verify, container, false);
        ButterKnife.bind(this, view);
        pref = getActivity().getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        mResend_TV.setOnClickListener(this);
        mVerifyButton.setOnClickListener(this);
        tv_change_number.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
        phoneNumber = Constants.NUMBER;
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential credential) {
                Log.e("haaaaaaaaaaaaaaaaa", "onVerificationCompleted:" + credential);
                Constants.animateFragmentAuthActivity(Constants.context,new SignUpFragment());
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Log.e("haaaaaaaaaaaaaaaaa", "onVerificationFailed", e);
                Toast.makeText(Constants.context, ""+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                Log.e("haaaaaaaaaaaaaaaaa", "onCodeSent:" + verificationId);
                Constants.mVerificationId = verificationId;
                Constants.mResendToken = token;
            }
        };
        mVerificationNum1_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
               /* if(mVerificationNum1_ET.getText().toString().trim().length()>=1){
                    mVerificationNum2_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mVerificationNum1_ET.getText().toString().trim().length()>=1){
                    mVerificationNum1_ET.clearFocus();
                    mVerificationNum2_ET.requestFocus();
                    mVerificationNum2_ET.setCursorVisible(true);
                }
            }
        });
        mVerificationNum2_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
               /* if(mVerificationNum2_ET.getText().toString().trim().length()>=1){
                    mVerificationNum3_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mVerificationNum2_ET.getText().toString().trim().length()>=1){
                    mVerificationNum2_ET.clearFocus();
                    mVerificationNum3_ET.requestFocus();
                    mVerificationNum3_ET.setCursorVisible(true);
                }
            }
        });
        mVerificationNum3_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
               /* if(mVerificationNum3_ET.getText().toString().trim().length()>=1){
                    mVerificationNum4_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mVerificationNum3_ET.getText().toString().trim().length()>=1){
                    mVerificationNum3_ET.clearFocus();
                    mVerificationNum4_ET.requestFocus();
                    mVerificationNum4_ET.setCursorVisible(true);
                }
            }
        });
        mVerificationNum4_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
                /*if(mVerificationNum4_ET.getText().toString().trim().length()>=1){
                    mVerificationNum5_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mVerificationNum4_ET.getText().toString().trim().length()>=1){
                    mVerificationNum4_ET.clearFocus();
                    mVerificationNum5_ET.requestFocus();
                    mVerificationNum5_ET.setCursorVisible(true);
                }
            }
        });
        mVerificationNum5_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
                /*if(mVerificationNum5_ET.getText().toString().trim().length()>=1){
                    mVerificationNum6_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mVerificationNum5_ET.getText().toString().trim().length()>=1){
                    mVerificationNum5_ET.clearFocus();
                    mVerificationNum6_ET.requestFocus();
                    mVerificationNum6_ET.setCursorVisible(true);
                }
            }
        });
        mVerificationNum5_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // specify length of your editext here to move on next edittext
                /*if(mVerificationNum5_ET.getText().toString().trim().length()>=1){
                    mVerificationNum6_ET.requestFocus();
                }*/
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                code = mVerificationNum1_ET.getText().toString() +
                        mVerificationNum2_ET.getText().toString() + mVerificationNum3_ET.getText().toString()
                        + mVerificationNum4_ET.getText().toString() + mVerificationNum5_ET.getText().toString()
                        + mVerificationNum6_ET.getText().toString();
               //verifyPhoneNumberWithCode(Constants.mVerificationId,code);
            }
        });
        startTimer();
        return view;
    }

    private void startTimer() {
        mResend_TV.setVisibility(View.INVISIBLE);
        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                timerTV.setText("Seconds Remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                timerTV.setText("");
                mResend_TV.setVisibility(View.VISIBLE);
            }

        }.start();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
        Toast.makeText(Constants.context, "Verifying...", Toast.LENGTH_SHORT).show();

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");

                        Constants.animateFragmentAuthActivity(Constants.context,new SignUpFragment());
                        // ...
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Toast.makeText(Constants.context, ""+task.getException(), Toast.LENGTH_SHORT).show();

                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                        }
                    }
                });
    }

    private void resendVerificationCode(String phoneNumber) {
        startTimer();
        mVerificationNum1_ET.setText("");
        mVerificationNum2_ET.setText("");
        mVerificationNum3_ET.setText("");
        mVerificationNum4_ET.setText("");
        mVerificationNum5_ET.setText("");
        mVerificationNum6_ET.setText("");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                getActivity(),               // Activity (for callback binding)
                mCallbacks,
                Constants.mResendToken);             // ForceResendingToken from callbacks
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.verify_button:
                 code = mVerificationNum1_ET.getText().toString() +
                        mVerificationNum2_ET.getText().toString() + mVerificationNum3_ET.getText().toString()
                        + mVerificationNum4_ET.getText().toString() + mVerificationNum5_ET.getText().toString()
                        + mVerificationNum6_ET.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationNum1_ET.setError("Cannot be empty.");
                    mVerificationNum2_ET.setError("Cannot be empty.");
                    mVerificationNum3_ET.setError("Cannot be empty.");
                    mVerificationNum4_ET.setError("Cannot be empty.");
                    mVerificationNum5_ET.setError("Cannot be empty.");
                    mVerificationNum6_ET.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(Constants.mVerificationId, code);
                break;
            case R.id.sent_it_again_tv:
                resendVerificationCode(phoneNumber);
                break;
            case R.id.tv_change_number:
                changeNumber();
                break;
        }
    }

    private void changeNumber() {
        Constants.animateFragmentAuthActivity(Constants.context,new PhoneCollectFragment());
    }
}

