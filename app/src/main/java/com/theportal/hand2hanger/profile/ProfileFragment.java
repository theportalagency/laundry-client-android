package com.theportal.hand2hanger.profile;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hbb20.CountryCodePicker;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.BasicErrorModel;
import com.theportal.hand2hanger.data.model.EditProfileResponse;
import com.theportal.hand2hanger.data.model.ImgBody;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportalagency.hand2hanger.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.theportal.hand2hanger.Utility.Constants.DATE_OF_BIRTH_KEY;
import static com.theportal.hand2hanger.Utility.Constants.EMAIL_KEY;
import static com.theportal.hand2hanger.Utility.Constants.GENDER_KEY;
import static com.theportal.hand2hanger.Utility.Constants.IMAGE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.NAME_KEY;
import static com.theportal.hand2hanger.Utility.Constants.PHONE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.POINTS_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";
    @BindView(R.id.expandable_email_lay)
    ExpandableLayout emailExpandableLayout;
    //@BindView(R.id.expandable_name_lay)
    //ExpandableLayout nameExpandableLayout;
    @BindView(R.id.expandable_pass_lay)
    ExpandableLayout passExpandableLayout;
    @BindView(R.id.expandable_phone_lay)
    ExpandableLayout phoneExpandableLayout;
    @BindView(R.id.edit_user_full_name_iv)
    ImageButton nameEditImageView;
    @BindView(R.id.edit_user_email_iv)
    ImageButton emailEditImageView;
    @BindView(R.id.edit_user_phone_iv)
    ImageButton phoneEditImageView;
    @BindView(R.id.edit_user_date_of_birth_iv)
    ImageButton dateOfBirthEditImageView;
    @BindView(R.id.edit_user_password_iv)
    ImageButton passEditImageView;
    @BindView(R.id.edit_user_gender_iv)
    ImageButton genderEditImageView;
    @BindView(R.id.edit_profile_phone)
    EditText edit_phone_code;
    @BindView(R.id.user_profile_image_iv)
    CircleImageView userProfileImageIv;
    @BindView(R.id.user_name)
    TextView userName_tv;
    @BindView(R.id.user_points_tv)
    TextView userPoints;
    @BindView(R.id.edit_pass_current_pass)
    EditText editPassCurrentPass;
    @BindView(R.id.edit_pass_new_pass)
    EditText editPassNewPass;
    @BindView(R.id.edit_pass_confirm_new_pass)
    EditText editPassConfirmNewPass;
    @BindView(R.id.submit_change_pass_btn)
    Button submitChangePassBtn;

    EditText editProfilePhone;

    @BindView(R.id.edit_phone_current_pass)
    EditText editPhoneCurrentPass;
    @BindView(R.id.edit_phone_confirm_current_pass)
    EditText editPhoneConfirmCurrentPass;
    @BindView(R.id.submit_change_phone_btn)
    Button submitChangePhoneBtn;
    @BindView(R.id.edit_profile_new_email)
    EditText editProfileNewEmail;
    @BindView(R.id.edit_email_current_password)
    EditText editEmailCurrentPassword;
    @BindView(R.id.edit_email_confirm_pass)
    EditText editEmailConfirmPass;
    @BindView(R.id.submit_edit_email_btn)
    Button submitEditEmailBtn;
    @BindView(R.id.user_full_name)
    TextView userFullName_tv;
    @BindView(R.id.textView29)
    TextView textView29;
    @BindView(R.id.user_email)
    TextView userEmail_tv;
    @BindView(R.id.constraintLayout5)
    ConstraintLayout constraintLayout5;
    @BindView(R.id.user_gender)
    TextView userGender_tv;
    @BindView(R.id.user_date_of_birth)
    TextView userDateOfBirth_tv;
    @BindView(R.id.user_password)
    TextView userPassword_tv;
    @BindView(R.id.user_phone_number)
    TextView userPhoneNumber_tv;
    @BindView(R.id.expandable_name_lay)
    ExpandableLayout expandableNameLay;
    @BindView(R.id.expandable_date_lay)
    ExpandableLayout expandableDateLay;
    @BindView(R.id.expandable_gender_lay)
    ExpandableLayout expandableGenderLay;
    @BindView(R.id.edit_profile_new_name)
    EditText editProfileNewName;
    @BindView(R.id.edit_profile_new_date)
    EditText editProfileNewDate;
    @BindView(R.id.gender_radio_group)
    RadioGroup genderRadioGroup;
    @BindView(R.id.radio_male)
    RadioButton radioMale;
    @BindView(R.id.radio_female)
    RadioButton radioFemale;

    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    @BindView(R.id.layoutVerificationCode)
    RelativeLayout layoutVerificationCode;
    @BindView(R.id.ccpEdit)
    CountryCodePicker ccp;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.ivNextCode)
    ImageView ivNextCode;
    @BindView(R.id.etCode)
    EditText etCode;

    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int i = 1;
    private Map<String, Object> editEmailMap = new HashMap<>();
    private Map<String, Object> editNameMap = new HashMap<>();
    private Map<String, Object> editGenderMap = new HashMap<>();
    private Map<String, Object> editDateMap = new HashMap<>();
    private Map<String, Object> editPhoneMap = new HashMap<>();
    private Map<String, Object> editPasswordMap = new HashMap<>();
    private APIInterface apiInterface;
    private String token, name, emailAddress, phoneNumber, DateOfBirth, gender, collectionDate;
    private int RESULT_LOAD_IMAGE = 1;
    private String image;
    private ProgressDialog dialog;
    private String phone;
    private String code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
//        editor.apply();
        mAuth = FirebaseAuth.getInstance();
        token = pref.getString(TOKEN_KEY, token);
        name = pref.getString(NAME_KEY, name);
        userName_tv.setText("" + name);
        userFullName_tv.setText("" + name);
        emailAddress = pref.getString(EMAIL_KEY, emailAddress);
        userEmail_tv.setText("" + emailAddress);

        editProfilePhone = view.findViewById(R.id.edit_profile_phone);
        code = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(() -> {
            code = ccp.getSelectedCountryCodeWithPlus();
        });

        phoneNumber = pref.getString(PHONE_KEY, phoneNumber);
        if (phoneNumber == null || phoneNumber.equals(""))
            phoneExpandableLayout.expand();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential credential) {
                layoutVerificationCode.setVisibility(View.VISIBLE);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                editProfilePhone.setError("Enter right number" + e.getMessage());
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                layoutVerificationCode.setVisibility(View.VISIBLE);
                Constants.mVerificationId = verificationId;
            }
        };

        ivNext.setOnClickListener(view1 -> {
            phoneNumber = code + edit_phone_code.getText().toString();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    getActivity(),               // Activity (for callback binding)
                    mCallbacks);
        });
        ivNextCode.setOnClickListener(view1 -> {
            String verify_code = etCode.getText().toString();
            verifyPhoneNumberWithCode(Constants.mVerificationId, verify_code);
        });

        image = Constants.BASE_URL + "uploads/" + pref.getString(IMAGE_KEY, image);
        Glide.with(Constants.context)
                .load(image)
                .placeholder(R.drawable.placeholder)
                .into(userProfileImageIv);

        dialog = new ProgressDialog(getActivity());
        DateOfBirth = pref.getString(DATE_OF_BIRTH_KEY, DateOfBirth);
        if (DateOfBirth == null)
            DateOfBirth = "";
        if (phoneNumber == null)
            phoneNumber = "";
        userPhoneNumber_tv.setText("" + phoneNumber);
        userDateOfBirth_tv.setText("" + DateOfBirth);
        gender = pref.getString(GENDER_KEY, gender);
        checkGender("" + gender);
        datePickerFromET();
        updateUi();
        counterPanel_.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.profile);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        userProfileImageIv.setOnClickListener(view12 -> changePic());
        emailEditImageView.setOnClickListener(v -> getViewExpandAndCollapse(emailExpandableLayout, phoneExpandableLayout,
                passExpandableLayout, expandableNameLay, expandableDateLay, expandableGenderLay));
        phoneEditImageView.setOnClickListener(view13 -> {
            getViewExpandAndCollapse(phoneExpandableLayout,
                    emailExpandableLayout, passExpandableLayout, expandableNameLay, expandableDateLay, expandableGenderLay);
            editPhoneCurrentPass.setEnabled(true);
            editPhoneConfirmCurrentPass.setEnabled(true);
            edit_phone_code.setEnabled(true);
        });

        passEditImageView.setOnClickListener(v ->
                getViewExpandAndCollapse(passExpandableLayout, emailExpandableLayout,
                        phoneExpandableLayout, expandableNameLay, expandableDateLay, expandableGenderLay));

        nameEditImageView.setOnClickListener(v ->
                getViewExpandAndCollapse(expandableNameLay, emailExpandableLayout,
                        phoneExpandableLayout, passExpandableLayout, expandableDateLay, expandableGenderLay));

        dateOfBirthEditImageView.setOnClickListener(v ->
                getViewExpandAndCollapse(expandableDateLay, emailExpandableLayout,
                        phoneExpandableLayout, passExpandableLayout, expandableNameLay, expandableGenderLay));

        genderEditImageView.setOnClickListener(v ->
                getViewExpandAndCollapse(expandableGenderLay, emailExpandableLayout,
                        phoneExpandableLayout, passExpandableLayout, expandableNameLay, expandableDateLay));

        return view;
    }

    private void changePic() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE &&
                resultCode == RESULT_OK && null != data) {
            try {
                final Uri uriImage = data.getData();
                final InputStream inputStream = getActivity().getContentResolver().openInputStream(uriImage);
                final Bitmap imageMap = BitmapFactory.decodeStream(inputStream);
                File file = persistImage(imageMap, "amr");
                editImage(file, imageMap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(Constants.context, "Image was not found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void editImage(File file, Bitmap imageMap) {
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Please Wait");
        dialog.setCancelable(false);
        dialog.show();
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        Call<ImgBody> call = apiInterface.editImage(token, body);
        call.enqueue(new Callback<ImgBody>() {
            @Override
            public void onResponse(Call<ImgBody> call, Response<ImgBody> response) {
                ImgBody body = response.body();
                if (body == null) return;
                if (response.code() == 200) {
                    Toast.makeText(Constants.context, body.getSuccess(), Toast.LENGTH_SHORT).show();
                    String photo = body.getLink();
                    editor.putString(IMAGE_KEY, photo);
                    editor.apply();
                    userProfileImageIv.setImageBitmap(imageMap);
                    dialog.dismiss();
                } else {
                    Toast.makeText(Constants.context, "Can't Edit Try Again", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ImgBody> call, Throwable t) {

            }
        });
    }

    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }


    private String convertGenderToString(String gender) {
        String type = "";
        switch ("" + gender) {
            case "1":
                userGender_tv.setText(R.string.male);
                type = "Male";
                break;
            case "null":
                userGender_tv.setText("" + null);
                break;
            case "2":
                userGender_tv.setText(R.string.female);
                type = "Female";
                break;
        }
        return type;
    }

    private void checkGender(String gender) {
        switch (gender) {
            case "1":
            case "null":
                radioMale.isChecked();
                break;
            case "2":
                radioFemale.isChecked();
                break;
        }

    }

    private void getViewExpandAndCollapse(ExpandableLayout extendAndCollapse, ExpandableLayout collapseView1,
                                          ExpandableLayout collapseView2, ExpandableLayout collapseView3,
                                          ExpandableLayout collapseView4, ExpandableLayout collapseView5) {
        if (i == 1) {
            //Single click
            i++;
            extendAndCollapse.expand();
            collapseView1.collapse(true);
            collapseView2.collapse(true);
            collapseView3.collapse(true);
            collapseView4.collapse(true);
            collapseView5.collapse(true);
        } else if (i == 2) {
            i--;
            extendAndCollapse.collapse(true);
        }
    }

    @OnClick({R.id.submit_change_pass_btn, R.id.submit_change_phone_btn, R.id.submit_edit_email_btn,
            R.id.submit_edit_name_btn, R.id.submit_edit_gender_btn, R.id.submit_edit_date_btn})
    void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit_change_pass_btn:
                changePass();
                break;
            case R.id.submit_change_phone_btn:
                changePhone();
                break;
            case R.id.submit_edit_email_btn:
                changeEmail();
                break;
            case R.id.submit_edit_name_btn:
                changeName();
                break;
            case R.id.submit_edit_gender_btn:
                changeGender();
                break;
            case R.id.submit_edit_date_btn:
                changeDate();
                break;
        }
    }


    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        prepareToChangeMobileStep2();
                    } else {
                        Toast.makeText(Constants.context, "Code is wrong try again", Toast.LENGTH_SHORT).show();
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                        }
                    }
                });
    }

    private void prepareToChangeMobileStep2() {
        editPhoneCurrentPass.setVisibility(View.VISIBLE);
        submitChangePhoneBtn.setVisibility(View.VISIBLE);
        etCode.setEnabled(false);
        editProfilePhone.setEnabled(false);
        editPhoneConfirmCurrentPass.setVisibility(View.VISIBLE);
    }


    private boolean isNotEmpty(Editable editField, Editable Password, Editable confirmPassword) {
        return editField != null && Password != null && confirmPassword != null;
    }

    private boolean isNotEmptyNameGenderDate(Editable editField) {
        return editField != null;
    }

    private void updateUi() {
        userPoints.setText("MY Points: " + pref.getInt(POINTS_KEY, 0));
    }

    private void editEmailRequest(String newEmail, String currentPassword, String newPassword) {
        editEmailMap.put("email", newEmail);
        editEmailMap.put("password", currentPassword);
        editEmailMap.put("password_confirmation", newPassword);

        Call<EditProfileResponse> call = apiInterface.editEmail(token, editEmailMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                Log.e("Signup Code:", response.code() + "");
                Log.e("Signup Message:", response.message() + "");
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Kindly check you email address" +
                                (response.body() != null ? response.body().getMsg() : null), Toast.LENGTH_LONG).show();
                        editor.putString(EMAIL_KEY, newEmail);
                        userEmail_tv.setText(newEmail);
                        clearEt();
                        emailExpandableLayout.collapse();
                    }
                } else {
                    Toast.makeText(getActivity(), "Email is Taken before", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void editNameRequest(String newName) {
        editNameMap.put("name", newName);
        Call<EditProfileResponse> call = apiInterface.editName(token, editNameMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                Log.e("Signup Code:", response.code() + "");
                Log.e("Signup Message:", response.message() + "");
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Updated Successfully" +
                                (response.body() != null ? response.body().getMsg() : null), Toast.LENGTH_LONG).show();
                        editProfileNewName.getText().clear();
                        editor.putString(NAME_KEY, newName);
                        userName_tv.setText("" + newName);
                        userFullName_tv.setText("" + newName);
                        editor.apply();
                        expandableNameLay.collapse();
                    }
                } else {
                    Toast.makeText(getActivity(), "Code :" + response.code(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void editGenderRequest(String newGender) {
        editGenderMap.put("gender", newGender);

        Call<EditProfileResponse> call = apiInterface.editGender(token, editGenderMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                Log.e("Signup Code:", response.code() + "");
                Log.e("Signup Message:", response.message() + "");
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Updated Successfully" +
                                (response.body() != null ? response.body().getMsg() : null)
                                , Toast.LENGTH_LONG).show();
                        String genderType = convertGenderToString(gender);
                        editor.putString(GENDER_KEY, genderType);
                        editor.apply();
                        expandableGenderLay.collapse();
                    }
                } else {
                    //TODO not work with female because it required true or false not 1 or 2 !!! (Zahran Edit)
                    Toast.makeText(getActivity(), "Code :" + response.code(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void editDateOfBirthRequest(String newDate) {
        editDateMap.put("date_of_birth", newDate);

        Call<EditProfileResponse> call = apiInterface.editDateOFBirth(token, editDateMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                EditProfileResponse editProfileResponse = response.body();
                if (editProfileResponse == null) return;
                Toast.makeText(Constants.context, "" + editProfileResponse.getMsg(), Toast.LENGTH_SHORT).show();
                editProfileNewName.getText().clear();
                editor.putString(DATE_OF_BIRTH_KEY, newDate);
                userDateOfBirth_tv.setText("" + newDate);
                editor.apply();
                expandableDateLay.collapse();
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void editPhoneRequest(String newPhone, String currentPassword, String confirmPassword) {
        editPhoneMap.put("phone", newPhone);
        editPhoneMap.put("password", currentPassword);
        editPhoneMap.put("password_confirmation", confirmPassword);

        Call<EditProfileResponse> call = apiInterface.editPhone(token, editPhoneMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                if (response.isSuccessful()) {
                    editor.putString(PHONE_KEY, newPhone);
                    editor.apply();
                    userPhoneNumber_tv.setText(newPhone);
                    clearEt();
                    phoneExpandableLayout.collapse();
                    Toast.makeText(getActivity(), "phone updated success", Toast.LENGTH_SHORT).show();

                } else {
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = null;
                    try {
                        mJson = parser.parse(response.errorBody().string());
                        Gson gson = new Gson();
                        try {
                            BasicErrorModel errorResponse = gson.fromJson(mJson, BasicErrorModel.class);
                            if (errorResponse != null && errorResponse.getErrors().getPhone()[0] != null){
                                Toast.makeText(Constants.context, ""+errorResponse.getErrors().getPhone()[0], Toast.LENGTH_SHORT).show();
                            }
                            if (errorResponse != null && errorResponse.getErrors().getPassword()[0] != null){
                                Toast.makeText(Constants.context, ""+errorResponse.getErrors().getPassword()[0], Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }


            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void editPasswordRequest(String currentPassword, String pass, String confirm_pass) {
        editPasswordMap.put("old_password", currentPassword);
        editPasswordMap.put("password", pass);
        editPasswordMap.put("password_confirmation", confirm_pass);

        Call<EditProfileResponse> call = apiInterface.editPass(token, editPasswordMap);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                Log.e("Signup Code:", response.code() + "");
                Log.e("Signup Message:", response.message() + "");
                if (response.code() == 200) {
                    Toast.makeText(getActivity(), "Password Updated Succesfully" + response.body().getMsg(), Toast.LENGTH_LONG).show();
                    clearEt();
                    passExpandableLayout.collapse();
                } else {
                    Toast.makeText(getActivity(), "Please check data you inserted", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void clearEt() {
        editProfileNewEmail.getText().clear();
        editEmailCurrentPassword.getText().clear();
        editEmailConfirmPass.getText().clear();
    }

    private boolean checkPasswordMatching(EditText password, EditText confirmPassword) {
        boolean isValid = true;
        if (!(password.getText().toString().equals(confirmPassword.getText().toString()))) {
            isValid = false;
            confirmPassword.setError("The Password Doesn't Match!");
        }
        return isValid;
    }

    private boolean isValidEmail(String email) {
        boolean isValid = true;
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValid = false;
            editProfileNewEmail.setError("Wrong Email Format.");
        }
        return isValid;
    }


    private boolean isValidName(String name) {
        //TODO Edit Number of phone number
        boolean isValid = true;
        if (!(name.length() >= 4)) {
            editProfilePhone.setError("Enter at least 4 char");
            isValid = false;
        }
        return isValid;
    }

    private boolean isValidPassword(String phone) {
        boolean isValid = true;
        if (!(phone.length() >= 6)) {
            editPassNewPass.setError("Enter at least 6 number");
            isValid = false;
        }
        return isValid;
    }


    private void changeEmail() {
        if (isNotEmpty(editProfileNewEmail.getText(), editEmailCurrentPassword.getText(),
                editEmailConfirmPass.getText())) {
            if (isValidEmail(editProfileNewEmail.getText().toString())
                    && checkPasswordMatching(editEmailCurrentPassword, editEmailConfirmPass)) {
                editEmailRequest(editProfileNewEmail.getText().toString(),
                        editEmailCurrentPassword.getText().toString(),
                        editEmailConfirmPass.getText().toString());
            } else {
                editEmailCurrentPassword.setError("The Input data are wrong!");
            }
        }
    }

    private void changeName() {
        if (isNotEmptyNameGenderDate(editProfileNewName.getText())) {
            if (isValidName(editProfileNewName.getText().toString())) {
                editNameRequest(editProfileNewName.getText().toString());
            }
        }
    }

    private void changeGender() {
        if (genderRadioGroup.getCheckedRadioButtonId() == R.id.radio_male) {
            editGenderRequest("1");
        } else {
            editGenderRequest("2");
        }
    }

    private void changeDate() {
        String date = datePickerFromET();
        editDateOfBirthRequest(date);
    }

    private String datePickerFromET() {
        editProfileNewDate.setOnClickListener(v -> {
            Calendar c = Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), (view1, year, month, dayOfMonth) -> {
                String _year = String.valueOf(year);
                String _month = String.valueOf(month + 1);
                String _date = String.valueOf(dayOfMonth);
                if (_month.length() < 2)
                    _month = "0" + _month;
                if (_date.length() < 2)
                    _date = "0" + _date;
                collectionDate = _year + "-" + _month + "-" + _date;
                editProfileNewDate.setText(collectionDate);
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            dialog.updateDate(1990, 1, 11);
            dialog.show();
        });
        return collectionDate;
    }

    private void changePhone() {
        if (isNotEmpty(editProfilePhone.getText(), editPhoneCurrentPass.getText(),
                editPhoneConfirmCurrentPass.getText())) {
            editPhoneRequest(code + editProfilePhone.getText().toString(),
                    editPhoneCurrentPass.getText().toString(),
                    editPhoneConfirmCurrentPass.getText().toString());

        }
    }

    private void changePass() {
        if (isNotEmpty(editPassNewPass.getText(), editPassCurrentPass.getText(),
                editPassConfirmNewPass.getText())) {
            if (isValidPassword(editPassNewPass.getText().toString())
                    && checkPasswordMatching(editPassNewPass, editPassConfirmNewPass)) {
                editPasswordRequest(editPassCurrentPass.getText().toString(), editPassNewPass.getText().toString(),
                        editPassConfirmNewPass.getText().toString());
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}
