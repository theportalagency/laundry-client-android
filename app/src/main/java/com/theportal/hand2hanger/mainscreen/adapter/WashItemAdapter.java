package com.theportal.hand2hanger.mainscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.ServiceTypesResponse;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import static com.theportal.hand2hanger.Utility.Constants.WASH_TYPE_ID;


public class WashItemAdapter extends RecyclerView.Adapter<WashItemAdapter.MyViewHolder> {
    private final List<ServiceTypesResponse> courseItemList;
    private Context context;
    private int count = 0;
    private boolean isPLAYBLE = false;

    private WashItemClickListener mListener;

    public WashItemAdapter(List<ServiceTypesResponse> courseItemList, Context context) {
        this.courseItemList = courseItemList;
        this.context = context;
    }

    public WashItemAdapter(Context context) {
        this.context = context;
        courseItemList = null;
    }

    public void setWashItemClickListener(WashItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wash_press_item_rv, parent,
                        false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ServiceTypesResponse courseItem = courseItemList.get(position);
        holder.priceAuthorTv.setText(courseItem.getPrice() + " QR");
        holder.itemTitleTv.setText(courseItem.getName());
        if (!isPLAYBLE) {
            holder.tv_number.setText(courseItem.getItemsCount() + "");
        }
        else {
            holder.tv_number.setText(count+"");
        }
        WASH_TYPE_ID = courseItem.getId();
        holder.btn_add.setOnClickListener(v -> {
            if (mListener != null) {
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemAdded(position, courseItem.getId(),
                            Integer.parseInt(holder.tv_number.getText().toString()),courseItem.getOrder_id());
                }
            }
        });
        holder.btn_remove.setOnClickListener(view1 -> {
            if (mListener != null) {
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemDeleted(position, courseItem.getId(), courseItem.getService_id(),
                            Integer.parseInt(holder.tv_number.getText().toString()),courseItem.getOrder_id(),courseItem.getItem_id());
                }
            }
        });
    }

    public void increaseCount(int pos, int num) {
        count = (num + 1);
        isPLAYBLE = true;
        notifyItemChanged(pos);
    }

    public void decreaseCount(int num,int pos) {
        count = (num - 1);
        isPLAYBLE = true;
        notifyItemChanged(pos);
    }

    @Override
    public int getItemCount() {
        assert courseItemList != null;
        return courseItemList.size();
    }

    public interface WashItemClickListener {
        void onItemAdded(int position, int item_id, int count, Integer order_id);
        void onItemDeleted(int position, int id, int service_id, int count, int order_id,int itemID);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_type)
        TextView itemTitleTv;
        @BindView(R.id.item_cost)
        TextView priceAuthorTv;
        @BindView(R.id.tv_number)
        TextView tv_number;
        @BindView(R.id.btn_add)
        TextView btn_add;
        @BindView(R.id.btn_remove)
        TextView btn_remove;
        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
