package com.theportal.hand2hanger.myorders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.theportal.hand2hanger.DetailedOrderFragment;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.PickUpTimes;
import com.theportal.hand2hanger.mainscreen.SplitActivity;
import com.theportal.hand2hanger.data.model.FullOrderResponseDataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.theportal.hand2hanger.Utility.Constants.ORDER_ADDRESS;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_AMOUNT;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_COLLECTION_DATE;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_COLLECTION_TIME;
import static com.theportal.hand2hanger.Utility.Constants.ORDER_ID;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private final List<FullOrderResponseDataModel> ordersListItem;
    private Context context;

    OrdersAdapter(List<FullOrderResponseDataModel> ordersList, Context context) {
        this.ordersListItem = ordersList;
        this.context = context;
    }

    public void setItemListener(OrdersAdapter.RecyclerViewClickListener itemListener) {
    }

    @NonNull
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.orders_item_rv, parent, false);
        return new OrdersAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.ViewHolder holder, int position) {
        FullOrderResponseDataModel orderItem = ordersListItem.get(position);
        holder.itemRefTv.setText(orderItem.getOrderNo());
        holder.itemsStatusTv.setText(orderItem.getStatus());
        double total_amount = 0;
        if (orderItem.getAddress() != null){
            total_amount += orderItem.getAddress().getDelivery_cost();
        }
        total_amount += orderItem.getTotalAmount();
        total_amount += orderItem.getTax();
        holder.itemPriceTv.setText(total_amount + " QR");
        if (orderItem.getCollectionDate() != null)
            holder.itemDateTv.setText(orderItem.getCollectionDate());
        else
            holder.itemDateTv.setText("----");
        PickUpTimes pickUpTimes = orderItem.getPickup_time();
        if (pickUpTimes == null) {
            holder.itemsTimeTv.setText("----");
            return;
        }
        holder.itemsTimeTv.setText(pickUpTimes.getTo());
    }

    @Override
    public int getItemCount() {
        return ordersListItem.size();
    }

    private void loadFragment(Fragment fragment) {
        if (context != null) {
            FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.intro_slider_container, fragment, "DetailedOrderFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    private interface RecyclerViewClickListener {
        void onItemDelete(View v, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ref_num)
        TextView itemRefTv;
        @BindView(R.id.item_price)
        TextView itemPriceTv;
        @BindView(R.id.item_time)
        TextView itemsTimeTv;
        @BindView(R.id.item_date)
        TextView itemDateTv;
        @BindView(R.id.item_status)
        TextView itemsStatusTv;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(view1 -> {
                if (ordersListItem.get(getAdapterPosition()).getStatusId() == 4 )
                    return;
                Intent intent = new Intent(context, SplitActivity.class);
                intent.putExtra("extra", "details");
                intent.putExtra(ORDER_ID, ordersListItem.get(getAdapterPosition()).getId());
                context.startActivity(intent);
            });
        }


    }
}
