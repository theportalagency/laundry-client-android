package com.theportal.hand2hanger.wallet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theportalagency.hand2hanger.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context context;
    private List<HistoryModel> items;

    public HistoryAdapter(Context context, List<HistoryModel> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryModel model = items.get(position);
        if (model == null)
            return;

        holder.tv_invoice.setText(model.getInvoice());
        holder.draw_tv.setText(model.getDraw()+ " QR");
        holder.tv_remaining.setText(model.getRemaining()+ " QR");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView draw_tv,tv_remaining,tv_invoice;

        public ViewHolder(View itemView) {
            super(itemView);
            draw_tv = itemView.findViewById(R.id.draw_tv);
            tv_remaining = itemView.findViewById(R.id.tv_remaining);
            tv_invoice = itemView.findViewById(R.id.tv_invoice);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
