package com.theportal.hand2hanger.account;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.AccountItem;
import com.theportal.hand2hanger.data.model.PointsResponseModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.MainActivity;
import com.theportal.hand2hanger.mainscreen.SplitActivity;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.mainscreen.SplitActivity;
import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.AccountItem;
import com.theportal.hand2hanger.data.model.PointsResponseModel;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.mainscreen.HomeFragment;
import com.theportal.hand2hanger.mainscreen.MainActivity;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.BackButtonUtil.hideBackButton;
import static com.theportal.hand2hanger.Utility.Constants.IMAGE_KEY;
import static com.theportal.hand2hanger.Utility.Constants.NAME_KEY;
import static com.theportal.hand2hanger.Utility.Constants.POINTS_KEY;
import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class AccountFragment extends Fragment implements AccountAdapter.OnItemClickListener {
    private final ArrayList<AccountItem> accountItemArrayList = new ArrayList<>();
    @BindView(R.id.account_screen_rv)
    RecyclerView accountRv;
    @BindView(R.id.view_profile_tv)
    TextView viewProfile;
    @BindView(R.id.mypoints_tv)
    TextView myPointsTv;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.imageView13)
    CircleImageView imageView13;
    private Toolbar toolbar;
    private TextView fragmentTitle;
    private APIInterface apiInterface;
    private String token, name;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        Constants.FRAGMENT_NUMBER = 3;
        toolbar = Objects.requireNonNull(getActivity()).findViewById(R.id.toolbar);
        fragmentTitle = getActivity().findViewById(R.id.top_bar_title);
        toolbar.setNavigationOnClickListener(view1 -> {
            loadFragment(new HomeFragment());
            hideBackButton(toolbar);
            this.fragmentTitle.setText("");
        });




        apiInterface = ApiClient.getClient().create(APIInterface.class);

        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        name = pref.getString(NAME_KEY, name);
        image = Constants.BASE_URL + "uploads/" + pref.getString(IMAGE_KEY, image);
        Glide.with(Constants.context)
                .load(image)
                .placeholder(R.drawable.placeholder)
                .into(imageView13);
        nameTv.setText("" + name);
        getUserPoints();
        setupAccountUI();

        viewProfile.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SplitActivity.class);
            intent.putExtra("extra", "edit");
            startActivity(intent);
        });

        return view;
    }


    private void setupAccountUI() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        accountRv.setLayoutManager(layoutManager);
        accountItemArrayList.add(new AccountItem(R.string.my_Address, R.drawable.ic_account_adress));
        accountItemArrayList.add(new AccountItem(R.string.payment, R.drawable.ic_payment));
        accountItemArrayList.add(new AccountItem(R.string.my_Wallet, R.drawable.ic_wallet));
        accountItemArrayList.add(new AccountItem(R.string.packages, R.drawable.ic_package));
        accountItemArrayList.add(new AccountItem(R.string.support, R.drawable.ic_support));
        accountItemArrayList.add(new AccountItem(R.string.about, R.drawable.ic_about));
        accountItemArrayList.add(new AccountItem(R.string.privacy_Policy, R.drawable.ic_terms_conditions));
        accountItemArrayList.add(new AccountItem(R.string.logOut, R.drawable.ic_logout));
        AccountAdapter mAdapter = new AccountAdapter(accountItemArrayList, getActivity());
        accountRv.setAdapter(mAdapter);
        accountRv.setHasFixedSize(true);
        mAdapter.setOnItemClickListener(this);
    }

    private void getUserPoints() {
        Call<PointsResponseModel> call = apiInterface.getUserPoints(token);
        call.enqueue(new Callback<PointsResponseModel>() {
            @Override
            public void onResponse(Call<PointsResponseModel> call, Response<PointsResponseModel> response) {
                switch (response.code()) {
                    case 200: {
                        editor.putInt(POINTS_KEY, response.body() != null ? response.body().getPoints() : null);
                        editor.apply();
                        myPointsTv.setText("MY Points: " + response.body().getPoints());
                        updateUi();
                    }
                    break;
                    default:
                        Toast.makeText(Constants.context, Constants.context.getResources().getString(R.string.check_net), Toast.LENGTH_LONG).show();
                        break;

                }

            }

            @Override
            public void onFailure(Call<PointsResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });
    }

    private void updateUi() {
        myPointsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), SplitActivity.class);
                i.putExtra("extra", "points");
                startActivity(i);
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.intro_slider_container, fragment);
            fragmentTransaction.disallowAddToBackStack();
            fragmentTransaction.commit(); // save the changes
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), SplitActivity.class);
        switch (position) {
            case 0:
                intent.putExtra("extra", "address");
                startActivity(intent);
                /*loadFragment(new MyAddressFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.my_Address);*/
                break;
            case 1:
                intent.putExtra("extra", "payment");
                startActivity(intent);
                /*loadFragment(new PaymentFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.payment);*/
                break;
            case 2:
                intent.putExtra("extra", "wallet");
                startActivity(intent);
                /*loadFragment(new WalletFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.my_Wallet);*/
                break;
            case 3:
                intent.putExtra("extra", "package");
                startActivity(intent);
                /*loadFragment(new PackagesFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.packages);*/
                break;
            case 4:
                intent.putExtra("extra", "support");
                startActivity(intent);
                /*loadFragment(new SupportFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.support);*/
                break;
            case 5:
                intent.putExtra("extra", "about");
                startActivity(intent);
                /*loadFragment(new AboutFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.about);*/
                break;
            case 6:
                intent.putExtra("extra", "privacy");
                startActivity(intent);
                /*loadFragment(new PrivacyFragment());
                showBackButton(toolbar);
                fragmentTitle.setText(R.string.privacy_Policy);*/
                break;
            case 7:
                logoutUser();
                break;

        }
    }

    private void logoutUser() {
        clearToken();
        Call<SignupResponseModel> call = apiInterface.logoutUser(token);
        call.enqueue(new Callback<SignupResponseModel>() {
            @Override
            public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                SignupResponseModel responseModel = response.body();
                if (responseModel == null) return;
                Toast.makeText(getActivity(), Constants.context.getResources().getString(R.string.success_logout), Toast.LENGTH_LONG).show();
                clearToken();
                getActivity().finish();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }

            @Override
            public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");

            }
        });
    }

    private void clearToken() {
        editor.clear();
        editor.apply();
        disconnectFromFacebook();
    }

    private void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Constants.context = context;
    }
}