package com.theportal.hand2hanger.data.network;

import com.theportal.hand2hanger.CategriesModel;
import com.theportal.hand2hanger.data.ServicesModel;
import com.theportal.hand2hanger.data.model.AddressUser;
import com.theportal.hand2hanger.data.model.AddressData;
import com.theportal.hand2hanger.data.model.BasketResponse;
import com.theportal.hand2hanger.data.model.CityName;
import com.theportal.hand2hanger.data.model.CityZone;
import com.theportal.hand2hanger.data.model.DataFacility;
import com.theportal.hand2hanger.data.model.EditProfileResponse;
import com.theportal.hand2hanger.data.model.FacilityTypesResponse;
import com.theportal.hand2hanger.data.model.FilterItems;
import com.theportal.hand2hanger.data.model.FilterItemsID;
import com.theportal.hand2hanger.data.model.FullOrderResponseModel;
import com.theportal.hand2hanger.data.model.GiftResponseModel;
import com.theportal.hand2hanger.data.model.ImgBody;
import com.theportal.hand2hanger.data.model.LoginResponseModel;
import com.theportal.hand2hanger.data.model.Notifcation;
import com.theportal.hand2hanger.data.model.Order;
import com.theportal.hand2hanger.data.model.OrderItem;
import com.theportal.hand2hanger.data.model.PickUpTimer;
import com.theportal.hand2hanger.data.model.PointsResponseModel;
import com.theportal.hand2hanger.data.model.Prices;
import com.theportal.hand2hanger.data.model.Pricing;
import com.theportal.hand2hanger.data.model.PromoCodeModel;
import com.theportal.hand2hanger.data.model.RedeemsPoints;
import com.theportal.hand2hanger.data.model.ReviewModel;
import com.theportal.hand2hanger.data.model.ServiceTypesResponse;
import com.theportal.hand2hanger.data.model.ServicesChilds;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.model.SizesResponseModel;
import com.theportal.hand2hanger.data.model.UrgentModel;
import com.theportal.hand2hanger.data.model.WalletHistory;
import com.theportal.hand2hanger.data.model.WalletModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    @POST("api/auth/register")
    @Headers({
            "Content-Type: application/json"
    })
    Call<SignupModel> getSignupResponse(@Body Map<String, String> signupData);

    @POST("api/auth/login")
    @Headers({
            "Content-Type: application/json"
    })
    Call<LoginResponseModel> getLoginResponse(@Body Map<String, String> loginData);

    @POST("api/auth/social")
    @Headers({
            "Content-Type: application/json"
    })
    Call<LoginResponseModel> social_login(@Body Map<String, String> loginData);

    @POST("api/auth/reset")
    @Headers({
            "Content-Type: application/json"
    })
    Call<LoginResponseModel> reset_pass(@Body Map<String, String> reset);

    @POST("api/auth/forgot")
    @Headers({
            "Content-Type: application/json"
    })
    Call<LoginResponseModel> forget_password(@Body Map<String, String> forgetData);

    @POST("api/auth/logout")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> logoutUser(@Header("X-Authorization") String token);

    //Get ServiceTypes  (T-shirt-Shirt)
    @POST("api/v1/services/types")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<ServiceTypesResponse>> getServiceTypes(@Header("X-Authorization") String token, @Body Map<String, Integer> serviceTypesMap);

    //Get facilityTypes (Incense)
    //Parameters--> ServiceTypeId
    @POST("api/v1/services/types")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<FacilityTypesResponse> getFacilityTypes(@Header("X-Authorization") String token, @Body Map<String, Integer> facilityTypesMap);

    //Get facilityTypeChild (Gold Oud)
    //Parameters--> FacilityTypeId
    @POST("api/v1/facilities/childs")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<FacilityTypesResponse>> getFacilityTypeChilds(@Header("X-Authorization") String token, @Body Map<String, String> facilityChildMap);



    @GET("api/v1/facilities")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<FacilityTypesResponse>> getFacilityType(@Header("X-Authorization") String token);


    //Set OrderDetails
    //Parameters--> Array of items and special requests.
    @POST("api/v1/orders")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<Order> sendOrderDetails(@Header("X-Authorization") String token, @Body Map<String, Object> orderDetailsMap);

    @POST("api/v1/orders/increment")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<Order> increment_order(@Header("X-Authorization") String token, @Body Map<String, Object> orderDetailsMap);

    @POST("api/v1/orders/decrement")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<Order> decrement_order(@Header("X-Authorization") String token, @Body Map<String, Object> orderDetailsMap);

    @POST("api/v1/orders/update")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> sendBasketObject(@Header("X-Authorization") String token, @Body Map<String, Object> basketObject);

    //get Basket
    @GET("api/v1/orders/basket")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<BasketResponse> getBasketOrders(@Header("X-Authorization") String token);

    @HTTP(method = "DELETE", path = "api/v1/orders/item/delete", hasBody = true)
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<OrderItem> deleteOrderItem(@Header("X-Authorization") String token, @Body Map<String, Object> deletedItemMap);


    //delete Order item
    @DELETE("api/v1/orders/item/delete")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> addOrderItem(@Header("X-Authorization") String token, @Body Map<String, String> orderItemMap);

    @POST("api/v1/orders/item")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<FilterItems> get_items_by_filters(@Header("X-Authorization") String token, @Body Map<String, Integer> orderItemMap);

    @GET("api/v1/orders/item/{id}")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<FilterItemsID> get_items_by_id(@Header("X-Authorization") String token, @Path("id")int id);

    @GET("api/v1/sizes")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<SizesResponseModel>> getClothesSizes(@Header("X-Authorization") String token);

    @GET("api/v1/colors")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<SizesResponseModel>> getClothesColors(@Header("X-Authorization") String token);

    @POST("api/v1/orders/confirm")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> confirmPayment(@Header("X-Authorization") String token, @Body Map<String, Integer> paymentOptionId);

    @GET("/api/pages/about")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> getAboutResponse();


    @GET("api/v1/packages")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<PackagesResponse> getPackagesResponse(@Header("X-Authorization") String token);

    @POST("api/v1/packages")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> subscribe(@Header("X-Authorization") String token,@Body Map<String,Object> data);



    @GET("/api/pages/terms")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> getPrivacyUrl();

    @GET("api/v1/cities")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<CityName>> getCityName(@Header("X-Authorization") String token);

    @GET("api/v1/cities/regions")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<CityZone>> getCityZone(@Header("X-Authorization") String token, @Query("city_id") int cityID);

    @GET("api/v1/pickupTimes")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<PickUpTimer>> getPickupTimes(@Header("X-Authorization") String token);


    //EDIT API
    @POST("/api/v1/customers/edit/name")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editName(@Header("X-Authorization") String token, @Body Map<String, Object> userNameMap);

    @POST("/api/v1/customers/edit/email")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editEmail(@Header("X-Authorization") String token, @Body Map<String, Object> userEmailMap);

    @POST("/api/v1/customers/edit/phone")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editPhone(@Header("X-Authorization") String token, @Body Map<String, Object> userPhoneMap);

    @POST("/api/v1/customers/edit/name")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editUserName(@Header("X-Authorization") String token, @Body Map<String, Object> userNameMap);

    @POST("/api/v1/customers/edit/gender")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editGender(@Header("X-Authorization") String token, @Body Map<String, Object> genderMap);

    @POST("/api/v1/customers/edit/password")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editPass(@Header("X-Authorization") String token, @Body Map<String, Object> userPassMap);

    @POST("/api/v1/customers/edit/date_of_birth")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<EditProfileResponse> editDateOFBirth(@Header("X-Authorization") String token, @Body Map<String, Object> dateOfBirthMap);

    @Multipart
    @POST("/api/v1/customers/edit/image")
    Call<ImgBody> editImage(@Header("X-Authorization") String token, @Part MultipartBody.Part image);

    @POST("/api/v1/customers/addresses")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<AddressUser> addAddressItem(@Header("X-Authorization") String token, @Body Map<String, Object> addressMap);

    @GET("/api/v1/customers/addresses")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
        //TODO Change Model
    Call<AddressData> getAddress(@Header("X-Authorization") String token, @Query("address_type_id") int addressType);


    @GET("/api/v1/customers/points")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<PointsResponseModel> getUserPoints(@Header("X-Authorization") String token);

    @GET("/api/v1/gifts/getRedeems")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<RedeemsPoints> getReddems(@Header("X-Authorization") String token);

    @GET("api/v1/services/prices")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<Pricing> getItemPrice(@Header("X-Authorization") String token, @Query("service_id") int service_id);

    //GET ORDERS    0 FOR ALL, 1 FOR ACTIVE
    @GET("/api/v1/customers/orders")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<FullOrderResponseModel> getUserAllOrders(@Header("X-Authorization") String token,@Query("page")int page);

    @GET("/api/v1/orders/details/{order_id}")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<DetailsOrder> getDetailsOrder(@Header("X-Authorization") String token, @Path("order_id") int order_id);

    @POST("/api/v1/orders/setPayment")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SupportModel> setPayment(@Header("X-Authorization") String token, @Body Map<String, Object> userImage);

    //
    @GET("api/v1/categories")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<CategriesModel>> categories(@Header("X-Authorization") String token);

    @GET("api/v1/services")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<ServicesModel> services(@Header("X-Authorization") String token);

    @GET("/api/v1/customers/edit/image")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> editUserImage(@Header("X-Authorization") String token, @Body Map<String, Object> userImage);


    @POST("/api/v1/orders/cancel")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> cancelOrder(@Header("X-Authorization") String token, @Body Map<String, Object> orderIdMap);

    @POST("/api/v1/orders/rate")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> submitReview(@Header("X-Authorization") String token, @Body Map<String, Object> submitReviewMap);

    @GET("/api/v1/orders/rate")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<ReviewModel> getReview(@Header("X-Authorization") String token, @Query("order_id") String order_id);

    @POST("/api/v1/invoices")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<ResponseBody> getOrderInvoice(@Header("X-Authorization") String token, @Body Map<String, Object> orderId);

    @POST("/api/v1/support")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SupportModel> sendSupportTicket(@Header("X-Authorization") String token, @Body Map<String, Object> support);

    @POST("/api/v1/orders/place")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> placeOrder(@Header("X-Authorization") String token, @Body Map<String, Object> placeOrderMap);

    @POST("/api/v1/orders/track")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupResponseModel> getTrackingPhase(@Header("X-Authorization") String token, @Body Map<String, Object> orderIdMap);

    @GET("/api/v1/gifts")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<GiftResponseModel> getGiftsList(@Header("X-Authorization") String token);

    @POST("/api/v1/gifts/redeem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    Call<SignupModel> redeemGift(@Header("X-Authorization") String token, @Body Map<String, Object> giftIdMap);


    @POST("/api/v1/promocode")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    Call<PromoCodeModel> check_promo(@Header("X-Authorization") String token, @Body Map<String, String> promo);

    @GET("/api/v1/services/childs/{service_id}")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<ServicesChilds>> facilities_childs(@Header("X-Authorization") String token, @Path("service_id") int service_id);

    @POST("/api/v1/services/types/childs")
    @Headers({"Content-Type: application/json","X-Requested-With: XMLHttpRequest"})
    Call<List<ServicesChilds>> service_type_tailoring(@Header("X-Authorization") String token, @Body Map<String,String> items);

    @POST("/api/v1/services/type/prices")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<Prices> service_type_prices(@Header("X-Authorization") String token, @Body Map<String,String> items);

    @GET("/api/v1/customers/wallet")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<WalletModel> wallet_balance(@Header("X-Authorization") String token);

    @GET("/api/v1/wallet")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<WalletHistory> history(@Header("X-Authorization") String token);

    @GET("api/v1/customers/notifications")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<List<Notifcation>> notification(@Header("X-Authorization") String token);

    @POST("api/v1/orders/urgent")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<UrgentModel> urgent(@Header("X-Authorization") String token, @Body Map<String,Integer> items);

    @POST("api/v1/orders/facilities/add")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<DataFacility> toogle_facility(@Header("X-Authorization") String token, @Body Map<String,Integer> items);

    @POST("api/v1/orders/facilities/delete")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<DataFacility> delete_facility(@Header("X-Authorization") String token);

    @GET("/api/v1/customers/addresses/delete/{addreess_id}")
    @Headers({
            "Content-Type: application/json",
            "X-Requested-With: XMLHttpRequest"
    })
    Call<SignupModel> delete_address(@Header("X-Authorization") String token,
                                     @Path("addreess_id") int addreess_id);
}