package com.theportal.hand2hanger.mypoints;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.theportal.hand2hanger.data.model.RedeemsPointsData;
import com.theportal.hand2hanger.wallet.HistoryModel;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyGiftsAdapter extends RecyclerView.Adapter<MyGiftsAdapter.ViewHolder> {

    private Context context;
    private List<RedeemsPointsData> items;

    public MyGiftsAdapter(Context context, List<RedeemsPointsData> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.gifts_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RedeemsPointsData model = items.get(position);
        if (model == null)
            return;

        holder.gift_code.setText("#"+model.getCode());
        holder.giftName.setText(model.getGift().getName());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView giftName,gift_code;

        public ViewHolder(View itemView) {
            super(itemView);
            gift_code = itemView.findViewById(R.id.btn_gift_code);
            giftName = itemView.findViewById(R.id.giftName);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
