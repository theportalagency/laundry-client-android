package com.theportal.hand2hanger.packages;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theportal.hand2hanger.data.network.DataPackages;
import com.theportalagency.hand2hanger.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.ViewHolder> {

    private Context context;
    private List<DataPackages> items;
    private onPackageSelect onPackageSelect;
    private TextView textView;
    private int mSelectedPosition = -1;


    public PackagesAdapter(Context context, List<DataPackages> items, PackagesAdapter.onPackageSelect onPackageSelect) {
        this.context = context;
        this.items = items;
        this.onPackageSelect = onPackageSelect;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_packages_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        DataPackages model = items.get(position);

        if (model == null)
            return;

        holder.mypackage_cost.setText(model.getCost() + " QR");
        holder.mypackage_title.setText(model.getName() + "Packages");
        holder.itemView.setOnClickListener(view ->
                {
                    mSelectedPosition = position;
                    notifyDataSetChanged();
                    onPackageSelect.select(model.getId(), model.getName(),model.getCost(), holder.mypackage_title);
                }
        );
        if (mSelectedPosition == position) {
            holder.mypackage_title.setTextColor(context.getResources().getColor(R.color.YellowColor));
        } else {
            holder.mypackage_title.setTextColor(context.getResources().getColor(R.color.silverColor));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mypackage_title, mypackage_cost;


        public ViewHolder(View itemView) {
            super(itemView);
            mypackage_title = itemView.findViewById(R.id.mypackage_title);
            mypackage_cost = itemView.findViewById(R.id.mypackage_cost);
            itemView.setClickable(true);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface onPackageSelect {
        void select(int id, String name, double cost, TextView tv_name);
    }
}
