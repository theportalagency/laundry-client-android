package com.theportal.hand2hanger.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServicesModel {

    @SerializedName("data")
    @Expose
    private List<ServiceModelData> data;

    public List<ServiceModelData> getData() {
        return data;
    }
}
