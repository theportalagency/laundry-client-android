package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notifcation {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("notifiable_id")
    @Expose
    private int notifiable_id;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("data")
    @Expose
    private NotificationData data;

    @SerializedName("reciver_id")
    @Expose
    private int reciver_id;

    public int getId() {
        return id;
    }

    public int getNotifiable_id() {
        return notifiable_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public NotificationData getData() {
        return data;
    }

    public int getReciver_id() {
        return reciver_id;
    }
}
