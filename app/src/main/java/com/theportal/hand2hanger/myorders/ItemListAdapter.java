package com.theportal.hand2hanger.myorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.theportal.hand2hanger.Utility.Constants;
import com.theportal.hand2hanger.data.model.ItemsListOrderDetails;
import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.Utility.ColorIconProvider;
import com.theportal.hand2hanger.data.model.ItemsListOrderDetails;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    private Context context;
    private List<ItemsListOrderDetails> items;


    public ItemListAdapter(Context context, List<ItemsListOrderDetails> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_list_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ItemsListOrderDetails model = items.get(position);

        if (model == null)
            return;

        holder.tv_total.setText(model.getTotal_amount() + " QR");
        holder.tv_count.setText(model.getCount()+"");
        double newCount = (model.getTotal_amount()/model.getCount());
        holder.tv_amount.setText(Constants.roundAvoid(newCount)+"");
        holder.tv_type.setText(model.getType());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_amount, tv_total, tv_type, tv_count;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_total = itemView.findViewById(R.id.tv_total);
            tv_count = itemView.findViewById(R.id.tv_count);
            tv_type = itemView.findViewById(R.id.tv_type);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
