package com.theportal.hand2hanger.data.model;

public class NotificationItem {
    private String notificationTitle, notificationBrief, notificationDescription, notificationDate, notificationTime, notificationStatue;

    public NotificationItem(String notificationTitle, String notificationBrief, String notificationDescription, String notificationDate, String notificationTime, String notificationStatue) {
        this.notificationTitle = notificationTitle;
        this.notificationBrief = notificationBrief;
        this.notificationDescription = notificationDescription;
        this.notificationDate = notificationDate;
        this.notificationTime = notificationTime;
        this.notificationStatue = notificationStatue;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationBrief() {
        return notificationBrief;
    }

    public void setNotificationBrief(String notificationBrief) {
        this.notificationBrief = notificationBrief;
    }

    public String getNotificationDescription() {
        return notificationDescription;
    }

    public void setNotificationDescription(String notificationDescription) {
        this.notificationDescription = notificationDescription;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getNotificationStatue() {
        return notificationStatue;
    }

    public void setNotificationStatue(String notificationStatue) {
        this.notificationStatue = notificationStatue;
    }
}
