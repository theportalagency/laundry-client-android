package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.SerializedName;

public class GiftsReedem {

    @SerializedName("error")
    private String error;

    @SerializedName("success")
    private String success;

    public String getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }
}
