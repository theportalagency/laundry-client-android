package com.theportal.hand2hanger.data.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.theportal.hand2hanger.data.model.AddressOrderDetails;
import com.theportal.hand2hanger.data.model.InvoiceOrderDetails;
import com.theportal.hand2hanger.data.model.ItemsListOrderDetails;
import com.theportal.hand2hanger.data.model.PickUpTimes;
import com.theportal.hand2hanger.data.model.PromoCodeModel;

import java.util.List;

public class DetailsOrderData {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("orderNo")
    @Expose
    private String orderNo;

    @SerializedName("is_urgent")
    @Expose
    private int is_urgent;

    @SerializedName("is_hanged")
    @Expose
    private int is_hanged;

    @SerializedName("facility")
    @Expose
    private String facility;

    @SerializedName("facility_amount")
    @Expose
    private int facility_amount;


    @SerializedName("subtotal")
    @Expose
    private double subtotal;

    @SerializedName("facility_id")
    @Expose
    private int facility_id;

    @SerializedName("isPercentage")
    @Expose
    private int isPercentage;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("collection_date")
    @Expose
    private String collection_date;

    @SerializedName("total_quantity")
    @Expose
    private int total_quantity;

    @SerializedName("total_amount")
    @Expose
    private double total_amount;

    @SerializedName("pickup_time")
    @Expose
    private PickUpTimes pickup_time;



    @SerializedName("status_id")
    @Expose
    private int status_id;

    @SerializedName("items")
    @Expose
    private List<ItemsListOrderDetails> items;

    @SerializedName("address")
    @Expose
    private AddressOrderDetails address;

    @SerializedName("invoice")
    @Expose
    private InvoiceOrderDetails invoice;

    @SerializedName("promocode")
    @Expose
    private PromoCodeModel promocode;

    @SerializedName("promo_amount")
    @Expose
    private PromoCodeAmount promo_amount;

    @SerializedName("tax")
    @Expose
    private double tax;

    public PromoCodeAmount getPromo_amount() {
        return promo_amount;
    }

    public PromoCodeModel getPromocode() {
        return promocode;
    }

    public double getTax() {
        return tax;
    }


    public double getSubtotal() {
        return subtotal;
    }

    public int getId() {
        return id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public int getIs_urgent() {
        return is_urgent;
    }

    public int getIs_hanged() {
        return is_hanged;
    }

    public String getStatus() {
        return status;
    }

    public String getCollection_date() {
        return collection_date;
    }

    public PickUpTimes getPickup_time() {
        return pickup_time;
    }

    public int getTotal_quantity() {
        return total_quantity;
    }

    public double getTotal_amount() {
        return total_amount;
    }


    public int getStatus_id() {
        return status_id;
    }

    public List<ItemsListOrderDetails> getItems() {
        return items;
    }

    public AddressOrderDetails getAddress() {
        return address;
    }

    public String getFacility() {
        return facility;
    }

    public int getFacility_id() {
        return facility_id;
    }

    public int getFacility_amount() {
        return facility_amount;
    }

    public int getIsPercentage() {
        return isPercentage;
    }

    public InvoiceOrderDetails getInvoice() {
        return invoice;
    }

}
