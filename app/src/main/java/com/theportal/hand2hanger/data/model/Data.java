package com.theportal.hand2hanger.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("order_id")
    @Expose
    private int order_id;

    @SerializedName("items")
    @Expose
    private List<OrderItem> items = null;

    @SerializedName("order_amount")
    @Expose
    private float orderAmount;

    @SerializedName("order_total_amount")
    @Expose
    private float orderTotalAmount;

    @SerializedName("amount")
    @Expose
    private float amount;

    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("total_amount")
    @Expose
    private float total_amount;

    //
    public List<OrderItem> getItems() {
        return items;
    }

    public float getAmount() {
        return amount;
    }

    public float getTotal_amount() {
        return total_amount;
    }

    public int getOrder_id() {
        return order_id;
    }


    public int getCount() {
        return count;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public float getOrderAmount() {
        return orderAmount;
    }

    public float getOrderTotalAmount() {
        return orderTotalAmount;
    }


}
