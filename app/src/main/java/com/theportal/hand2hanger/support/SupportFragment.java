package com.theportal.hand2hanger.support;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.theportalagency.hand2hanger.R;
import com.theportal.hand2hanger.data.model.SignupResponseModel;
import com.theportal.hand2hanger.data.network.APIInterface;
import com.theportal.hand2hanger.data.network.ApiClient;
import com.theportal.hand2hanger.data.network.SupportModel;
import com.theportal.hand2hanger.data.network.TestSendSupportBody;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theportal.hand2hanger.Utility.Constants.TOKEN_KEY;
import static com.theportal.hand2hanger.Utility.Constants.USER_SHARED_PREFERENCES_KEY;

public class SupportFragment extends Fragment {

    @BindView(R.id.support_title_et)
    EditText supportTitleEt;
    @BindView(R.id.support_body_et)
    EditText supportBodyEt;
    @BindView(R.id.support_submit)
    Button submitButton;
    @BindView(R.id.toolbarr)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_inside)
    TextView tv_toolbar_inside;

    @BindView(R.id.counterPanel_)
    ConstraintLayout counterPanel_;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private APIInterface apiInterface;
    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support, container, false);
        ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        pref = Objects.requireNonNull(getActivity()).getSharedPreferences(USER_SHARED_PREFERENCES_KEY, 0);
        editor = pref.edit();
        token = pref.getString(TOKEN_KEY, token);
        submitButton.setOnClickListener(view1 -> {
            if (supportTitleEt.getText().equals(null) || supportBodyEt.getText().equals(null)) {
                Toast.makeText(getActivity(), "Please Enter a title and a body.", Toast.LENGTH_LONG).show();
            } else {
                sendSupportMessage();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        counterPanel_.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_toolbar_inside.setText(R.string.support);
        toolbar.setNavigationOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
    }

    private void sendSupportMessage() {
        Map<String, Object> supportTicketMap = new HashMap<>();
        supportTicketMap.put("title", supportBodyEt.getText().toString());
        supportTicketMap.put("description", supportTitleEt.getText().toString());
        TestSendSupportBody testSendSupportBody = new TestSendSupportBody(
                supportTitleEt.getText().toString(),
                supportBodyEt.getText().toString()
        );
        Call<SupportModel> call = apiInterface.sendSupportTicket(token,supportTicketMap );
        call.enqueue(new Callback<SupportModel>() {
            @Override
            public void onResponse(Call<SupportModel> call, Response<SupportModel> response) {
                Log.e("sendSupport Code:", response.code() + "");
                Log.e("sendSupport Message:", response.message() + "");
                switch (response.code()) {
                    case 200: {
                        Toast.makeText(getActivity(), ""+response.body().getSuccess(), Toast.LENGTH_LONG).show();
                        supportBodyEt.setText("");
                        supportTitleEt.setText("");
                    }
                    break;
                    default:
                        break;

                }

            }

            @Override
            public void onFailure(Call<SupportModel> call, Throwable t) {
                Log.e("onFailure", t.getMessage() + "");
                call.cancel();
            }
        });

    }
}
