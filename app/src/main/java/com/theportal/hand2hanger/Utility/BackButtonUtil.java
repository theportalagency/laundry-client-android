package com.theportal.hand2hanger.Utility;

import androidx.appcompat.widget.Toolbar;

import com.theportalagency.hand2hanger.R;

public class BackButtonUtil {

    public static void showBackButton(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_back_btn);
    }

    public static void hideBackButton(Toolbar toolbar) {
        toolbar.setNavigationIcon(null);
    }
}
